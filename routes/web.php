<?php
//use RealRashid\SweetAlert\Facades\Alert;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Artisan;


Route::get('migrate',function(){
    Artisan::call('migrate:fresh');
    Artisan::call('db:seed');
    return redirect()->route('login');
});

//use Symfony\Component\Routing\Route;

Route::group(['Karyana'],function () {

//            Route::get('/', function () {  return view('welcome');    } );
    Route::get('/', 'HomeController@index')->name('home');
                 //************** Public Routs *******************

//  ************************paymentmathods******************************

    Route::get('/dikha', 'StripePaymentController@dikha')->name('dikha');

    Route::post('paypal', 'orderController@paypal')->name('checkout.paypal');
    Route::get('processpaypal', 'orderController@returnpaypal')->name('process.paypal');
    Route::get('cancelPaypal', 'orderController@cancelPaypal')->name('cancel.Paypal');
    Route::get('/payments', 'orderController@testview')->name('test_view');
    Route::post('/payments', 'orderController@charge')->name('payment');
    Route::get('/tracking', 'RiderController@Tracking')->name('tracking');

    Route::group(['StripePaymentController'], function () {

        Route::get('stripe', 'StripePaymentController@stripe');
        Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');
        Route::get('Home', 'StripePaymentController@StripeOrderPlacement')->name('StripeOrder-Placement');
    });


//  ************************paymentmathods******************************



            Route::group(['Public'],function () {

                View::composer(['*'],function($view){$categories=\App\Models\Category::with('childs')->get();$view->with('categories',$categories);
                });
                Route::group(['HomeController'],function () {




                    Route::get('/home', 'HomeController@index')->name('home');
                    Route::get('{shopid}/details', 'HomeController@shopDetail')->name('shopDetail');
                    Route::get('{productid}/product-details', 'HomeController@productDetail')->name('productDetail');
                    Route::get('productCompare' , 'HomeController@compareProduct')->name('compareProduct');
                    Route::get('/home', 'HomeController@index')->name('home');
                    Route::get('{shopid}/details', 'HomeController@shopDetail')->name('shopDetail');
//                    -------test-route
                    Route::get('details/{categoryid}','HomeController@test')->name('test');
//                    -------
                });


                Route::group(['DryfruitController'],function () {
                    Route::get('/dryfruit', 'DryfruitController@index')->name('dryfruit');
                });

                Route::group(['BrandController'],function () {

                    Route::get('/brand', 'BrandController@index')->name('brand');
                });

                Route::group(['BeverageController'],function () {

                    Route::get('/beverage', 'BeverageController@index')->name('beverage');

                });

                Route::group(['MailController'],function () {

                    Auth::routes(['verify' => true]);
                    Route::get('sendMail', 'MailController@index');
                    Route::get('login/facebook', 'Auth\LoginController@redirectToProvider')->name('facebook');
                    Route::get('login/facebook/callback', 'Auth\LoginController@handleFacebookCallback');


                });

                Route::group(['FeedbackController'],function () {
                    Route::get('/feedback', 'FeedbackController@review')->name('feedback');
                    Route::post('/post/comment', 'FeedbackController@post_comment')->name('post-feedback');
                    Route::get('/fetch','FeedbackController@fetchComment')->name('fetch');
                    Route::post('/delete-comment','FeedbackController@deleteComment')->name('deleteComment');
                });


                 Route::group(['ProductController'],function () {

//                    Route::get('/addToCart/{product}','ProductController@addToCart')->name('addToCart');
//                    Route::get('addToCompare/{product}', 'ProductController@addToCompare')->name('addToCompare');
                    Route::get('/addToCart/{product}','ProductController@addToCart')->name('addToCart');
//                    Route::get('/addToCompare/{id}', 'ProductController@addToCompare')->name('addToCompare');
                    Route::get('search','ProductController@search')->name('search');
                    Route::get('find' , 'ProductController@find')->name('find');
                     Route::get('addToCompare/{product}', 'ProductController@addToCompare')->name('addToCompare');
                     Route::post('/remove-item', 'ProductController@removeItem')->name('remove_Item');

                });

            });
                 //************** Rrivate Routs *********************
              Route::group(['private'], function () {
                //*********** if call inAuthorized route*************
                Route::get('/denied', function () {return view('404');})->name('denied');

                Route::group(['Admin', 'middleware' => 'usertypechecking:' . 'admin'], function () {
                    Route::group(['RiderController'], function () {

                        Route::get('/add-rider', 'RiderController@add_rider')->name('add_rider');
                        Route::post('/add-riders', 'RiderController@saveRider')->name('save_rider');
                        Route::get('/rider-list', 'RiderController@rider_list')->name('rider_list');
                        Route::get('/rider/delete/{id}', 'RiderController@rider_delete')->name('rider_delete');
                        Route::get('/rider/edit/{id}', 'RiderController@rider_edit')->name('rider_edit');
                        Route::post('/rider/update', 'RiderController@update_rider')->name('update_rider');

                    });

                    Route::group(['ProductController'], function () {
                        Route::get('/add-product', 'ProductController@add_product')->name('add_product');
                        Route::post('/add-product', 'ProductController@create')->name('add_product');
                        Route::get('/product-list', 'ProductController@product_list')->name('product_list');
                        Route::post('/update-product', 'ProductController@update_product')->name('update_product');
                        Route::get('/products/edit/{id}', 'ProductController@edit_product')->name('edit_product');
                        Route::get('/products/delete/{id}', 'ProductController@delete_product')->name('delete_product');
                        Route::get('/subcatogries', 'ProductController@subcatogries')->name('subcatogries');
                        Route::post('quickview', 'ProductController@quickview')->name('quick-view');
//                        Route::get('/subcatogries', 'ProductController@s')->name('subcatogries');

//                        Route::get('/addToCart','ProductController@addToCart')->name('addToCart');
//                        Route::get('/cart','ProductController@cart')->name('cartShow');
                        Route::get('order-list','OrderController@orderList')->name('order_list');
                    });


                    Route::group(['ShopController'], function () {

                        Route::get('/add-shop', 'ShopController@view_shop')->name('view_shop');
                        Route::post('/add-shop', 'ShopController@create')->name('add_shop');
                        Route::get('/shop-list', 'ShopController@shop_list')->name('shop_list');
                        Route::get('/admin-dashboard', 'ShopController@admindashboard')->name('view_dashboard');
                        Route::post('/shops-update', 'ShopController@shop_update')->name('shop_update');
                        Route::get('/shops/edit/{id}', 'ShopController@shop_edit')->name('shop_edit');
                        Route::get('/shops/delete/{id}', 'ShopController@shop_delete')->name('shop_delete');

                    });

                    Route::group(['CategoryController'], function () {

                        Route::get('/add-category', 'CategoryController@manageCategory')->name('add_category');
                        Route::post('/add-category', 'CategoryController@add_category')->name('add_category');
                        Route::get('/table-category', 'CategoryController@category_table')->name('category_table');
                        Route::post('/delete-category', 'CategoryController@deleteCategory')->name('delete_category');
                        Route::post('/update-category', 'CategoryController@updateCategory')->name('updatecategory');
                        Route::post('/add-child-category', 'CategoryController@createChild')->name('createchild');
                    });

                });


/**
 * Rider's Dashboard Routes
 */
        Route::group([ 'Rider', 'middleware' => 'usertypechecking:'.'rider'], function () {
    Route::get('/rider-profile', 'RiderdashboardContrller@rider_profile')->name('rider_profile');
    Route::get('/rider-edit-profile', 'RiderdashboardContrller@rider_edit_profile')->name('rider_edit_profile');
    Route::get('/order-history','RiderdashboardContrller@order_history')->name('order_history');

});
                            //*****START: (RiderDash) Routes
                Route::group(['Rider', 'middleware' => 'usertypechecking:' . 'rider'], function () {

                    Route::group(['RiderdashboardContrller'],function () {
                        Route::get('/rider-profile', 'RiderdashboardContrller@rider_profile')->name('rider_profile');
                        Route::get('/rider-edit-profile', 'RiderdashboardContrller@rider_edit_profile')->name('rider_edit_profile');
                        Route::get('/rider-order-list', 'RiderdashboardContrller@rider_order_list')->name('rider_order_list');
                        Route::get('/rider-order-history', 'RiderdashboardContrller@rider_order_history')->name('rider_order_history');
                        Route::post('/edit-profile', 'RiderdashboardContrller@editprofile')->name('editprofile');
                    });
                });


            });


    Route::group(['Rider', 'middleware' => 'usertypechecking:' . 'customer'], function () {



        Route::get('/Wishlist', 'WishlistController@wishlistview')->name('Wishlist');
        Route::get('/track-order','OrderController@trackOrder')->name('track-order');

        Route::get('/Wish-list/{id}', 'WishlistController@savewish')->name('save-wish');
        Route::get('/Wishlist/{id}', 'WishlistController@wishdelete')->name('wish_delete');
//        Route::get('/addToCart/{product}','ProductController@addToCart')->name('addCart');



    });
//        Route::post('/remove-item', 'ProductController@removeItem')->name('remove_Item');
//        Route::get('searched_Product' ,'ProductController@searchedProduct')->name('searched_Product');
//    });






});
//Route::get('/cart-view','ProductController@cartView')->name('cartShow');
//Route::post('/remove-cart','ProductController@removeCart')->name('remove');
Route::get('/addToCart','ProductController@addToCart')->name('addToCart');
Route::get('/cart-view','ProductController@cartView')->name('cartShow');
Route::post('/remove-cart','ProductController@removeCart')->name('remove');
Route::get('/view-details','ProductController@viewdetails')->name('view-details');
//Route::get('/addToCompare/{id}', 'ProductController@addToCompare')->name('addToCompare');
Route::get('/check-out','ProductController@checkOut')->name('checkOut');
Route::get('/order-placement','OrderController@OrderPlacement')->name('OrderPlacement');
Route::get('/approve-order','OrderController@approve_order')->name('approve_order');
Route::get('/accept-order','RiderController@accept_order')->name('accept_order');
Route::get('/delivered/{id}','RiderController@delivered')->name('delivered');

Route::get('/assign-rider-notification/{id}','OrderController@assignRiderNoti')->name('assignRiderNoti');
Route::get('/discard-order/{id}','OrderController@discardOrder')->name('discardOrder');

Route::get('/rider-accept-order/{id}','RiderController@riderAcceptOrder')->name('riderAcceptOrder');
Route::get('/getOrderDetails/{id}','OrderController@getOrderDetails')->name('getOrderDetails');
Route::get('/getOrderDetailsRider','RiderController@getOrderDetailsRider')->name('getOrderDetailsRider');
Route::get('/getOrderRiderDetails','OrderController@getOrderRiderDetails')->name('getOrderRiderDetails');

Route::get('/register1','ProductController@registerCheckout')->name('register1');
Route::get('/logincheck','ProductController@logincheck')->name('logincheck');
Route::get('/checkout/guest','ProductController@guest')->name('checkout/guest');
Route::get('/checkout/register','ProductController@registerCheckOut1')->name('checkout/register');
Route::get('/shipping','ProductController@shipping')->name('shipping');
Route::get('/shipping-address','ProductController@shippingAddress')->name('shipping-address');
Route::get('/payment-address','ProductController@paymentAddress')->name('payment-address');
Route::get('/payment-method','ProductController@paymentmethod')->name('payment-method');
Route::get('/order-confirm','ProductController@orderConfirm')->name('order-confirm');
Route::post('/payment-address/save','ProductController@paymentAddressSave')->name('payment-address/save');
Route::post('/register-checkout','RegisterCheckoutController@register')->name('register-checkout');
Route::get('/product-Comparison','ProductController@comparison')->name('product-Comparison');

Route::get('markAsRead',function(){
   auth()->user()->unreadNotifications->markAsRead();
    return redirect('/approve-order');
})->name('markAsRead');

Route::get('markAsReadRider',function(){
    auth()->user()->unreadNotifications->markAsRead();
    return redirect('/accept-order');
})->name('markAsReadRider');

