@extends('layouts.master')
@section('title', 'Karyana')
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />--}}
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />--}}

@section('body')
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Track Order</a></li>
    </ul>
    <div class="">
        <!-- <div class="box-heading"><span>Our Products</span></div> -->
        <div id="tabs" class="htabs">
            @if(session()->has('message'))
                <p class="alert alert-success">
                    {{session()->get('message')}}
                </p>
            @endif
            <div class="tab-title-main">
                <ul class='etabs nav nav-tabs'>
                    <li class='nav-item active'>
                        <a href="#" data-toggle="tab" class="nav-link">Live Tracking</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <h1></h1>
    <hr>
    <div class="container">
        <div class="row">
            <aside id="column-left" class="col-sm-3 hidden-xs">
                <div class="box">
                    <div class="box-heading">Categories</div>
                    <div class="box-content">
                        <ul class="box-category treeview-list treeview">
                            @foreach($categories->where('parent_id',0) as $cat)
                                <li>
                                    <a href="#" >{{$cat->name}}</a>
                                    @if($cat->childs->count()>0)
                                        <ul>
                                            @foreach($cat->childs as $submenu)
                                                <li>
                                                    <a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a>
                                                </li>
                                            @endforeach

                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </aside>
            <div class="col-sm-3">
                <label for="" class="text-danger">Select Order</label>
                <select id="delivery" onchange="orderDetail(this.value)" class="form-control">
                <option disabled selected hidden>Select order</option>
                    @if($bool == 0)
                    @foreach($delivery as $data)
                    <option value="{{$data->order_id}}">KARYANA-OR-{{$data->id}}</option>
                        @endforeach
                        @else
                        <p>Pleas Place Order</p>
                        @endif
                </select>
{{--            <div class="col-sm-6">--}}
{{--                <h1>Track Rider</h1>--}}
{{--                <div id="map-canvas" style="width:100%;height:400px"></div>--}}
{{--            </div>--}}
            <div class="">
                <label for="" class="text-danger" style="margin: 15px">Your Order</label>
                <div class="card-body">
                    <div id="no-more-tables">
                        <table class="table table-responsive table-bordered text-center" id="dataTable" width="100%"
                               cellspacing="0">
                            <thead class="bg-grey">
                            <tr class="">
                                <th>Order Details</th>
                                <th>Rider Details</th>
                            </tr>
                            </thead>
                            <tbody>
{{--                            @foreach($products as $product)--}}

                                <tr>
                                    <td >
                                       <a href="" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-shopping-cart" style="font-size:24px;color: #0b0b0b"></i></a>
                                    </td>
                                    <td>
                                        <a href="" data-toggle="modal" data-target="#example"> <i class="fa fa-motorcycle" style="font-size:24px;color: #0b0b0b"></i></a>
                                    </td>
                                </tr>
{{--                            @endforeach--}}
                            </tbody>
                        </table>
                        <!-- order details table Modal start from here-->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><h1>Order Details</h1></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Shop Name</th>
                                                <th>Order Date</th>
                                                <th>Quantity</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody id="order-detail">
                                            <tr>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
{{--                                     order details table modal end here--}}


                    <!--Rider detail table Modal start from here-->
                        <div class="modal fade" id="example" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><h1>Rider Details</h1></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th>Rider Name</th>
                                                <th>Vehicle Number</th>
                                                <th>Phone number</th>
                                            </tr>
                                            </thead>
                                            <tbody id="rider-detail">
                                            <tr id="rider">

                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
{{--                        Rider details modal end here--}}
                    </div>
                </div>
            </div>

{{--            <div class="col-sm-6">--}}
{{--                                <h1>Track Rider</h1>--}}
{{--                                <div id="map-canvas" style="width:100%;height:400px"></div>--}}
{{--                            </div>--}}

        </div>
            <div class="col-sm-6">
                <h1>Track Rider</h1>
                <div id="map-canvas" style="width:100%;height:400px"></div>
            </div>
    </div>
    </div>
    <script>
        function orderDetail (id) {
            // $id= $(#'delivery').value();

            $('#order-detail').empty();
            $.ajax({
                type : 'get',
                data : {'id':id},
                url  : '{{route('getOrderRiderDetails')}}',
                success : function (data) {
                    // $('#order-detail').empty();
                    $.each(data,function (i,value) {
                        var tr=$("<tr/>");
                        tr.append($("<td/>",{
                            text:value.product_id[0]
                        }))

                            .append($("<td/>",{
                                text:value.order
                            }))
                            .append($("<td/>",{
                                text:value.updated_at
                            }))
                            .append($("<td/>",{
                                text:value.quantity
                            }))
                            .append($("<td/>",{
                                text:value.amount
                            }))
                        $('#order-detail').append(tr);
                        // $('#order-detail').empty();

                    }
                    )
                    ////
                    $('#rider-detail').empty();

                    var tr=$("<tr/>");

                    tr.append($("<td/>",{
                                    text:data[0].rider
                                }))
                                .append($("<td/>",{
                                    text:data[0].vehicle
                                }))
                                .append($("<td/>",{
                                    text:data[0].contact
                                }))
                    $('#rider-detail').append(tr);
                    ////
                    console.log(data);
                }
            });
        }
    </script>
    <script>
        function check() {
            console.log('rider latitude ');
            console.log('rider longitude ');

            window.lat = 31.5499918;
            window.lng = 74.3279534;

            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(updatePosition);
                    console.log('after upate curent lati' + window.lat);
                    console.log('after upate curent Longi' + window.lng);
                }
                return null;
            }
            //
            function updatePosition(position) {
                if (position) {
                    window.lat = position.coords.latitude;
                    window.lng = position.coords.longitude;
                }
            }

            var map;
            var userMark;
            var mark;
            var lineCoords = [];
            var userLocation =new google.maps.LatLng(31.5365528, 74.3266412);
            var riderLocation = new google.maps.LatLng(lat, lng);
            var iconBase = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/';

            map = new google.maps.Map(document.getElementById('map-canvas'), {
                center: {
                    lat: lat,
                    lng: lng
                },
                zoom: 15,
            });
            console.log('user lati');
            console.log('user lati');
            userMark = new google.maps.Marker({
                position: userLocation,
                map: map,
            });

            mark = new google.maps.Marker({
                position: {
                    lat: lat,
                    lng: lng
                },
                map: map,
            });


            function addMarker() {
                var geocoder = new google.maps.Geocoder;
                var infowindow = new google.maps.InfoWindow;
                var directionsDisplay = new google.maps.DirectionsRenderer();
                var directionsService = new google.maps.DirectionsService();

                directionsDisplay.setMap(map);
                directionsDisplay.setOptions({
                    suppressMarkers: true
                });

                var request = {
                    origin: riderLocation,
                    destination: userLocation,
                    travelMode: 'DRIVING'
                };

                directionsService.route(request, function(result, status) {
                    if (status === 'OK') {
                        directionsDisplay.setDirections(result);
                    }
                });
            }



            addMarker();
            setInterval(function() {
                console.log('update lati' + window.lat);
                console.log('update longi' + window.lng);
                console.log('map lati' + lat);
                console.log('map long' + lng);


                if (mark != undefined) {

                } else {
                    mark = new google.maps.Marker({
                        position: {
                            lat: lat,
                            lng: lng
                        },
                        map: map,
                    });
                    addMarker();

                }
                updatePosition(getLocation());
            });

            function currentLocation() {
                return {
                    lat: window.lat,
                    lng: window.lng
                };
            }

            var redraw = function(payload) {
                lat = payload.message.lat;
                lng = payload.message.lng;
                map.setCenter({
                    lat:lat,
                    lng:lng,
                    alt: 0
                });
                mark.setPosition({
                    lat: lat,
                    lng: lng,
                    alt: 0
                });

                lineCoords.push(new google.maps.LatLng(lat, lng));

                var lineCoordinatesPath = new google.maps.Polyline({
                    path: lineCoords,
                    geodesic: true,
                    strokeColor: '#2E10FF'
                });

                lineCoordinatesPath.setMap(map);
            };
            var pnChannel = "karyana-track";

            var pubnub = new PubNub({
                publishKey: 'pub-c-ba54aa46-435f-4289-9050-30a8de397fd0',
                subscribeKey: 'sub-c-1112b4ba-1666-11ea-bb30-4a09dafe54f0'
            });

            pubnub.subscribe({
                channels: [pnChannel]
            });
            pubnub.addListener({
                message: redraw
            });
            setInterval(function() {
                pubnub.publish({
                    channel: pnChannel,
                    message: {
                        lat: window.lat,
                        lng: window.lng
                    }
                });
            }, 500);
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBgV-mkhz5pqHJrtexHQXJdV12D8nGefoI&callback=check"></script>
@endsection




