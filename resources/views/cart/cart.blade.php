@extends('layouts.master')
@section('title', 'cart')
{{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
@section('body')
    <!------ Include the above in your HEAD tag ---------->
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Cart View</a></li>
    </ul>
    <div id="tabs" class="htabs">
        @if(session()->has('message'))
            <p class="alert alert-success">
                {{session()->get('message')}}
            </p>
        @endif
        <div class="tab-title-main">
            <ul class="etabs nav nav-tabs">
                <li class="nav-item active">
                    <a href="#" data-toggle="tab" class="nav-link">Cart view</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <aside id="column-left" class="col-sm-3">
                <div class="box">
                    <div class="box-heading">Categories</div>
                    <div class="box-content">
                        <ul class="box-category treeview-list treeview">
                            @foreach($categories->where('parent_id',0) as $cat)
                                <li>
                                    <a href="#" >{{$cat->name}}</a>
                                    @if($cat->childs->count()>0)
                                        <ul>
                                            @foreach($cat->childs as $submenu)
                                                <li>
                                                    <a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </aside>
            <div class="col-sm-9">
                @if (Session::has('cart'))
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">Image</th>
                        <th class="text-left">Product Name</th>
                        <th class="text-left">Shop Name</th>
                        <th class="text-left">Price</th>
                        <th class="text-left">Quantity</th>
                        <th >Action</th>
                        <th class="text-right">Total</th>
                    </tr>
                    </thead>

                    @foreach($products as $product)
{{--                        @dd($product['product']->image)--}}
                        <tbody>
                        <tr>

                            <td>
                                @php
                                $images=$product['product']->image;
                                $img=explode(",",$images);
                                @endphp
                                <img width="60" src="{{asset('images').'/'.$img[0]}}" > </td>
                            <td class="text-left">{{$product['product']['name']}}</td>
                            <td class="text-left">{{$product['product']['shop']['name']}}</td>
                            <td class="text-left" >Rs:<span id="{{$product['product']['id']}}a">{{ $product['product']['price']}}</span>
                            </td>
                            <td class="text-left"><div class="input-group btn-block" style="max-width: 50px;">
                                    <input style="width: 35px;"  onchange="quantity(this.value,{{$product['product']['id']}})" type="number" value="{{$product['qty']}}" name="qty" id="{{$product['product']['id']}}" min="1">
                                </div>
                            </td>
                            <td>
                            <span class="input-group-btn">
                                 <button type="submit" data-toggle="tooltip" title="" class="btn btn-danger" onclick="removeRow({{$product['product']['id']}});" data-original-title="Remove"><i class="fa fa-times-circle"></i></button>
                              </span>
                            </td>
                            <td class="text-right" >Rs:<span id="{{$product['product']['id']}}b">{{ $product['price']}}</span></td>
                        </tr>
                        </tbody>
                    @endforeach
                </table>
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <table class="table table-bordered">
                            <tbody><tr>
                                <td class="text-right"><strong>Sub-Total:</strong></td>
                                <td class="text-right" >Rs:<span id="cart-subtotal">{{ $totalPrice}}</span></td>
                            </tr>
                            <tr>
                                <td class="text-right"><strong>Shipping Rate:</strong></td>
                                <td class="text-right" >Rs:<span id="cart-subtotal">{{$shippingRate}}</span></td>
                            </tr>
                            <tr>
                                <td class="text-right"><strong>Total:</strong></td>
                                <td class="text-right" >Rs:<span id="cart-subtotal">@if($products){{ $totalPrice + $shippingRate }}@endif</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="buttons clearfix">
                    <div class="pull-left"><a href="{{asset('home')}}" class="btn btn-default">Continue Shopping</a></div>
                    <div class="pull-right"><a @if(\Session::get('cart')->totalQty!=0) href="{{ route('checkOut') }}" @else (\Session::get('cart')->totalQty==0) style="visibility: hidden" @endif class="btn btn-primary">Checkout</a></div>
                </div>
            </div>
            @else
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Image</th>
                            <th class="text-left">Product Name</th>
                            <th class="text-left">Price</th>
                            <th class="text-left">Quantity</th>
                            <th >Action</th>
                            <th class="text-right">Total</th>
                        </tr>
                    </thead>
                </table>
                <h2>No Product in cart</h2>
                @endif
             </div>
         </div>
    </div>

    @include('dashboards.projectjs')

@endsection
