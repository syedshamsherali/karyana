<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{Auth::user()->type}}  Dashboard - Karyana.pk</title>

    <!-- Custom fonts for this template-->
    <link href="{{asset('assets\backend\admindash\fontawesome-free\css\all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('assets\backend\admindash\sb-admin-2\sb-admin-2.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets\backend\admindash\datatables\dataTables.bootstrap4.css')}}" rel="stylesheet">

{{--    bootstrap time picker--}}

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets\backend\admindash\treeview\treeview.css')}}" rel="stylesheet">
{{--    datepicker start--}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
{{--    <link rel="stylesheet" href="/resources/demos/style.css">--}}
{{--        datepicker end--}}
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background: #74ab26">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('view_dashboard')}}">
            <div class="sidebar-brand-text mx-3">{{auth()->user()->type}} Dashboard</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="{{route('view_dashboard')}}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Interface
        </div>

        <!-- Nav Item - Shops Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#shop" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-store-alt"></i>
                <span>Shops</span>
            </a>
            <div id="shop" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Manage Shops:</h6>
                    <a class="collapse-item" href="{{route('view_shop')}}">Add Shop</a>
                    <a class="collapse-item" href="{{route('shop_list')}}">Shop List</a>
                </div>
            </div>
        </li>

        <!-- Nav Item - Categories Collapse Menu -->

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#category" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-th-large"></i>
                <span>Categories</span>
            </a>
            <div id="category" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Manage Categories:</h6>
                    <a class="collapse-item" href="{{route('add_category')}}">Add Category</a>
                    <a class="collapse-item" href="{{route('category_table')}}">Category Table</a>
                </div>
            </div>
        </li>

        <!-- Nav Item - Products Collapse Menu -->

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#product" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-shopping-basket"></i>
                <span>Products</span>
            </a>
            <div id="product" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Manage Products:</h6>
                    <a class="collapse-item" href="{{route('add_product')}}">Add Product</a>
                    <a class="collapse-item" href="{{route('product_list')}}">Product List</a>
                </div>
            </div>
        </li>



        <!-- Nav Item - Riders Collapse Menu -->

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#rider" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-biking"></i>
                <span>Riders</span>
            </a>
            <div id="rider" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Manage Riders:</h6>
                    <a class="collapse-item" href="{{route('add_rider')}}">Add Rider</a>
                    <a class="collapse-item" href="{{route('rider_list')}}">Rider List</a>
                </div>
            </div>
        </li>

        <!-- Nav Item - Orders Collapse Menu -->

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#order" aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fa fa-shopping-cart"></i>
                <span>Orders</span>
            </a>
            <div id="order" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Manage Orders:</h6>
                    <a class="collapse-item" href="{{route('approve_order')}}">Approve Order</a>
                    <a class="collapse-item" href="{{route('order_list')}}">Order List</a>
                </div>
            </div>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">




        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light topbar mb-4 static-top shadow" style="background: #74ab26">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>



                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-search fa-fw"></i>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                            <form class="form-inline mr-auto w-100 navbar-search">
                                <div class="input-group">
                                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>

                    <!-- Nav Item - Alerts -->
                    <li class="nav-item dropdown no-arrow mx-1">
                        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-bell fa-fw"></i>
                            <!-- Counter - Alerts -->
                            <span class="badge badge-danger badge-counter">{{auth()->user()->unreadNotifications ->count()}}</span>
                        </a>
                        <!-- Dropdown - Alerts -->
                        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                            <h6 class="dropdown-header">
                                Notification Center
                            </h6>
                            @foreach(auth()->user()->unreadNotifications as $notification)
                            <a class="dropdown-item d-flex align-items-center" href="{{route('markAsRead')}}">
                                <div class="mr-3">
                                    <div class="icon-circle bg-success">
                                        <i class="fas fa-donate text-white"></i>
                                    </div>
                                </div>
                                <div>
                                    <div class="small text-gray-500">{{$notification->created_at->format('d-m-Y')}}</div>
                                    {{$notification->data['data']}}
                                </div>
                                <div class="pl-3">
                                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                                </div>
                            </a>
                            @endforeach

                            <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                        </div>
                    </li>



                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-light small">{{Auth::user()->type}}</span>
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>


                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid" >

                @yield('body')

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer py-2 " style="background: #74ab26">
            <div class="container my-auto">
                <div class="copyright text-center my-auto text-light">
                    <span>Copyright &copy; Karyana.pk 2019</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                @if (Route::has('login'))
                    @auth
                        <form method="post" action="{{route('logout')}}" class="dropdown-item">

                            @csrf
                    <button class="btn btn-primary" type="submit">Logout</button>
                        </form>
                        @endauth
                    @endif
            </div>
        </div>
    </div>
</div>
<!-- Bootstrap core JavaScript-->
<script src="{{asset('assets\backend\admindash\jquery\jquery.min.js')}}"></script>
<script src="{{asset('assets\backend\admindash\bootstrap\js\bootstrap.bundle.min.js')}}"></script>
<!-- Core plugin JavaScript-->
<script src="{{asset('assets\backend\admindash\jquery-easing\jquery.easing.min.js')}}"></script>
<!-- Custom scripts for all pages-->
<script src="{{asset('assets\backend\admindash\sb-admin-2\sb-admin-2.min.js')}}"></script>


<!-- Page level plugins -->

<script src="{{asset('assets\backend\admindash\datatables\jquery.dataTables.js')}}"></script>
<script src="{{asset('assets\backend\admindash\datatables\dataTables.bootstrap4.js')}}"></script>
<!-- Page level custom scripts -->
<script src="{{asset('assets\backend\admindash\demo\datatables-demo.js')}}"></script>
<script src="{{asset('assets\backend\admindash\chart\Chart.js')}}"></script>
<script src="{{asset('assets\backend\admindash\demo\chart-pie-demo.js')}}"></script>
<script src="{{asset('assets\backend\admindash\demo\chart-area-demo.js')}}"></script>


<script src="{{asset('assets\backend\admindash\treeview\treeview.js')}}"></script>
{{--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--}}
{{--<link rel="stylesheet" href="{{asset('assets\backend\admindash\demo\style.css')}}">--}}
<script src="{{asset('assets\backend\admindash\treeview\treeview.js')}}"></script>
{{--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--}}
                                    {{--datepicker starts --}}
{{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--}}
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                                    {{--datepicker end--}}
{{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>--}}
{{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbUaNMEhxu11BmllfCa0Gg3AAZPz9ZjF0&libraries=places&callback=initAutocomplete" async defer></script>--}}
{{--<script type="text/javascript" src="{{asset('assets/frontend/custom_js/cropper.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('assets/frontend/custom_js/properties.js')}}"></script>--}}
<script type="text/javascript" src="{{asset('assets/frontend/custom_js/spartan-multi-image-picker.js')}}"></script>
<script type="text/javascript" src="../dist/js/"></script>
</body>
</html>
