<!doctype html>
<html dir="ltr" lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <base />
    <meta name="csrf-token" content="{{csrf_token()}}"/>

    <meta name="description" content="metromarket" />

    <link href="{{asset('assets\frontend\catalog\view\javascript\font-awesome\css\font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('assets\frontend\catalog\view\javascript\jquery\jquery-2.1.1.min.js')}}"></script>
    <script src="{{asset('assets\frontend\catalog\view\javascript\bootstrap\js\bootstrap.min.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Poppins:100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet">
    <link href="{{asset('assets\frontend\catalog\view\theme\metromarket\stylesheet\stylesheet.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets\frontend\catalog\view\theme\metromarket\stylesheet\axopc\lightbox.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets\frontend\catalog\view\javascript\jquery\magnific\magnific-popup.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets\frontend\catalog\view\javascript\jquery\magnific\magnific-popup.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets\frontend\catalog\view\theme\metromarket\stylesheet\axopc\carousel.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets\frontend\catalog\view\theme\metromarket\stylesheet\axopc\custom.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets\frontend\catalog\view\theme\metromarket\stylesheet\axopc\bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets\frontend\catalog\view\theme\metromarket\stylesheet\axopc\animate.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets\frontend\catalog\view\theme\metromarket\stylesheet\axopc\animate.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets\frontend\catalog\view\theme\metromarket\stylesheet\axopc\slick.css')}}" />
    <script src="{{asset('assets\frontend\catalog\view\javascript\lightbox\lightbox-2.6.min.js')}}"></script>
{{--    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('assets\frontend\catalog\view\javascript\jquery\swiper\css\swiper.min.css')}}" type="text/css" rel="stylesheet" media="screen" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets\frontend\catalog\view\javascript\jquery\swiper\css\opencart.css')}}"  media="screen" />
    <link href="{{asset('assets\frontend\catalog\view\javascript\jquery\owl-carousel\owl.carousel.html')}}" type="text/css" rel="stylesheet" media="screen" />
    <link href="{{asset('assets\frontend\catalog\view\javascript\jquery\owl-carousel\owl.transitions.html')}}" type="text/css" rel="stylesheet" media="screen" />
    <script src="{{asset('assets\frontend\catalog\view\javascript\jquery\swiper\js\swiper.jquery.js')}}"></script>
{{--    <script src="{{asset('assets\frontend\catalog\view\javascript\jquery\owl-carousel\owl.carousel.min.html')}}"></script>--}}
    <script src="{{asset('assets\frontend\catalog\view\javascript\common.js')}}"></script>
    <link href="{{asset('assets\frontend\image\catalog\cart.png')}}" rel="icon" />
    <script src="{{asset('assets\frontend\catalog\view\javascript\axopc\custom.js')}}"></script>
    <script src="{{asset('assets\frontend\catalog\view\javascript\axopc\videoBackground.js')}}"></script>
    <script src="{{asset('assets\frontend\catalog\view\javascript\axopc\jstree.min.js')}}"></script>
    <script src="{{asset('assets\frontend\catalog\view\javascript\axopc\jquery.formalize.min.js')}}"></script>
    <script src="{{asset('assets\frontend\catalog\view\javascript\axopc\jquery.elevatezoom.min.js')}}"></script>
    <script src="{{asset('assets\frontend\catalog\view\javascript\axopc\bootstrap-notify.min.js')}}"></script>
    <script src="{{asset('assets\frontend\catalog\view\javascript\axopc\snowfall.js')}}"></script>
    <script src="{{asset('assets\frontend\catalog\view\javascript\jquery\slick.js')}}"></script>
    <script src="{{asset('assets\frontend\catalog\view\javascript\axopc\tabs.js')}}"></script>
    <script src="{{asset('assets\frontend\catalog\view\javascript\jquery\magnific\jquery.magnific-popup.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('assets\frontend\catalog\view\javascript\jquery\magnific\magnific-popup.css')}}" />
    <script src="{{asset('assets\frontend\catalog\view\javascript\common.js')}}"></script>
    <script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.19.0.min.js"></script>

    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
</head>
<style>
    /* enable absolute positioning */
    .inner-addon {
        position: relative;
    }

    /* style glyph */
    .inner-addon .glyphicon {
        position: absolute;
        padding: 10px;
        pointer-events: none;
    }

    /* align glyph */
    .left-addon .glyphicon  { left:  0px;}
    .right-addon .glyphicon { right: 0px;}

    /* add padding  */
    .left-addon input  { padding-left:  30px; }
    .right-addon input { padding-right: 30px; }

</style>
<body>

<nav id="top">
    <div class="container">

        <div id="top-links" class="nav pull-right">
            <ul class="list-inline">
                <li><a href="#?route=account/wishlist" id="wishlist-total" title="Wish List (0)"><i class="fa fa-heart-o" aria-hidden="true"></i><span class="wishlist hidden-xs hidden-sm hidden-md">Wish List (0)</span></a></li>
                <li><a href="#?route=checkout/checkout" title="Checkout"><i class="fa fa-check-square-o" aria-hidden="true"></i>
                        <span class="checkout hidden-xs hidden-sm hidden-md">Checkout</span></a></li>
            </ul>
        </div>


        <div class="nav pull-left">

        </div>



    </div>
</nav>
<header>
    <div class="container">

            <div class="header-logo">
                <div id="logo"><a href="{{asset('home')}}">
                        <img src="{{asset('assets\frontend\image\catalog\logo.png')}}" title="karyana.pk" alt="metromarket" class="img-responsive" style="height:54px"  />

                    </a></div>
            </div>
        <div class="header-cart"><div id="cart" class="btn-group btn-block">
                <div class="cart-inner" >
                    <a href="{{route('cartShow')}}">

                    <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="btn btn-inverse btn-block btn-lg dropdown-toggle" >
                    </button>

                    <span class="cart_heading" title="My Cart" id="reloadCart">My Cart
                        <span id="cart-total">{{\Session::has('cart') ? \Session::get('cart')->totalQty:'0'}}</span>
                    </span>
                    </a>

                </div>
{{--                <div class="compare-total text-white"><a href="{{route('compareProduct')}}" id="compare-total"> Product Compare</a></div>--}}

            </div>
        </div>
        <div class="dropdown myaccount">
           @if(!Auth::check())
            <a href="" title="Sign Up" class="dropdown-toggle" data-toggle="dropdown">
                <span class="account_heading">Sign Up <span>& Join Free</span>
                </span>
{{--                @if(Auth::check())--}}
{{--                    <a href="#" style="color: #0b0b0b" class="btn-link nav-link btn btn-outline-warning ml-2">{{ auth()->user()->fullname }}</a>--}}
            </a>
            @endif
               @if(Auth::check())
                   <a href="" title="Sign Up" class="dropdown-toggle" data-toggle="dropdown">
                <span class="account_heading">{{Auth::user()->fullname}}
                </span>
                       {{--                @if(Auth::check())--}}
                       {{--                    <a href="#" style="color: #0b0b0b" class="btn-link nav-link btn btn-outline-warning ml-2">{{ auth()->user()->fullname }}</a>--}}
                   </a>
               @endif

            <ul class="dropdown-menu dropdown-menu-right myaccount-menu">
                @if (Route::has('register'))
                    <li class="nav-item">
                        @if(!Auth::check())
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>

                    <li>
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login ') }}</a>
                            @endif
                    </li>
                @endif
                @if (Route::has('login'))
                        <div class="top-right links ">
                         @auth
                            <form method="post" action="{{route('logout')}}">
                                @csrf
{{--                                <button style="color: #0b0b0b" class="btn-link nav-link btn btn-outline-warning ml-2">{{ auth()->user()->fullname }}</button>--}}
                                <button style="color: #0b0b0b" class="btn-link nav-link btn btn-outline-warning ml-2">logout</button>
                            </form>
                        @endif
                        @else
                            <div class="text-warning btn btn-outline-warning ml-2">
                                <a class="text-warning" href="{{ route('login') }}">Login</a>
                            </div>

                        @endauth

                        </div>
                        <li class="nav-item">

                            <a class="nav-link fa fa-heart" href="{{route('Wishlist')}}" title="Checkout">WishList</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('checkOut') }}?route=checkout/checkout" title="Checkout">Checkout</a>
                        </li>

            </ul>
        </div>
        <div class="main-menu">
            <nav class="nav-container" role="navigation">
                <div class="nav-inner">
                    <!-- ======= Menu Code START ========= -->
                    <!-- Opencart 3 level Category Menu-->
                    <div id="menu" class="main-menu">
                        <div class="nav-responsive"><span>Menu</span><div class="expandable"></div></div>

                        <ul class="nav navbar-nav">
                            <li class="top_level home"><a href="{{asset('home')}}">Home</a></li>
                            @foreach($categories as $cat)
                                @if($cat->childs->count()>0)
                                    @foreach($cat->childs as $submenu)
                                <li class="top_level"><a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a></li>
                                        @endforeach
                                @endif
                            @endforeach
                        </ul>

                    </div>

                </div>

            </nav>

        </div>


        <div class="nav-inner-cms ">
            <div class="container">
                <div class="content_headercms_top"><div class="box category">
                        <div class="box-category-heading">Shop By  categories</div>
                        <div class="box-content">
                            <ul id="nav-one" class="dropmenu">
                                @foreach($categories->where('parent_id',0) as $cat)
                                <li class="top_level dropdown"><a href="">{{$cat->name}}</a>
                                    @if($cat->childs->count()>0)
                                    <div class="dropdown-menu megamenu column3">
                                        <div class="dropdown-inner">
                                            <ul class="list-unstyled childs_1">
                                                <!-- 2 Level Sub Categories START -->
                                                <li class="dropdown">
                                                    <div class="dropdown-menu">
                                                        <div class="dropdown-inner">
                                                            <ul class="list-unstyled childs_2">
                                                                @foreach($cat->childs as $submenu)
                                                                <li><a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a></li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                    @endif
                                    @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="dropdown search"><div class="header-search dropdown-toggle" data-toggle="dropdown"></div>
                    <div id="search" class="input-group">
                        <form action="{{route('find')}}">
                              <span class="input-group-btn">
                                <input type="text" name="search" value="" placeholder="Search" class="form-control input-lg" />
            {{--                      <a href="{{route('search')}}">--}}
                                      <button class="btn btn-default btn-lg">Search
                                            <span class="search_button"><i class="fa fa-search" aria-hidden="true"></i></span>
                                      </button>
            {{--                      </a>--}}
                              </span>
                        </form>
                    </div>
                </div>
                <div class="header-bottom">
                    {{--<div class="compare-total"><a href="{{route('compareProduct')}}" id="compare-total"> Product Compare</a></div>--}}
                    <div class="social">
                        <ul id="collapsesocial">
                            <a href="{{route('track-order')}}" class="btn btn-xs pull-right text-light" style="height: 35px;">Track order
{{--                                <span id="">{{Session::has('key') ? \Session::get('key')->totalQty:'0'}}</span>--}}
                            </a>
                            <a href="{{route('product-Comparison')}}" id="compare" class="btn btn-xs pull-right text-light" style="height: 35px;">Compare
                                {{--                                <span id="compare-total">{{Session::has('key') ? \Session::get('key')->totalQty:'0'}}</span>--}}
                            </a>
{{--                            </div>--}}
{{--                            <li><button href="{{route('product-Comparison')}}" class="btn btn-info" >Compare</button></li>--}}
                            <li class="facebook"><a href="#">Facebook</a></li>
                            <li class="twitter"><a href="#">Twitter</a></li>
                            <li class="google-plus"><a href="#">Google-Plus</a></li>
{{--                            <li class="rss"><a href="#">RSS</a></li>--}}
{{--                            <li class="youtube"><a href="#">Youtube</a></li>--}}
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>



{{--=====================================================================================================--}}





<!-- ======= Quick view JS ========= -->
<script>
    function quickbox() {
        if ($(window).width() > 767) {
            $('.quickview').magnificPopup({
                type: 'iframe',
                delegate: 'a',
                preloader: true,
                tLoading: 'Loading image #%curr%...',
            });
        }
    }
    jQuery(document).ready(function() {
        quickbox();
    });
    jQuery(window).resize(function() {
        quickbox();
    });

</script>

<div id="common-home">
    <div class="content-top">
        <div id="content1" class="">
            <script type="text/javascript">
                $('#slideshow0').swiper({
                    effect: 'fade',
                    slidesPerView: 1,
                    pagination: '.slideshow0',
                    paginationClickable: true,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    spaceBetween: 0,
                    autoplay: 5000,
                    autoplayDisableOnInteraction: true,
                    loop: true,
                    paginationType: 'bullets',
                    paginationBulletRender: function(index, className) {
                        return '<span class="swiper-pagination-bullet">0' + (className + 1) + '</span>';
                    }
                });

            </script>

            <script type="text/javascript">
                // Can also be used with $(document).ready()
                $(window).load(function() {
                    $("#spinner").fadeOut("slow");
                });

            </script>
        </div>

        <div class="container-fluid">

        <div>
            <div class="container-fluid">
            <div class="row">
                <div>
                    @yield('text')
                    @yield('content')
                    @yield('body')
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<footer>
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 column first">

                </div>

                <div class="footer-right">
                    <div class="content_footer_top">
                        <div class="container">
                            <div class="row">
                                <div class="aei-service">
                                    <div class="container">
                                        <div class="row">
                                            <ul id="service-item">
                                                <li class="aei-service-item first">
                                                    <a class="aei-service-item-inner" href="#">
                                                        <span class="aei-image-block"><span class="aei-image-icon">&nbsp;</span></span>
                                                        <span class="service-right">
                                                                <span class="aei-service-title">Free Shipping</span>
                                                                <span class="aei-service-desc">All Order Over $100 </span>
                                                            </span>
                                                    </a>
                                                </li>
                                                <li class="aei-service-item second">
                                                    <a class="aei-service-item-inner" href="#">
                                                        <span class="aei-image-block"><span class="aei-image-icon">&nbsp;</span></span>
                                                        <span class="service-right">
                                                                <span class="aei-service-title">Money Back Guarantee</span>
                                                                <span class="aei-service-desc">If You Are Unable</span>
                                                            </span>
                                                    </a>
                                                </li>
                                                <li class="aei-service-item third">
                                                    <a class="aei-service-item-inner" href="#">
                                                        <span class="aei-image-block"><span class="aei-image-icon">&nbsp;</span></span>
                                                        <span class="service-right">
                                                                <span class="aei-service-title">24 / 7 Online Support </span>
                                                                <span class="aei-service-desc">Call Us Anytime You Want</span>
                                                            </span>
                                                    </a>
                                                </li>
                                                <li class="aei-service-item fourth">
                                                    <a class="aei-service-item-inner" href="#">
                                                        <span class="aei-image-block"><span class="aei-image-icon">&nbsp;</span></span>
                                                        <span class="service-right">
                                                                <span class="aei-service-title">Win $100 on shop</span>
                                                                <span class="aei-service-desc">enter now</span>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="arrows" id="service-arrows"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3 column second">
                        <h5 data-toggle="collapse" data-target="#collapseTwo">Information
                            <span class="toggle-arrow">
                                    <span class="show-arrow">
                                    </span>
                                    <span class="hide-arrow">
                                    </span>
                                </span>

                        </h5>
                        <ul id="collapseTwo" class="list-unstyled collapse">
                            <li><a href="#?route=information/information&amp;information_id=4">About Us</a></li>
                            <li><a href="#?route=information/information&amp;information_id=6">Delivery Information</a></li>
                            <li><a href="#?route=information/information&amp;information_id=3">Privacy Policy</a></li>
                            <li><a href="#?route=information/information&amp;information_id=5">Terms &amp; Conditions</a></li>
                            <li><a href="#?route=information/sitemap">Site Map</a></li>

                        </ul>
                    </div>
                        <div class="col-sm-3 column third">
                            <h5 data-toggle="collapse" data-target="#collapseThree">Extras
                                 <span class="toggle-arrow">
                                    <span class="show-arrow">
                                    </span>
                                    <span class="hide-arrow">
                                    </span>
                                </span>

                            </h5>
                        <ul id="collapseThree" class="list-unstyled collapse">
                            <li><a href="#?route=product/manufacturer">Brands</a></li>
                            <li><a href="#?route=account/voucher">Gift Certificates</a></li>
                            <li><a href="#?route=affiliate/login">Affiliates</a></li>
                            <li><a href="#?route=product/special">Specials</a></li>
                            <li><a href="#?route=account/account">My Account</a></li>

                        </ul>
                    </div>
                    <div class="col-sm-3 column forth">
                        <ul class="list-unstyled collapseThree">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="footer-bottom-first">

                <div class="powered">Powered By <a href="#">Karyana-Team</a> karyana.pk &copy; 2019</div>
            </div>
            <div class="content_footer_bottom">
                <div class="footer-payment">
                    <div class="payment-block">
                        <ul>
                            <li class="visa"><a href="#"><img alt="payment" title="payment" src="{{asset('assets\frontend\image\catalog\p1.png')}}"></a></li>
                            <li class="paypal"><a href="#"><img alt="payment" title="payment" src="{{asset('assets\frontend\image\catalog\p2.png')}}"></a></li>
                            <li class="discover"><a href="#"><img alt="payment" title="payment" src="{{asset('assets\frontend\image\catalog\p3.png')}}"></a></li>
                            <li class="mastercard"><a href="#"><img alt="payment" title="payment" src="{{asset('assets\frontend\image\catalog\p4.png')}}"></a></li>
                            <li class="bit-coin"><a href="#"><img alt="payment" title="payment" src="{{asset('assets\frontend\image\catalog\p5.png')}}"></a></li>
                            <li class="bit-coin"><a href="#"><img alt="payment" title="payment" src="{{asset('assets\frontend\image\catalog\p6.png')}}"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
    <script>
        // $(document).ready(function() {
        //     if ($(window).width() > 767) {
        //         $("#tmzoom").elevateZoom({
        //
        //             gallery:'additional-carousel',
        //             //inner zoom
        //
        //             zoomType : "inner",
        //             cursor: "crosshair"
        //
        //             /*//tint
        //
        //             tint:true,
        //             tintColour:'#F90',
        //             tintOpacity:0.5
        //
        //             //lens zoom
        //
        //             zoomType : "lens",
        //             lensShape : "round",
        //             lensSize : 200
        //
        //             //Mousewheel zoom
        //
        //             scrollZoom : true*/
        //
        //
        //         });
        //         var z_index = 0;
        //         $(document).on('click', '.thumbnail', function () {
        //             $('.thumbnails').magnificPopup('open', z_index);
        //             return false;
        //         });
        //
        //         $('.additional-carousel a').click(function() {
        //             var smallImage = $(this).attr('data-image');
        //             var largeImage = $(this).attr('data-zoom-image');
        //             var ez =   $('#tmzoom').data('elevateZoom');
        //             $('.thumbnail').attr('href', largeImage);
        //             ez.swaptheimage(smallImage, largeImage);
        //             z_index = $(this).index('.additional-carousel a');
        //             return false;
        //         });
        //
        //     }else{
        //         $(document).on('click', '.thumbnail', function () {
        //             $('.thumbnails').magnificPopup('open', 0);
        //             return false;
        //         });
        //     }
        // });
        $(document).ready(function() {
            $('.thumbnails').magnificPopup({
                delegate: 'a.elevatezoom-gallery',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-with-zoom',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                },
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function(item) {
                        return item.el.attr('title');
                    }
                }
            });
        });

</script>

</body>
</html>
