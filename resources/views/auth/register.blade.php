@extends('layouts.master')
@section('body')
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#">User Registration</a></li>
    </ul>
    <hr>
    <div class="">
        <!-- <div class="box-heading"><span>Our Products</span></div> -->
        <div id="tabs" class="htabs">
            @if(session()->has('message'))
                <p class="alert alert-success">
                    {{session()->get('message')}}
                </p>
            @endif
            <div class="tab-title-main">
                <ul class="etabs nav nav-tabs">
                    <li class="nav-item active">
                        <a href="#" data-toggle="tab" class="nav-link">User Registeration</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <aside id="column-left" class="col-md-3">
                <div class="box">
                    <div class="box-heading">Categories</div>
                    <div class="box-content">
                        <ul class="box-category treeview-list treeview">
                            @foreach($categories->where('parent_id',0) as $cat)
                                <li>
                                    <a href="#" >{{$cat->name}}</a>
                                    @if($cat->childs->count()>0)
                                        <ul>
                                            @foreach($cat->childs as $submenu)
                                                <li>
                                                    <a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <span class="latest_default_width" style="display:none; visibility:hidden"></span>
                <div class="swiper-viewport">
                    <div id="banner0" class="swiper-container  single-banner ">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide"><a href="{{route('home')}}"><img src="assets/frontend/image/shop_now.jpg" alt="leftbanner" class="img-responsive" /></a></div>
                        </div>
                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </aside>
            <div id="content" class="col-sm-9">
                <h1>Wellcome</h1>
                <p>If you already have an account with us, please login at the <a class="text-danger" href="{{ route('login') }}">login page</a>.</p>
                <form method="POST" action="{{ route('register') }}" class="needs-validation" novalidate>
                    @csrf
                    <fieldset id="account">
                        <legend>Your Personal Details</legend>
                        <div class="form-group required" style="display:  none ;">
                            <label class="col-sm-2 control-label">Customer Group</label>
                            <div class="col-sm-10">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="customer_group_id" value="1" checked="checked">Default
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-firstname"><span style="color:#0d0c0c;">Full Name:</span></label>
                            <div class="col-sm-10">
                                <input id="" type="text" class="form-control   @error('fullname') is-invalid @enderror" name="fullname" value="{{ old('fullname') }}" autocomplete="fullname" autofocus placeholder="Full Name" required>
                                @error('fullname')
                                <span class="invalid-feedback" role="">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group required" >
                            <label class="col-sm-2 control-label" for="input-lastname " style="margin-top: 12px;" ><span style="color:#0d0c0c;">Email:</span></label>
                            <div class="col-sm-10">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" placeholder="E-Mail" required style="margin-top: 12px;">
                                @error('email')
                                <span class="invalid-feedback" role="">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-email" style="margin-top: 12px;"><span style="color:#0d0c0c;">Phone:</span></label>
                            <div class="col-sm-10">
                                <input id="user_contact" type="tel" class="form-control @error('user_contact') is-invalid @enderror" name="user_contact" value="{{ old('user_contact') }}"  autocomplete="user_contact" placeholder="Phone" required style="margin-top: 12px;" >
                                @error('user_contact')
                                <span class="invalid-feedback" role="">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="" style="margin-top: 12px;"><span style="color:#0d0c0c;">City:</span></label>
                            <div class="col-sm-10">
                                <input id="city" type="text" class="form-control" name="city" placeholder="City" style="margin-top: 12px;">
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-password" style="margin-top: 12px;"><span style="color:#0d0c0c;">Address:</span></label>
                            <div class="col-sm-10">
                                <textarea id="address" type="" class="form-control" name="address" placeholder="address" style="margin-top: 12px;"></textarea>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Your Password</legend>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-confirm" style="margin-top: 12px;"><span style="color:#0d0c0c;">Password:</span></label>
                            <div class="col-sm-10">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password" placeholder="Password" required style="margin-top: 12px;">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-confirm" style="margin-top: 12px;"><span style="color:#0d0c0c;">Re-type:</span></label>
                            <div class="col-sm-10">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" autocomplete="new-password" placeholder="Confirm Password" required style="margin-top: 12px;">
                            </div>
                        </div>
                    </fieldset>
                    <div class="buttons">
                        <div class="pull-"> <a href="" class="agree"><b class="text-danger"></b></a>
                            <button type="submit" class="btn btn-primary">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
