@extends('layouts.master')
@section('body')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h2>{{ __('Reset Password') }}</h2>
                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}" name="form" >
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control  @error('password') is-invalid @enderror" name="password_confirmation" autocomplete="new-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4">
                                <button type="submit" class="btn btn-success" onsubmit="return resetForm()">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{--    <script>--}}
{{--        function resetForm() {--}}
{{--            var email = document.forms["form"]["email"];--}}
{{--            var password = document.forms["form"]["password"];--}}
{{--            var confirmPassword = document.froms["from"]["password_confirmation"];--}}
{{--            if (email.value == ""){--}}
{{--                window.alert("Please enter your email");--}}
{{--                email.focus();--}}
{{--                return false;--}}
{{--            }--}}
{{--            if (password.value == ""){--}}
{{--                window.alert("Please enter the password");--}}
{{--                password.focus();--}}
{{--                return false;--}}
{{--            }--}}
{{--            if (confirmPassword == ""){--}}
{{--                window.alert("Please enter the confirm password");--}}
{{--                confirmPassword.focus();--}}
{{--                return false;--}}
{{--            }--}}
{{--        }--}}
{{--    </script>--}}
@endsection
