@extends('layouts.master')
@section('body')
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Reset Password</a></li>
    </ul>
    <hr>
    <div class="">
        <!-- <div class="box-heading"><span>Our Products</span></div> -->
        <div id="tabs" class="htabs">
            @if(session()->has('message'))
                <p class="alert alert-success">
                    {{session()->get('message')}}
                </p>
            @endif
            <div class="tab-title-main">
                <ul class="etabs nav nav-tabs">
                    <li class="nav-item active">
                        <a href="#" data-toggle="tab" class="nav-link">Reset Password</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
{{--    <aside id="column-left" class="col-md-3">--}}
{{--        <span class="latest_default_width" style="display:none; visibility:hidden"></span>--}}
{{--        <div class="swiper-viewport">--}}
{{--            <div id="banner0" class="swiper-container  single-banner ">--}}
{{--                <div class="swiper-wrapper">--}}
{{--                    <div class="swiper-slide"><a href="{{route('home')}}"><img src="assets/frontend/image/shop_now.jpg" alt="leftbanner" class="img-responsive" /></a></div>--}}
{{--                </div>--}}
{{--                <!-- If we need pagination -->--}}
{{--                <div class="swiper-pagination"></div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </aside>--}}
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4">
            <div class="card">
                <h2>{{ __('Reset Password') }}</h2>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right" style="color: #0b0b0b">{{ __('E-Mail Address:') }}</label>
                            <div class="col-md-">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Enter Email " value="{{ old('email') }}" autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</div>
@endsection
