@extends('layouts.master')
@section('body')
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#">User Login</a></li>
    </ul>
    <hr>
    <div id="tabs" class="htabs">
        @if(session()->has('message'))
            <p class="alert alert-success">
                {{session()->get('message')}}
            </p>
        @endif
        <div class="tab-title-main">
            <ul class="etabs nav nav-tabs">
                <li class="nav-item active">
                    <a href="#" data-toggle="tab" class="nav-link">User Login</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <aside id="column-left" class="col-sm-3 hidden-xs">
                <div class="box">
                    <div class="box-heading">Categories</div>
                    <div class="box-content">
                        <ul class="box-category treeview-list treeview">
                            @foreach($categories->where('parent_id',0) as $cat)
                                <li>
                                    <a href="#" >{{$cat->name}}</a>
                                    @if($cat->childs->count()>0)
                                        <ul>
                                            @foreach($cat->childs as $submenu)
                                                <li>
                                                    <a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <span class="latest_default_width" style="display:none; visibility:hidden"></span>
                <div class="swiper-viewport">
                    <div id="banner0" class="swiper-container  single-banner ">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide"><a href="{{route('home')}}"><img src="assets/frontend/image/shop_now.jpg" alt="leftbanner" class="img-responsive" /></a></div>
                        </div>
                    </div>
                </div>
            </aside>
            <div class="col-sm-4">
                <div class="well">
                    <h2>New Customer</h2>
                    <p><strong>Register</strong></p>
                    <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                    <a href="{{ route('register') }}" class="btn btn-primary">Continue</a></div>
            </div>
            <div class="col-sm-4" style="margin-left: 70px">
                <h2>Returning Customer</h2>
                <p><strong>I am a returning customer</strong></p>
                <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate>
                    @csrf
                    <div class="row form-group">
                        <label for="email" class="col-md-4 col-form-label text-md-right" style="color: #0b0b0b"><strong>E-Mail Address</strong></label>
                        <div class="col-md-">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"  placeholder="Enter Email"  required >
                            @error('email')
                            <span class="invalid-feedback" role="alert" class="input-group-addon">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right" style="margin-left: -20px ; color: #0b0b0b"><strong>Password</strong></label>
                        <div class="col-md-">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Enter Password" required >
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-0 pull-left">
                        <div class="">
                            @if (Route::has('password.request'))
                                <a class=" text-danger" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}<br><br>
                                </a>
                            @endif
                            <div class="pull-left">
                                <button type="submit" id="#btnSubmit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
