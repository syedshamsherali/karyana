@extends('layouts.master')
@section('title', 'Karyana')
@section('body')
    <script>
        function quickbox() {
            if ($(window).width() > 767) {
                $('.quickview').magnificPopup({
                    type: 'iframe',
                    delegate: 'a',
                    preloader: true,
                    tLoading: 'Loading image #%curr%...',
                });
            }
        }
        jQuery(document).ready(function() {
            quickbox();
        });
        jQuery(window).resize(function() {
            quickbox();
        });

    </script>

    <div id="common-home">
        <div class="content">
            <div id="content1" class="">
                <div class="main-slider">
                    <div id="spinner"></div>
                    <div class="swiper-viewport">
                        <div id="slideshow0" class="swiper-container" style="opacity: 1;">
                            <div class="swiper-wrapper">
                                @foreach($shops as $shop)
                                <div class="swiper-slide text-center"><a href="{{route('shopDetail',[$shop->id])}}"><img src="{{url('/images/'.$shop->logo)}}" alt="mainbanner2" class="img-responsive" /></a></div>
{{--                                <div class="swiper-slide text-center"><a href="#"><img src="{{asset('assets/frontend/image/cache/catalog/Main-Banner-3-1903x845.jpg')}}" alt="mainbanner2" class="img-responsive" /></a></div>--}}
{{--                                <div class="swiper-slide text-center"><a href="#"><img src="{{asset('assets/frontend/image/cache/catalog/Main-Banner-1-1903x845.jpg')}}" alt="mainbanner2" class="img-responsive" /></a></div>--}}
{{--                                <div class="swiper-slide text-center"><a href="#"><img src="{{asset('assets/frontend/image/cache/catalog/Main-Banner-2-1903x845.jpg')}}" alt="mainbanner1" class="img-responsive" /></a></div>--}}
                                @endforeach
                            </div>
                        </div>
                        <div class="swiper-pagination slideshow0"></div>
                        <div class="swiper-pager">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>

                </div>
                <script type="text/javascript">
                    $('#slideshow0').swiper({
                        effect: 'fade',
                        slidesPerView: 1,
                        pagination: '.slideshow0',
                        paginationClickable: true,
                        nextButton: '.swiper-button-next',
                        prevButton: '.swiper-button-prev',
                        spaceBetween: 0,
                        autoplay: 5000,
                        autoplayDisableOnInteraction: true,
                        loop: true,
                        paginationType: 'bullets',
                        paginationBulletRender: function(index, className) {
                            return '<span class="swiper-pagination-bullet">0' + (className + 1) + '</span>';
                        }
                    });

                </script>

                <script type="text/javascript">
                    // Can also be used with $(document).ready()
                    $(window).load(function() {
                        $("#spinner").fadeOut("slow");
                    });

                </script>
            </div>
            <div class="container">
                <div class="row">

                </div>
            </div>
        </div>
    </div>
        <div class="content-bottom">
            <div class="container">
                <div id="content">
                    <div id="aei_cmscategory">
                        <div class="container">
                            <div class="row">
                                <div class="ax-title">
                                    <div class="box-heading"><span>Top Featured Karyana Shops</span></div>
                                </div>
                                <div class="aei-cmscategory-inner">
                                    <div class="category-cms-slider">
                                        @foreach($shops as $shop)
                                        <div class="category-cms">
                                            <div class="category-cms-inner">
                                                <a href="{{route('shopDetail',[$shop->id])}}" class="category-cms-img"><img src="{{url('/images/'.$shop->logo)}}" alt="cat" height="175px" width="175px" style="border-radius: 50%"></a>
                                                <a href="{{route('shopDetail',[$shop->id])}}" class="category-cms-title text-danger">{{$shop->name}}</a>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="arrows" id="category-cms-arrow"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hometab box">
                        <!-- <div class="box-heading"><span>Our Products</span></div> -->
                        <div class="container">
                            <div id="tabs" class="htabs">
                                <div class="tab-title-main">
                                    <ul class='etabs nav nav-tabs'>
                                        <li class='nav-item'>
                                            <a href="#tab-latest" data-toggle="tab" class="nav-link">Latest</a>
                                        </li>
                                        <li class='nav-item active'>
                                            <a href="#tab-bestseller" data-toggle="tab" class="nav-link">Bestseller</a>
                                        </li>

                                        <li class='nav-item'>
                                            <a href="#tab-special" data-toggle="tab" class="nav-link">Special</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div id="tab-bestseller" class="tab-pane active">
                                        <div class="box">
                                            <div class="box-content">
                                                <div id="bestseller-arrows" class="arrows"> </div>
                                                <div class="box-product bestseller-slider" id="bestseller-slider">
                                                    @for ($i = 0; $i <5; $i++)
                                                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <div>
                                                            <div class="product-block product-thumb transition">
                                                                <div class="product-block-inner">

                                                                    <div class="image">
{{--                                                                        @dd($products);--}}
{{--                                                                        @dd($products)--}}
                                                                        <a href="{{route('productDetail',[$products[$i]->id])}}">
                                                                            <img src="{{url('/images/'.$products[$i]->image)}}" title="{{$products[$i]->name}}" alt="{{$products[$i]->name}}" class="img-responsive reg-image" />
                                                                            <img class="img-responsive hover-image" src=""  />
                                                                        </a>
                                                                    </div>
                                                                    <div class="caption">
                                                                        <span class="rating">
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                                        </span>
                                                                        <h4><a href="{{route('productDetail',[$products[$i]->id])}}">{{$products[$i]->name}}</a></h4>
                                                                        <p class="price">
                                                                            Rs.{{$products[$i]->price}}
                                                                        </p>
                                                                        <div class="button-group">
                                                                            <a onclick="addToCart({{$products[$i]->id}})">
                                                                                <button  type="button" data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart listaddtocart">
                                                                                    Add to Cart
                                                                                </button>
                                                                            </a>
                                                                            <button class="wishlist" value="{{$products[$i]->id}}" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List "><i class="fa fa-heart"></i></button>
                                                                            <button class="compare" type="button" value="{{$products[$i]->id}}" data-toggle="tooltip" data-placement="top" title="Compare this Product "><i class="fa fa-exchange"></i></button>

                                                                        </div>
                                                                        <div class="button-group">
                                                                            <a onclick="addToCart({{$products[$i]->id}})">
                                                                                <button  type="button" data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart listaddtocart">
                                                                                    Add to Cart
                                                                                </button>
                                                                            </a>
                                                                            <button class="wishlist" value="{{$products[$i]->id}}" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List "><i class="fa fa-heart"></i></button>
                                                                            <button class="compare" type="button" value="{{$products[$i]->id}}" data-toggle="tooltip" data-placement="top" title="Compare this Product "><i class="fa fa-exchange"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endfor
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-latest" class=" tab-pane">
                                        <div class="box">
                                            <div class="box-content">
                                                <div id="latest-arrows" class="arrows"> </div>
                                                <div class="tablatest-slider" id="tablatest-slider">
                                                    @for ($i = 5; $i <10; $i++)
                                                        <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <div>
                                                            <div class="product-block product-thumb transition">
                                                                <div class="product-block-inner">
                                                                    <div class="image">
                                                                        <a href="{{route('productDetail',[$products[$i]->id])}}">
                                                                            <img src="{{url('/images/'.$products[$i]->image)}}" title="{{$products[$i]->name}}" alt="{{$products[$i]->name}}" class="img-responsive reg-image" />
                                                                            <img class="img-responsive hover-image" src="{{url('/images/'.$products[$i]->image)}}" title="{{$products[$i]->name}}" alt="{{$products[$i]->name}}" />
                                                                        </a>
                                                                    </div>
                                                                    <div class="caption">
                                                                        <span class="rating">
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                                        </span>
                                                                        <h4><a href="{{route('productDetail',[$products[$i]->id])}}">{{$products[$i]->name}}</a></h4>

                                                                        <p class="price">
                                                                            Rs.{{$products[$i]->price}}
                                                                        </p>

                                                                        <div class="button-group">
                                                                            <a onclick="addToCart({{$products[$i]->id}})">
                                                                                <button  type="button" data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart listaddtocart">
                                                                                    Add to Cart
                                                                                </button>
                                                                            </a>
                                                                            <button class="wishlist" value="{{$products[$i]->id}}" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List "><i class="fa fa-heart"></i></button>
                                                                            <button class="compare" type="button" value="{{$products[$i]->id}}" data-toggle="tooltip" data-placement="top" title="Compare this Product "><i class="fa fa-exchange"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        @endfor
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-special" class="tab-pane">
                                        <div class="box">
                                            <div class="box-content">
                                                <div id="special-arrows" class="arrows"> </div>
                                                <div class="box-product special-slider" id="special-slider">
                                                    @for($i = 10; $i <15; $i++)
                                                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <div>
                                                            <div class="product-block product-thumb transition">
                                                                <div class="product-block-inner">
                                                                    <div class="image">
                                                                        <a href="{{route('productDetail',[$products[$i]->id])}}">
                                                                            <img src="{{url('/images/'.$products[$i]->image)}}" title="{{$products[$i]->name}}" alt="{{$products[$i]->name}}" class="" />
                                                                            <img class="img-responsive hover-image" src="{{url('/images/'.$products[$i]->image)}}" title="{{$products[$i]->name}}" alt="{{$products[$i]->name}}" />
                                                                        </a>
                                                                        <div class="saleback">
                                                                            <span class="saleicon sale">Sale</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="caption">
                                                                        <span class="rating">
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                                        </span>
                                                                        <h4><a href="{{route('productDetail',[$products[$i]->id])}}">{{$products[$i]->name}}</a></h4>

                                                                        <p class="price">
                                                                            <p>Rs.{{$products[$i]->price}}</p>
                                                                        </p>
                                                                        <div class="button-group">
                                                                            <a onclick="addToCart({{$products[$i]->id}})">
                                                                                <button  type="button" data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart listaddtocart">
                                                                                    Add to Cart
                                                                                </button>
                                                                            </a>
                                                                            <button class="wishlist" value="{{$products[$i]->id}}" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List "><i class="fa fa-heart"></i></button>
                                                                            <button class="compare" type="button" value="{{$products[$i]->id}}" data-toggle="tooltip" data-placement="top" title="Compare this Product "><i class="fa fa-exchange"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        @endfor
                                                </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="aei_cmsbanner">
                        <div class="ax-title">
                            <div class="box-heading"><span>Why You Choose Us</span></div>
                        </div>
                        <div class="container">
                            <div class="aeibannerblock">
                                <div class="banner-image">
                                    <a href="#"><img src="{{asset('assets/frontend/image/catalog/banner1.jpg')}}" alt="banner1"></a>
                                </div>
                                <div class="banner-main">
                                    <div class="banner-description">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since
                                        the 1500s, when an unknown printer.
                                    </div>
                                    <div class="banner-inner first">
                                        <div class="banner-inner-main">
                                            <a href="#" class="banner-icon"><img src="{{asset('assets/frontend/image/catalog/icon1.png')}}" alt="icon1"></a>
                                            <a href="#" class="banner-text-first">100% Clean &amp; Fresh</a>
                                        </div>
                                    </div>
                                    <div class="banner-inner second">
                                        <div class="banner-inner-main">
                                            <a href="#" class="banner-icon"><img src="{{asset('assets/frontend/image/catalog/icon2.png')}}" alt="icon2"></a>
                                            <a href="#" class="banner-text-first">Rich In Nutrients</a>
                                        </div>
                                    </div>
                                    <div class="banner-inner third">
                                        <div class="banner-inner-main">
                                            <a href="#" class="banner-icon"><img src="{{asset('assets/frontend/image/catalog/icon3.png')}}" alt="icon3"></a>
                                            <a href="#" class="banner-text-first">No pesticides</a>
                                        </div>
                                    </div>
                                    <div class="banner-inner fourth">
                                        <div class="banner-inner-main">
                                            <a href="#" class="banner-icon"><img src="{{asset('assets/frontend/image/catalog/icon4.png')}}" alt="icon4"></a>
                                            <a href="#" class="banner-text-first">Cheapest price on market</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="banner-offer-cms">
                            <div class="banner-offer-inner">
                                <span class="text1">Today Offer : <span>70% Off</span> all organic for the next 02 hours only.</span>
                                <a href="#" class="offer-image"><img src="{{asset('assets/frontend/image/catalog/offer-img.png')}}" alt="offer-img"></a>
                                <div class="offer-description">
                                    <span class="text2">Use Coupon Code</span>
                                    <a href="#" class="text3">"ORGANICFOOD"</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        <!--
                        $('#Tab_Category_Slider.category_tab .tab-content > .tab-pane.active .categoryslider').slick({
                            appendArrows: $('#category-arrows'),
                            dots: false,
                            autoplay: false,
                            infinite: false,
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            responsive: [{
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1,
                                    infinite: true,
                                }
                            },
                                {
                                    breakpoint: 992,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 2
                                    }
                                },
                                {
                                    breakpoint: 768,
                                    settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 2
                                    }
                                },
                                {
                                    breakpoint: 480,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1
                                    }
                                }
                            ]
                        });

                        function zcoyshkiqwloadAjaxData(category_id) {
                            var unavailabledates = {
                                "name": "categorytab",
                                "heading": "category tab",
                                "category": "",
                                "product_category": ["20", "18", "57"],
                                "category_limit": "5",
                                "product_limit": "5",
                                "width": "328",
                                "height": "324",
                                "status": "1"
                            };
                            var dataToSend = {
                                'category_id': category_id,
                                'setting': unavailabledates
                            };
                            $.ajax({
                                type: 'POST',
                                data: dataToSend,
                                url: 'index.php?route=extension/module/category_tab/ajaxloaddata',
                                beforeSend: function() {
                                    $("#zcoyshkiqw").html('<div style="text-align:center;"></div>');
                                    //http://www.bigtravelsghana.com/img/images/ajax-loader.gif
                                },
                                success: function(data) {
                                    $('#Tab_Category_Slider').replaceWith(data);
                                    //alert(data);
                                }
                            });
                        }

                        -->
                    </script>
                    <script type="text/javascript">
                        // Can also be used with $(document).ready()
                        $(document).ready(function() {
                            $("#tab_spinner");
                        });

                    </script>
                    <div class="box brands">
                        <div class="box-heading">Brand logo</div>
                        <div id="brand-slider" class="brands-slider">
                            <div class="container">
                                <!-- container -->
                                <div class="row">
                                    <div id="brand-arrows" class="arrows"> </div>
                                    <div class="brands-slider" id="brandslider">
                                        <div class="slider-item">
                                            <div class="product-block">
                                                <div class="product-block-inner">
                                                    <img src="{{asset('assets/frontend/image/cache/catalog/brand-4-200x120.png')}}" alt="RedBull" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item">
                                            <div class="product-block">
                                                <div class="product-block-inner">
                                                    <img src="{{asset('assets/frontend/image/cache/catalog/brand-3-200x120.png')}}" alt="NFL" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item">
                                            <div class="product-block">
                                                <div class="product-block-inner">
                                                    <img src="{{asset('assets/frontend/image/cache/catalog/brand-2-200x120.png')}}" alt="Sony" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item">
                                            <div class="product-block">
                                                <div class="product-block-inner">
                                                    <img src="{{asset('assets/frontend/image/cache/catalog/brand-8-200x120.png')}}" alt="Coca Cola" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item">
                                            <div class="product-block">
                                                <div class="product-block-inner">
                                                    <img src="{{asset('assets/frontend/image/cache/catalog/brand-4-200x120.png')}}" alt="Burger King" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item">
                                            <div class="product-block">
                                                <div class="product-block-inner">
                                                    <img src="{{asset('assets/frontend/image/cache/catalog/brand-6-200x120.png')}}" alt="Canon" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item">
                                            <div class="product-block">
                                                <div class="product-block-inner">
                                                    <img src="{{asset('assets/frontend/image/cache/catalog/brand-5-200x120.png')}}" alt="Harley Davidson" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item">
                                            <div class="product-block">
                                                <div class="product-block-inner">
                                                    <img src="{{asset('assets/frontend/image/cache/catalog/brand-4-200x120.png')}}" alt="Dell" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item">
                                            <div class="product-block">
                                                <div class="product-block-inner">
                                                    <img src="{{asset('assets/frontend/image/cache/catalog/brand-3-200x120.png')}}" alt="Disney" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item">
                                            <div class="product-block">
                                                <div class="product-block-inner">
                                                    <img src="{{asset('assets/frontend/image/cache/catalog/brand-2-200x120.png')}}" alt="Starbucks" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-item">
                                            <div class="product-block">
                                                <div class="product-block-inner">
                                                    <img src="{{asset('assets/frontend/image/cache/catalog/brand-1-200x120.png')}}" alt="Nintendo" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboards.projectjs')
@endsection
