@extends('layouts.dashboard')

@section('title','Category List')
<meta name="csrf-token" content="{{csrf_token()}}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<style>
    i span {
        visibility: hidden;
    }
    .float-right span {
        visibility: hidden;
    }
    span span {
        visibility: hidden;
    }
</style>
@section('body')
    <h2 class="h3 mb-4 offset-4 "> Category Table View </h2>
{{--<div class="col-md-6 float-right">--}}
{{--    <input type="text" class="form-control" placeholder="Main Category" id="main-category"><div class="pt-2 pb-2 float-right"><input type="button" class="btn btn-success" value="Create Category" onclick="mainCategory()"></div>--}}
{{--</div>--}}
<table id="basic" border="1" class="table text-uppercase table-hover text-dark" >
    @foreach($category as $categories)
    <tr data-node-id="{{$categories->id }}">
        <td>
            <span id="{{$categories->id }}">{{ $categories->name }}</span>
            <div class="float-right">
                <i id="{{$categories->id }}id"></i>
                <i class="fa fa-plus-square" style="display: none"></i>
                <i class="fa fa-plus-square" onclick="addCategory({{$categories->id }})" ></i>
                <i class="fa fa-edit" onclick="editRow({{$categories->id }})"></i>
                <i @if (count($categories->childs))
                    style="visibility:hidden"
                    @endif
                    class="fa fa-trash" onclick="ConfirmDelete({{$categories->id }})"></i>
            </div>
        </td>
    </tr>
        @if(count($categories->childs))
            @include('categories.manageChild',['childs' => $categories->childs])
        @endif
            @endforeach
</table>
    @include('dashboards.projectjs')

    <script src="{{asset('assets\backend\admindash\treeview\jquery-simple-tree-table.js')}}"></script>
    <script>
        $('#basic').simpleTreeTable({
            expander: $('#expander'),
            collapser: $('#collapser'),
            store: 'session',
            storeKey: 'simple-tree-table-basic'
        });
    </script>

@endsection
