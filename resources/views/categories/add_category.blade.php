@extends('layouts.dashboard')
@section('title','Add Product')
@section('body')
    <div class="row col-md-12">
        <div class="col-md-6">
    <h1 class="h3 mb-4 align-content-center ">Add Category</h1>
    <!-- Page Heading -->
    <form action="{{route('add_category')}}" method="post" class="needs-validation"  novalidate>
        {{csrf_field()}}
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
            </div>
        @endif
        <div class="form-group">
            <div class="mb-3">
            <label for="validationCustom01" class="">Main Category</label>
            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="validationCustom01" placeholder="Main Category" value="" required>
            <div class="invalid-feedback">
                Category Name Required
            </div>
                @error('name')
                <span class="invalid-feedback" role="alert" class="input-group-addon">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
         </div>
        <div class="form-group">
            <button class="btn text-light" style="background: #74ab26" type="submit">Add Category</button>
        </div>
    </form>
        </div>
    </div>
    @include('dashboards.projectjs')
    <script>
        $('#timepicker').timepicker({
            uiLibrary: 'bootstrap4'
        });

        $('#timepicker1').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
@endsection
