
        @foreach($childs as $child)
        <tr data-node-id="{{$child->id}}" data-node-pid="{{$child->parent_id}}">

                <td>
                    <span id="{{$child->id }}">{{ $child->name }}</span>
                    <div class="float-right">
                        <i id="{{$child->id }}id"></i>
                        <i class="fas fa-plus" style="display: none"></i>
                        <i class="fa fa-plus-square" onclick="addCategory({{$child->id }})" ></i>
                        <i class="fa fa-edit" onclick="editRow({{$child->id }})"></i>
                        <i @if (count($child->childs))
                            style = "visibility:hidden"

                        @endif
                        class="fa fa-trash" onclick="ConfirmDelete({{$child->id }})"></i>
                    </div>
                </td>
                   @if(count($child->childs))
                        @include('categories.manageChild',['childs' => $child->childs])
                    @endif
        </tr>
    @endforeach





