@extends('layouts.dashboard')
@section('title','Order List')
@section('body')
    <div id="mydiv"> </div>

    <div id="content-wrapper">
        <div class="container-fluid">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
        @endif
        <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        <b>Approve Order</b>
                    </div>
                    <div class="card-body">
                        <div id="no-more-tables">
                            <table class="table table-responsive table-bordered text-center" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead class="bg-grey">
                                <tr class="">
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Customer Address</th>
                                    <th>Customer Phone Number</th>
                                    <th>Order Date</th>
                                    <th>Total Amount</th>
                                    <th>Payment Status</th>
                                    <th>Order Detail</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>



                                    @foreach($orders as $order)
                                        <tr>
                                            <td >{{$order->User['fullname']}}</td>
                                            <td >{{$order->User['email']}}</td>
                                            <td >{{$order->User['address']}}</td>
                                            <td >{{$order->User['user_contact']}}</td>
                                            <td >{{$order->updated_at->format('d M,Y') }}</td>
                                            <td >{{$order->total_ammount}}</td>
                                            <td >{{$order->payment_status }}</td>

                                            <td >
                                                <i onclick="orderDetail({{$order->id}})"  class="fa fa-eye detail bd-example-modal-lg"  data-toggle="modal" data-target="#exampleModal"></i>
                                            </td>

                                            <td>
                                                <div class="dropdown">
                                                <span  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-h"></i>
                                                </span>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                        <a href="{{route('discardOrder',[$order->id])}}" class="btn btn-danger my-2" onclick="return confirm('Are you sure you want to discard this order?');">Discard</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{--                            <div class="card-footer small text-muted">Last Updated--}}
                    {{--                                at {{$orders->updated_at->format('d M, Y h:i A')}}--}}
                    {{--                            </div>--}}
                </div>
            </div>
        </div>
    </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Order Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Shop Name</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody id="order-detail">
                        <tr>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        function orderDetail (id) {

            $('#order-detail').empty();
            $.ajax({
                type : 'get',
                data : {'id':id},
                url  : '{{route('getOrderDetailsRider')}}',
                success : function (data) {
                    // $('#order-detail').empty();

                    $.each(data,function (i,value) {
                        var tr=$("<tr/>");

                        tr.append($("<td/>",{
                            text:value.product_id[0]
                        }))

                            .append($("<td/>",{
                                text:value.order
                            }))
                            .append($("<td/>",{
                                text:value.quantity
                            }))
                            .append($("<td/>",{
                                text:value.amount
                            }))




                        $('#order-detail').append(tr);
                        // $('#order-detail').empty();

                    })
                    console.log(data);
                }
            });
        }
    </script>
@endsection
