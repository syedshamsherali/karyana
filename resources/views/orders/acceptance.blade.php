@extends('layouts.riderdashboardlayout')
@section('title','Accept Order')
@section('body')
    <div id="mydiv"> </div>

    <div id="content-wrapper">
        <div class="container-fluid">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
        @endif
        <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        <b>Accept Order</b>
                    </div>
                    <div class="card-body">
                        <div id="no-more-tables">
                            <table class="table  table-bordered text-center" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead class="bg-grey ">
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Customer Address</th>
                                    <th>Customer Phone Number</th>
                                    <th>Order Date</th>
                                    <th>Total Amount</th>
                                    <th>Order Detail</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if($orders->count() > 0)
                                    @foreach($orders as $order)
                                        <tr>
                                            <td >{{$order->User['fullname']}}</td>
                                            <td >{{$order->User['email']}}</td>
                                            <td >{{$order->User['address']}}</td>
                                            <td >{{$order->User['user_contact']}}</td>
                                            <td >{{$order->created_at->format('d M, Y')}}</td>
                                            <td >{{$order->total_ammount}}</td>

                                            <td>
                                                <i onclick="orderDetail({{$order->id}})"  class="fa fa-eye detail bd-example-modal-lg"  data-toggle="modal" data-target="#exampleModal"></i>                                                                             </button>
                                            </td>

                                            <td>
                                                <a href="{{route('riderAcceptOrder',[$order->id])}}" class="btn btn-primary my-2">Accept Order</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
{{--                    <div class="card-footer small text-muted">Last Updated--}}
{{--                        at {{$order->updated_at->format('d M, Y h:i A')}}--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
    </div>


                    {{--                        order detail modal starts here--}}


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Order Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Shop Name</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody id="order-detail"><tr>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        function orderDetail (id) {
            $('#order-detail').empty();
            $.ajax({
                type : 'get',
                data : {'id':id},
                url  : '{{route('getOrderDetailsRider')}}',
                success : function (data) {
                    // $('#order-detail').empty();
                    $.each(data,function (i,value) {
                        var tr=$("<tr/>");
                        tr.append($("<td/>",{
                            text:value.product_id[0]
                        }))

                            .append($("<td/>",{
                                text:value.order
                            }))
                            .append($("<td/>",{
                                text:value.quantity
                            }))
                            .append($("<td/>",{
                                text:value.amount
                            }))
                         $('#order-detail').append(tr);
                                                // $('#order-detail').empty();

                    })
                    console.log(data);
                }
            });
        }
    </script>

@endsection

{{--@extends('layouts.riderdashboardlayout')--}}

{{--@section('title','Accept Order')--}}
{{--@section('body')--}}
{{--    <div class="col-md-12">--}}
{{--        <div class="col-md-12">--}}
{{--            <h1 class="h3 mb-4" >Accept Order</h1>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    @if(session('message'))--}}
{{--        <p class="alert alert-success">{{session('message')}}</p>--}}
{{--    @endif--}}
{{--        @dd($orderDetails[0]->)--}}

{{--    <div class="row">--}}

{{--        @foreach($orders as $order)--}}


{{--            <div class="col-sm-6">--}}
{{--                <div class="card text-center">--}}
{{--                    <div class="card-body">--}}
{{--                        <h5 class="card-title">Order Accept</h5>--}}
{{--                        <p class="card-text">Customer Name: {{$order->User['fullname']}} </p>--}}
{{--                        <p class="card-text">Customer Email:{{$order->User['email']}} </p>--}}
{{--                        <p class="card-text">Customer Address:{{$order->User['address']}} </p>--}}
{{--                        <p class="card-text">Customer Phone Number:{{$order->User['user_contact']}} </p>--}}
{{--                        <p class="card-text">Total Amount: Rs. {{$order->total_ammount}} </p>--}}

{{--                        <button type="button" onclick="orderDetail({{$order->id}})" class="btn btn-warning my-2" data-toggle="modal" data-target="#exampleModal">--}}
{{--                            Order Details--}}
{{--                        </button>--}}


{{--                        <script>--}}
{{--                            function orderDetail (id) {--}}

{{--                                $('#order-detail').empty();--}}
{{--                                $.ajax({--}}
{{--                                    type : 'get',--}}
{{--                                    data : {'id':id},--}}
{{--                                    url  : '{{route('getOrderDetailsRider')}}',--}}
{{--                                    success : function (data) {--}}
{{--                                        // $('#order-detail').empty();--}}

{{--                                        $.each(data,function (i,value) {--}}
{{--                                            var tr=$("<tr/>");--}}

{{--                                            tr.append($("<td/>",{--}}
{{--                                                text:value.product_id[0]--}}
{{--                                            }))--}}

{{--                                                .append($("<td/>",{--}}
{{--                                                    text:value.order--}}
{{--                                                }))--}}
{{--                                                .append($("<td/>",{--}}
{{--                                                    text:value.quantity--}}
{{--                                                }))--}}
{{--                                                .append($("<td/>",{--}}
{{--                                                    text:value.amount--}}
{{--                                                }))--}}




{{--                                            $('#order-detail').append(tr);--}}
{{--                                            // $('#order-detail').empty();--}}

{{--                                        })--}}
{{--                                        console.log(data);--}}
{{--                                    }--}}
{{--                                });--}}
{{--                            }--}}
{{--                        </script>--}}
{{--                        <a href="{{route('riderAcceptOrder',[$order->id])}}" class="btn btn-primary my-2">Accept Order</a>--}}




{{--                                            order detail modal starts here--}}


{{--                    <!-- Modal -->--}}
{{--                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--                            <div class="modal-dialog" role="document">--}}
{{--                                <div class="modal-content">--}}
{{--                                    <div class="modal-header">--}}
{{--                                        <h5 class="modal-title" id="exampleModalLabel">Order Details</h5>--}}
{{--                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                            <span aria-hidden="true">&times;</span>--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
{{--                                    <div class="modal-body">--}}
{{--                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">--}}
{{--                                            <thead>--}}
{{--                                            <tr>--}}
{{--                                                <th>Product Name</th>--}}
{{--                                                <th>Shop Name</th>--}}
{{--                                                <th>Quantity</th>--}}
{{--                                                <th>Amount</th>--}}
{{--                                            </tr>--}}
{{--                                            </thead>--}}

{{--                                            <tbody id="order-detail">--}}

{{--                                                <tr>--}}

{{--                                                </tr>--}}
{{--                                            </tbody>--}}
{{--                                        </table>--}}

{{--                                    </div>--}}
{{--                                    <div class="modal-footer">--}}
{{--                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                                                order detail modal ends here--}}




{{--                    </div>--}}

{{--                </div>--}}

{{--            </div>--}}




{{--        @endforeach--}}
{{--    </div>--}}
{{--@endsection--}}
