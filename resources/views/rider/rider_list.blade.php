@extends('layouts.dashboard')
@section('title','Rider List')
@section('body')
    <div id="content-wrapper">
        <div class="container-fluid">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
            @endif
            <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        <b>Karyana Riders</b>
                    </div>
                    <div class="card-body">
                        <div id="no-more-tables">
                            <table class="table table-responsive table-bordered text-center" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead class="bg-grey">
                                <tr class="">
                                    <th>Rider Name</th>
                                    <th>Rider Email</th>
                                    <th>User Contact</th>
                                    <th> City</th>
                                    <th> Address</th>
                                    <th>Vehicle Number</th>
                                    <th>Licence Number</th>
                                    <th>Joining Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($userlist as $riderlists)
                                    <tr>
                                        <td >{{$riderlists->User['fullname']}}</td>
                                        <td >{{$riderlists->User['email']}}</td>
                                        <td >{{$riderlists->User['user_contact']}}</td>
                                        <td >{{$riderlists->User['city']}}</td>
                                        <td >{{$riderlists->User['address']}}</td>
                                        <td >{{$riderlists->vehicle_number}}</td>
                                        <td >{{$riderlists->licence_number}}</td>
                                        <td >{{$riderlists->date_of_joining}}</td>
                                        <td>
                                            <div class="dropdown">
                                                <span  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-h"></i>
                                                </span>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a href="{{route('rider_edit',[$riderlists->User['id']])}}" class="btn btn-primary">Edit</a>
                                                    <a href="{{route('rider_delete',[$riderlists->User['id']])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer small text-muted">Last Updated
                        at {{$riderlists->updated_at->format('d M, Y h:i A')}}</div>
                </div>
            </div>
        </div>
    </div>
@endsection









{{--@extends('layouts.dashboard')--}}
{{--@section('title','Rider List')--}}
{{--@section('body')--}}
{{--    <h1 class="h3 mb-2 text-light">Rider List</h1>--}}

{{--    @if(session('message'))--}}
{{--        <p class="alert alert-success">{{session('message')}}</p>--}}
{{--    @endif--}}
{{--    <!-- DataTales Example -->--}}
{{--    @if (session('message'))--}}
{{--        <div class="alert alert-success" role="alert">--}}
{{--            {{ session('message') }}--}}
{{--        </div>--}}
{{--    @endif--}}
{{--    <div class="card shadow mb-4">--}}

{{--        <div class="card-body">--}}
{{--            <div class="table-responsive ">--}}
{{--                <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                                    <th> Rider Name</th>--}}
{{--                                    <th> Rider Email</th>--}}
{{--                                    <th> User Contact </th>--}}
{{--                                    <th> Address </th>--}}
{{--                                    <th> City </th>--}}
{{--                                    <th>Vehicle Number  </th>--}}
{{--                                    <th>Licence Number </th>--}}
{{--                                    <th>Joining Date</th>--}}
{{--                                    <th> Actions </th>--}}
{{--                                    <th> Update </th>--}}
{{--                                </tr>--}}
{{--                    </thead>--}}

{{--                    <tbody>--}}
{{--                    @dd($userlist)--}}
{{--                    @foreach($userlist as $riderlists)--}}
{{--                        @dd($riderlists->user_id)--}}
{{--                        <tr>--}}
{{--                                    <td>{{$riderlists->User['fullname']}}</td>--}}
{{--                                    <td>{{$riderlists->User['email']}}</td>--}}
{{--                            <td>{{$riderlists->User['user_contact']}}</td>--}}
{{--                            <td>{{$riderlists->User['city']}}</td>--}}
{{--                            <td>{{$riderlists->User['address']}}</td>--}}
{{--                                    <td>{{$riderlists->vehicle_number}}</td>--}}
{{--                                    <td>{{$riderlists->licence_number}}</td>--}}
{{--                                    <td>{{$riderlists->date_of_joining}}</td>--}}
{{--                            <td>--}}
{{--                                <div class="dropdown">--}}
{{--                                <span  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                    <i class="fas fa-ellipsis-h"></i>--}}
{{--                                </span>--}}
{{--                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">--}}
{{--                                        <a href="{{route('rider_edit',[$riderlists->User['id']])}}" class="btn btn-primary">Edit</a>--}}
{{--                                        <a href="{{route('rider_delete',[$riderlists->User['id']])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </td>--}}

{{--                                </tr>--}}
{{--                                    @endforeach--}}

{{--                    </tbody>--}}
{{--                </table>--}}
{{--                <div class="card-footer small text-muted">Last Updated--}}
{{--                    at {{$riderlists->updated_at->format('d M, Y h:i A')}}</div>--}}
{{--            </div>--}}
{{--             </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}
