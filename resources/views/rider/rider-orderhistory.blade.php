@extends('layouts.riderdashboardlayout')

@section('title','title')

@section('body')

    <div id="content-wrapper">
        <div class="container-fluid">

            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
        @endif

            <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        <b>My Order History</b>
                    </div>
                    <div class="card-body">
                        <div id="no-more-tables">
                            <table class="table table-responsive table-bordered text-center" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead class="bg-grey">
                                <tr class="">
                                    <th>Customer Name</th>
                                    <th>Address</th>
                                    <th>User Contact</th>
                                    <th>Order Date</th>
                                    <th>Total Amount</th>
                                    <th>Status</th>
                                    <th>Order Details</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assignOrder as $data)
                                    <tr>
                                        <td>{{\App\Models\Order::find($data->order_id)->User['fullname']}}</td>
                                        <td>{{\App\Models\Order::find($data->order_id)->User['address']}}</td>
                                        <td>{{\App\Models\Order::find($data->order_id)->User['user_contact']}}</td>
                                        <td>{{\App\Models\Order::find($data->order_id)->created_at->format('d M, Y')}}</td>
                                        <td>{{\App\Models\Order::find($data->order_id)->total_ammount}}</td>
                                        <td>{{$data->order_status}}</td>
                                        <td>
                                            <i onclick="orderDetail({{\App\Models\Order::find($data->order_id)->id}})"  class="fa fa-eye detail bd-example-modal-lg"  data-toggle="modal" data-target="#exampleModal"></i>
{{--                                            <button type="button" id="{{\App\Models\Order::find($data->order_id)->id}}" class="btn btn-warning my-2" data-toggle="modal" data-target="#exampleModal">--}}
{{--                                                Order Details--}}
{{--                                            </button>--}}

                                        </td>
{{--                                        <td><a href="" class="btn btn-success">Delivered</a></td>--}}
                                        <td><a href="{{route('delivered',[$data->order_id])}}" class="btn btn-success my-2">Delivered</a></td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>


                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Order Details</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Shop Name</th>
                                                <th>Quantity</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>

                                            <tbody id="order-detail">


                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
{{--                    <div class="card-footer small text-muted">Last Updated--}}
{{--                        at {{$shop->updated_at->format('d M, Y h:i A')}}</div>--}}
                </div>
            </div>
        </div>
    </div>


    <script>
        function orderDetail (id) {

            $('#order-detail').empty();
            $.ajax({
                type : 'get',
                data : {'id':id},
                url  : '{{route('getOrderDetailsRider')}}',
                success : function (data) {
                    // $('#order-detail').empty();

                    $.each(data,function (i,value) {
                        var tr=$("<tr/>");

                        tr.append($("<td/>",{
                            text:value.product_id[0]
                        }))

                            .append($("<td/>",{
                                text:value.order
                            }))
                            .append($("<td/>",{
                                text:value.quantity
                            }))
                            .append($("<td/>",{
                                text:value.amount
                            }))




                        $('#order-detail').append(tr);
                        // $('#order-detail').empty();

                    })
                    console.log(data);
                }
            });
        }
    </script>
@endsection
