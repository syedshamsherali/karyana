@extends('layouts.dashboard')
@section('title','Add Rider')
@section('body')
    <div class="col-md-12">
    <h1 class="h3 mb-4">Update Rider</h1>
    </div>
    <div class="col-md-12">
    <form class="needs-validation" method="post" novalidate action="{{route('update_rider')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <input type="hidden" name="id" value="{{$riders->id}}">
        </div>
        <div class="form-row col-md-12">
            <div class="col-md-4 mb-3">
                <label for="validationCustom01" class="">Rider name</label>
                <input type="text" name="fullname" class="form-control  @error('fullname') is-invalid @enderror" id="validationCustom01" placeholder="Rider name" value="{{$riders->fullname}}" required>
                <div class="invalid-feedback">
                    name is Required
                </div>
                @error('fullname')
                <span class="invalid-feedback" role="alert" class="input-group-addon">
                 <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom03" class="">E-mail</label>
                <input type="email" name="email" class="form-control  @error('email') is-invalid @enderror" id="validationCustom02" value="{{$riders->email}}" placeholder="E-mail" required>
                <div class="invalid-feedback">
                    Please provide a valid E-mail.
                </div>
                @error('user_email')
                <span class="invalid-feedback" role="alert" class="input-group-addon">
                 <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom03" class="">Password</label>
                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="validationCustom02" value="{{$riders->password}}" placeholder="password" required>
                <div class="invalid-feedback">
                    Please provide a valid Password.
                </div>
                @error('password')
                <span class="invalid-feedback" role="alert" class="input-group-addon">
                 <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-row col-md-12">
            <div class="col-md-4 mb-3">
                <label for="validationCustom03" class="">Contact</label>
                <input type="text" name="user_contact" class="form-control  @error('user_contact') is-invalid @enderror" id="validationCustom03" value="{{$riders->user_contact}}" placeholder="+92" required>
                <div class="invalid-feedback">
                    Valid Contact is required.
                </div>
                @error('user_contact')
                <span class="invalid-feedback" role="alert" class="input-group-addon">
                 <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom03" class="">City</label>
                <input type="text" name="city" class="form-control  @error('city') is-invalid @enderror" id="validationCustom04" value="{{$riders->city}}" placeholder="City" required>
                <div class="invalid-feedback">
                    Please provide a valid city.
                </div>
                @error('city')
                <span class="invalid-feedback" role="alert" class="input-group-addon">
                 <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom03" class="">Licence.No</label>
                <input type="text" name="licence_number" class="form-control  @error('licence_number') is-invalid @enderror" id="validationCustom05" value="{{$riders->Rider['licence_number']}}" placeholder="Licence" required>
                <div class="invalid-feedback">
                    Please provide your licence NO..
                </div>
                @error('licence_number')
                <span class="invalid-feedback" role="alert" class="input-group-addon">
                 <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-row col-md-12">
            <div class="col-md-4 mb-3">
                <label for="validationCustom04" class="">Vehicle No</label>
                <input type="text" name="vehicle_number" class="form-control  @error('vehicle_number') is-invalid @enderror" id="validationCustom06" value="{{$riders->Rider['vehicle_number']}}" placeholder="Vehicle" required>
                <div class="invalid-feedback">
                    Please provide Your Vehicle No
                </div>
                @error('vehicle_number')
                <span class="invalid-feedback" role="alert" class="input-group-addon">
                 <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom04" class="">Date Of joining</label>
                <input type="text" name="date_of_joining" class="form-control  @error('date_of_joining') is-invalid @enderror" id="datepicker" value="{{$riders->Rider['date_of_joining']}}" placeholder="Vehicle" required>
                <div class="invalid-feedback">
                    Please provide Your Vehicle No
                </div>
                @error('date_of_joining')
                <span class="invalid-feedback" role="alert" class="input-group-addon">
                 <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom02" class="">Rider's Address</label>
                <textarea name="address"  class="form-control  @error('address') is-invalid @enderror" id="validationCustom08" placeholder="Enter Address of shop..." required>{{$riders->address}}</textarea>
                <div class="invalid-feedback">
                    Rider's address Required
                </div>
                @error('address')
                <span class="invalid-feedback" role="alert" class="input-group-addon">
                 <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-row col-md-12">
            <div class="col-md-5 mb-3">
                <label for="validationCustom05" class="">Photo</label>
                <input type="file" accept="image/*" onchange="loadFile(event)" name="thumbnail" class="form-control @error('thumbnail') is-invalid @enderror" value="{{old('thumbnail')}}" id="validationCustom05" required>
                <div class="invalid-feedback">
                    Photo
                </div>
                @error('thumbnail')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="col-md-5">
                <img id="output" width="200px" height="100px;">
            </div>
        </div>
        <div class="form-row col-md-12">
            <div class="col-md-4">
                <input type="submit" class="btn text-light" style="background: #74ab26" value="Update">
            </div>
        </div>
    </form>
        @include('dashboards.projectjs')
    <script>
        $( function() {
            $( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
            // $( "#calender" ).datepicker({dateFormat: 'yy-mm-dd'});

        } );
    </script>
        <script>
            var loadFile = function(event) {
                var output = document.getElementById('output');
                output.src = URL.createObjectURL(event.target.files[0]);
            };
        </script>
    </div>
@endsection
