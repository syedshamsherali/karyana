@extends('layouts.riderdashboardlayout')

@section('title','title')

<meta name="csrf-token" content="{{ csrf_token() }}">
@section('body')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <div class="containner">
        <div class="row col-md-12">
            <div class="row col-md-5 justify-content-between">
                <div class="profile-img col-md-12">
                    <img style="width: 70%; height: 70%" src="{{url('/images/'.$userlist->Rider['thumbnail'])}}" alt=""/>
                </div>
            </div>
            <div class="row col-md-7">
                <div class="container mt-3">
                    <h2>{{$userlist->fullname}}</h2>
                    <br>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active text-success" data-toggle="tab" href="#home">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-success" data-toggle="tab" href="#menu1">Detail</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="home" class="container tab-pane active"><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label >Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p >{{$userlist->fullname}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$userlist->email}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Phone</label>
                                </div>
                                <div class="col-md-6 @error('user_contact') is-invalid @enderror " name="user_contact">
                                    <p class="edit">{{$userlist->user_contact}}</p>
                                </div>
                                @error('user_contact')
                                    <span class="invalid-feedback" role="alert" class="input-group-addon">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>City</label>
                                </div>
                                <div class="col-md-6">
                                    <p  class="edit">{{$userlist->city}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Address</label>
                                </div>
                                <div class="col-md-6">
                                    <p  class="edit">{{$userlist->address}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <a onclick="editdata()" class="btn btn-outline-warning" id="editbutton">Edit Profile</a>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="container tab-pane fade"><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Vehicle Number</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$userlist->Rider['vehicle_number']}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Licence</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$userlist->Rider['licence_number']}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Joining date</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$userlist->Rider['date_of_joining']}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Profession</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$userlist->type}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboards.projectjs')
    <script>
        function editdata() {
            var edit = document.getElementsByClassName('edit');
            var button = document.getElementById('editbutton');
            button.innerHTML = "Save";
            button.setAttribute('onclick','savedata()');
            for (var i = 0 ; i< edit.length ; i++)
            {
                var value = edit[i].innerHTML;
                var input = "<input type='text' id='validationCustom05' value='"+value+"' class='getvalue'>";
                edit[i].innerHTML = input;
            }
        }
        function savedata() {
            var editvalues = document.getElementsByClassName('getvalue');
            var edit = document.getElementsByClassName('edit');
            var button = document.getElementById('editbutton');
            button.innerHTML = "Edit Profile";
            button.setAttribute('onclick','editdata()');
            var values;
            for (var i = 0 ; i < edit.length ; i++)
            {
                edit[i].innerHTML = editvalues[0].value;
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type : 'post',
                url : "{{route('editprofile')}}",
                data : { 'user_contact': edit[0].innerHTML , 'city':edit[1].innerHTML , 'address': edit[2].innerHTML},
                success : function (response) {
                },
            });
        }
    </script>
@endsection
