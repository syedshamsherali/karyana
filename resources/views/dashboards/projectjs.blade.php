{{--Compare Products--}}
<script>
    // var token=document.getElementById('token').value;
    var increment = 0;
    $(document).ready(function(){
        $(".compare").click(function(){
            increment++;
            document.getElementById('compare').innerHTML = "";
            document.getElementById('compare').innerHTML = "Compare (" +increment+")";
                    if(increment == 4)
                    {
                        var array =  document.getElementsByClassName('compare');
                        for (var i = 0 ; i < array.length ; i++)
                        {
                            array[i].setAttribute('disabled','');
                        }
                    }
            var id=$(this).val();
            $.ajax({
                url: '/addToCompare/'+id,
                type : 'get',
                // dataType:'json',
                data:{'id':id},
                success: function(data){
                    // if(data){
                    // $('#data').html(data);
                    // console.log('hello');
                    // }
                    $.notify({
                        message: "Successful Added "+data+" Into Product Compare",
                        target: '_blank'
                    },{
                        // settings
                        element: 'body',
                        position: null,
                        type: "info",
                        allow_dismiss: true,
                        newest_on_top: false,
                        placement: {
                            from: "top",
                            align: "center"
                        },
                        offset: 0,
                        spacing: 10,
                        z_index: 2031,
                        delay: 5000,
                        timer: 1000,
                        url_target: '_blank',
                        mouse_over: null,
                        animate: {
                            enter: 'animated fadeInDown'
                            //exit: 'animated fadeOutUp'
                        },
                        onShow: null,
                        onShown: null,
                        onClose: null,
                        onClosed: null,
                        icon_type: 'class',
                        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +
                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">&nbsp;&times;</button>' +
                            '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
                            '<div class="progress" data-notify="progressbar">' +
                            '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                            '</div>' +
                            '<a href="{3}" target="{4}" data-notify="url"></a>' +
                            '</div>'
                    });
                    $('#compare-total').html(data['total']);
                    // alert(total);
                }
            });
        });
    });
</script>
{{--Compare Products End--}}

{{--Wish-List Products--}}
<script>
    // var token=document.getElementById('token').value;
    $(document).ready(function() {
        $(".wishlist").click(function () {
            var id = $(this).val();
            $.ajax({
                url: '/Wish-list/'+ id,
                type: 'get',
                // dataType: 'json',
                data: {'id': id},
                success: function (data) {
                    $.notify({
                        message: "Successful Added Into Wish List",
                        target: '_blank'
                    }, {
                        // settings
                        element: 'body',
                        position: null,
                        type: "info",
                        allow_dismiss: true,
                        newest_on_top: false,
                        placement: {
                            from: "top",
                            align: "center"
                        },
                        offset: 0,
                        spacing: 10,
                        z_index: 2031,
                        delay: 5000,
                        timer: 1000,
                        url_target: '_blank',
                        mouse_over: null,
                        animate: {
                            enter: 'animated fadeInDown'
                            //exit: 'animated fadeOutUp'
                        },
                        onShow: null,
                        onShown: null,
                        onClose: null,
                        onClosed: null,
                        icon_type: 'class',
                        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +
                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">&nbsp;&times;</button>' +
                            '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
                            '<div class="progress" data-notify="progressbar">' +
                            '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                            '</div>' +
                            '<a href="{3}" target="{4}" data-notify="url"></a>' +
                            '</div>'
                    });
                }
            });
        })
    });


</script>
{{--Wish-List Products End--}}
{{--start  Remove item from comparison--}}
<script>

function removeitem(id) {
    // alert(id);
    // console.log(id);
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },

        type: "post",
        url: '{{route("remove_Item")}}',
        data: {'id': id},
        success : function (data) {
            console.log(data);
            // location.reload();
        }
    });
}
</script>

{{--cart script start--}}
<script>
    function addToCart(id)

    {
        // alert(id);
        $.ajax({
            type : 'get',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url : "{{route('addToCart')}}",
            data : {'id':id},
            success : function (response) {
                if(response){
                    var a = document.getElementById('cart-total').innerHTML;
                    var b = 1;
                    var value = +a + +b;
                    document.getElementById('cart-total').innerHTML = value;

                }
                $.notify({
                    message: "Successful Added "+response+" Into Shopping Cart",
                    target: '_blank'
                },{
                    // settings
                    element: 'body',
                    position: null,
                    type: "info",
                    allow_dismiss: true,
                    newest_on_top: false,
                    placement: {
                        from: "top",
                        align: "center"
                    },
                    offset: 0,
                    spacing: 10,
                    z_index: 2031,
                    delay: 5000,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: null,
                    animate: {
                        enter: 'animated fadeInDown'
                        //exit: 'animated fadeOutUp'
                    },
                    onShow: null,
                    onShown: null,
                    onClose: null,
                    onClosed: null,
                    icon_type: 'class',
                    template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +
                        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">&nbsp;&times;</button>' +
                        '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
                        '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                        '</div>' +
                        '<a href="{3}" target="{4}" data-notify="url"></a>' +
                        '</div>'
                });

                $('#cart > ul').load('details?route=common/cart/info ul li');

            }
        });
    }

// update quantity()
    function quantity(quantity,product_id) {
        var totalPrice = document.getElementById(product_id+'b');
        var price = document.getElementById(product_id+'a');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type : "post",
            url :  '{{"remove-cart"}}',
            data : {'id': product_id , 'qty': quantity ,'price':price.innerHTML , 'totalPrice':totalPrice.innerHTML,'addQuantity': 'true'},
            success : function (response) {
                location.reload();
            }
        });
    }
    function removeRow(id) {
        var qty =  document.getElementById(id);
        var price =  document.getElementById(id+'a');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type : "post",
            url :  '{{"remove-cart"}}',
            data : {'id': id , 'qty': qty.value ,'price':price.innerHTML},
            success : function (response) {
                location.reload();
            }
        });
    }
</script>
{{--cart script end--}}

{{--category script start--}}
<script>
    function addCategory(id) {
        var addCategory = document.getElementById(id+'id');
        // var xyz = ;
        addCategory.innerHTML = "<input placeholder=' Add Child Category' type='text' id='"+id+"b'>&emsp;<input type='button' value='Create' class='btn btn-success' onclick=saveCat('"+id+"b','"+id+"',false)>&emsp;<input type='button' value='Cancel' class='btn btn-success' onclick=saveCat('"+id+"',0,true)>";
    }


    function saveCat(input,id,bool) {
        if (bool){
            var data =  document.getElementById(input);
            location.reload()
        }
        else {
            var data =  document.getElementById(input);
            $.ajax({
                type : 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url : "{{route('createchild')}}",
                data : {'parent_id':id , 'name':data.value},
                success : function (response) {
                    alert(response);
                    if(response){
                        location.reload()
                    }
                }
            });
        }
    }


    function mainCategory() {
        var name =  document.getElementById('main-category');
        $.ajax({
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url : "{{route('createchild')}}",
            data : {'name':name.value},
            success : function (response) {
                alert(response);
                if(response){
                    location.reload()
                }
            }
        });
    }
    function ConfirmDelete(id)
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            deleteCategory(id);
                {{--        @dd($categories)--}}
        else
            return false;
    }



    function deleteCategory(id)
    {
        $.ajax({
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url : "{{route('delete_category')}}",
            data : {'id':id},
            success : function (response) {
                if(response){
                    location.reload()
                }
            }
        });
    }
    function editRow(id) {
        $('#'+id).find('span').remove();
        var rowid = document.getElementById(id).innerHTML
        // document.getElementById(id).innerHTML = ;
        document.getElementById(id).innerHTML = "<input type='text' value='"+ rowid+"' id='edited' autocomplete='off'>&emsp;<input type='button' class='btn btn-success' value='Save' onclick='updateCategory("+id+")'>&emsp;<input type='button' class='btn btn-success' value='Cancel' onclick='updateCategory("+id+",true)'>";
    }
    function updateCategory(id,cancel)
    {
        if (cancel) {
            var edited = document.getElementById('edited').value;
            var rowid = document.getElementById(id).innerHTML = edited;
            location.reload();
        }
        else {
            var edited = document.getElementById('edited').value;
            var rowid = document.getElementById(id).innerHTML = edited;
            alert(rowid);
            $.ajax({
                type : 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url : "{{route('updatecategory')}}",
                data : {'id':id , 'name':edited},
                success : function (response) {
                    if(response){
                        location.reload();
                    }
                }
            });
        }

    }
</script>
{{--cagegory script end--}}


{{-- End Remove item from comparison--}}
{{--client side validation start--}}
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
{{--client side validation End--}}
