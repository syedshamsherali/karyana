<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.bootstrapious.com/directory/1-3/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 06:32:03 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Karyana.pk</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Price Slider Stylesheets -->
    <link rel="stylesheet" href="{{asset('welcom/Style/nouislider/nouislider.css')}}">
    <!-- Google fonts - Playfair Display-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700">
    <!-- Google fonts - Poppins-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,700">
    <!-- swiper-->
    <link rel="stylesheet" href="{{asset('welcom/Style/Swiper/4.4.1/css/swiper.min.css')}}">
    <!-- Magnigic Popup-->
    <link rel="stylesheet" href="{{asset('welcom/Style/magnific-popup/magnific-popup.css')}}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{asset('welcom/Style/css/style.default.616eba24.css')}}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{asset('welcom/Style/css/custom.0a822280.css')}}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('welcom/Style/img/cart.png')}}">
    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{asset('welcom/Style/use.fontawesome.com/releases/v5.8.1/css/all.css')}}" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>

<body style="padding-top: 72px;">
<header class="hero-home">

    <nav class="navbar navbar-expand-lg fixed-top shadow navbar-light" style="background-color: #6b9e23">

        <div class="container-fluid">
            <div class="d-flex align-items-center"><a href="" class="navbar-brand"><img src="{{asset('welcom/Style/img/photo/karyana-logo.png')}}" alt="Karyana logo" height="87px">

                </a>
            </div>

            <div class="input-label-absolute input-label-absolute-left input-reset input-expand ml-lg-2 ml-xl-3">
                <i class="fa fa-motorcycle" aria-hidden="true"></i>
            </div>

            <div id="navbarCollapse" class="collapse navbar-collapse">

                <ul class="navbar-nav ml-auto">


                    <li class="nav-item mt-3 mt-lg-0 ml-lg-3 d-lg-none d-xl-inline-block"><a href="{{ route('login') }}" class="btn btn-primary">Sign In</a></li>

                    <li class="nav-item mt-3 mt-lg-0 ml-lg-3 d-lg-none d-xl-inline-block"><a href="{{ route('register') }}" class="btn btn-primary">Register</a></li>

                </ul>

            </div>


        </div>


    </nav>


    <div class="swiper-container hero-slider">
        <div class="swiper-wrapper dark-overlay">
            <div style="background-image:url({{asset('welcom/Style/img/photo/shopSlide1.jpg')}})" class="swiper-slide"></div>
            <div style="background-image:url({{asset('welcom/Style/img/photo/shopslide2.jpg')}})" class="swiper-slide"></div>
            <div style="background-image:url({{asset('welcom/Style/img/photo/shopslide3.jpg')}})" class="swiper-slide"></div>
            <div style="background-image:url({{asset('welcom/Style/img/photo/shopslide4.jpg')}})" class="swiper-slide"></div>
        </div>
    </div>
    <div class="container py-6 py-md-7 text-white z-index-20">
        <div class="row">
            <div class="col-xl-10">
                <div class="text-center text-lg-left">
                    <p class="subtitle letter-spacing-4 mb-2 text-secondary text-shadow">The best open market experince</p>
                    <h1 class="display-3 font-weight-bold text-shadow">Shop like you mean it</h1>
                </div>
                <div class="search-bar mt-5 p-3 p-lg-1 pl-lg-4">
                    <form action="#">
                        <div class="row">

                            <div class="col-lg-10 d-flex align-items-center form-group">
                                <div class="input-label-absolute input-label-absolute-right w-100">
                                    <input type="text" name="location" placeholder="Location" id="location" class="form-control border-0 shadow-0">
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <a href="{{route('home')}}" class="btn btn-primary btn-block rounded-xl h-100" >Search </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>

<section class="py-6 bg-gray-100">
    <div class="container">
        <div class="text-center pb-lg-4">
            <p class="subtitle text-secondary">One-of-a-kind Karyana Store </p>
            <h2 class="mb-5">Shopping with us is easy</h2>
        </div>
        <div class="row">
            <div class="col-lg-4 mb-3 mb-lg-0 text-center">
                <div class="px-0 px-lg-3">
                    <div class="icon-rounded bg-primary-light mb-3">
                        <img src="{{asset('welcom/Style/img/photo/store.png')}}" height="45px" alt="">
                    </div>
                    <h3 class="h5">Find best stores around you</h3>
                    <p class="text-muted">One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed in</p>
                </div>
            </div>
            <div class="col-lg-4 mb-3 mb-lg-0 text-center">
                <div class="px-0 px-lg-3">
                    <div class="icon-rounded bg-primary-light mb-3">
                        <img src="{{asset('welcom/Style/img/photo/comparison.png')}}" height="45px" alt="">
                    </div>
                    <h3 class="h5">Price Comparison</h3>
                    <p class="text-muted">The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pit</p>
                </div>
            </div>
            <div class="col-lg-4 mb-3 mb-lg-0 text-center">
                <div class="px-0 px-lg-3">
                    <div class="icon-rounded bg-primary-light mb-3">
                        <img src="{{asset('welcom/Style/img/photo/shopping-basket.png')}}" height="45px" alt="">
                    </div>
                    <h3 class="h5">Only one cart</h3>
                    <p class="text-muted">His room, a proper human room although a little too small, lay peacefully between its four familiar </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Divider Section-->
<section class="py-7 position-relative dark-overlay"><img src="{{asset('welcom/Style/img/photo/shoplap.jpg')}}" alt="" class="bg-image">
    <div class="container">
        <div class="overlay-content text-white py-lg-5">
            <h3 class="display-3 font-weight-bold text-serif text-shadow mb-5">Ready for your next Shop?</h3><a href="category-rooms.html" class="btn btn-light">Get started</a>
        </div>
    </div>
</section>
<section class="py-7">
    <div class="container">
        <div class="text-center">
            <p class="subtitle text-primary">Testimonials</p>
            <h2 class="mb-5">Our dear customers said about us</h2>
        </div>
        <!-- Slider main container-->
        <div class="swiper-container testimonials-slider testimonials">
            <!-- Additional required wrapper-->
            <div class="swiper-wrapper pt-2 pb-5">
                <!-- Slides-->
                <div class="swiper-slide px-3">
                    <div class="testimonial card rounded-lg shadow border-0">
                        <div class="testimonial-avatar"><img src="{{asset('welcom/Style/img/avatar/avatar-3.jpg')}}" alt="..." class="img-fluid"></div>
                        <div class="text">
                            <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
                            <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide px-3">
                    <div class="testimonial card rounded-lg shadow border-0">
                        <div class="testimonial-avatar"><img src="{{asset('welcom/Style/img/avatar/avatar-3.jpg')}}" alt="..." class="img-fluid"></div>
                        <div class="text">
                            <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
                            <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide px-3">
                    <div class="testimonial card rounded-lg shadow border-0">
                        <div class="testimonial-avatar"><img src="{{asset('welcom/Style/img/avatar/avatar-3.jpg')}}" alt="..." class="img-fluid"></div>
                        <div class="text">
                            <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
                            <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide px-3">
                    <div class="testimonial card rounded-lg shadow border-0">
                        <div class="testimonial-avatar"><img src="{{asset('welcom/Style/img/avatar/avatar-3.jpg')}}" alt="..." class="img-fluid"></div>
                        <div class="text">
                            <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
                            <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide px-3">
                    <div class="testimonial card rounded-lg shadow border-0">
                        <div class="testimonial-avatar"><img src="{{asset('welcom/Style/img/avatar/avatar-3.jpg')}}" alt="..." class="img-fluid"></div>
                        <div class="text">
                            <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
                            <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide px-3">
                    <div class="testimonial card rounded-lg shadow border-0">
                        <div class="testimonial-avatar"><img src="{{asset('welcom/Style/img/avatar/avatar-3.jpg')}}" alt="..." class="img-fluid"></div>
                        <div class="text">
                            <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
                            <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination"> </div>
        </div>
    </div>
</section>

<footer class="position-relative z-index-10 d-print-none">
    <!-- Main block - menus, subscribe form-->

    <!-- Copyright section of the footer-->
    <div class="py-4 font-weight-light text-gray-300" style="background-color: #6b9e23">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 text-center text-md-left">
                    <p class="text-sm mb-md-0">&copy; 2019 Karyana.pk . All rights reserved.</p>
                </div>
                <div class="col-md-6">
                    <ul class="list-inline mb-0 mt-2 mt-md-0 text-center text-md-right">
                        <li class="list-inline-item"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-3/img/visa.svg" alt="..." class="w-2rem"></li>
                        <li class="list-inline-item"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-3/img/mastercard.svg" alt="..." class="w-2rem"></li>
                        <li class="list-inline-item"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-3/img/paypal.svg" alt="..." class="w-2rem"></li>
                        <li class="list-inline-item"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-3/img/western-union.svg" alt="..." class="w-2rem"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /Footer end-->

<script src="{{asset('welcom/Script/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap JS bundle - Bootstrap + PopperJS-->
<script src="{{asset('welcom/Script/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Magnific Popup - Lightbox for the gallery-->
<script src="{{asset('welcom/Script/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<!-- Smooth scroll-->
<script src="{{asset('welcom/Script/smooth-scroll/smooth-scroll.polyfills.min.js')}}"></script>
<!-- Bootstrap Select-->
<script src="{{asset('welcom/Script/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<!-- Object Fit Images - Fallback for browsers that don't support object-fit-->
<script src="{{asset('welcom/Script/object-fit-images/ofi.min.js')}}"></script>
<!-- Swiper Carousel                       -->
<script src="{{asset('welcom/Script/Swiper/4.4.1/js/swiper.min.js')}}"></script>
<script>
    var basePath = ''

</script>
<!--    Google Map Api-->
<script>

    var options = {
        types: ['(cities)'],
        componentRestrictions: {
            country: "pak"
        }
    };


    function activatePlacesSearch() {

        var intput = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(intput);
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgV-mkhz5pqHJrtexHQXJdV12D8nGefoI&libraries=places&callback=activatePlacesSearch"></script>
<!-- Main Theme JS file    -->
<script src="{{asset('welcom/Script/theme/theme.55f8348b.js')}}"></script>

</body>

<!-- Mirrored from demo.bootstrapious.com/directory/1-3/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 06:32:28 GMT -->

</html>
