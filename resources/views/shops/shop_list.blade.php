@extends('layouts.dashboard')
@section('title','Shop List')
@section('body')
    <div id="content-wrapper">
        <div class="container-fluid">
            @if(session('message'))
                <p class="alert alert-success">{{session('message')}}</p>
        @endif
            <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        <b>Karyana Shops</b>
                    </div>
                    <div class="card-body">
                        <div id="no-more-tables">
                            <table class="table table-responsive table-bordered text-center" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead class="bg-grey">
                                <tr class="">
                                    <th>Id</th>
                                    <th>Shop Name</th>
                                    <th>Address</th>
                                    <th>State</th>
                                    <th>Opening Time</th>
                                    <th>Closing Time</th>
                                    <th>City</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($shops as $shop)
                                    <tr>
                                        <td >{{$shop->id}}</td>
                                        <td >{{$shop->name}}</td>
                                        <td >{{$shop->address}}</td>
                                        <td >{{$shop->state}}</td>
                                        <td >{{$shop->opening_time}}</td>
                                        <td >{{$shop->closing_time}}</td>
                                        <td >{{$shop->city}}</td>
                                        <td>
                                            <div class="dropdown">
                                                <span  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-h"></i>
                                                </span>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a href="{{route('shop_edit',[$shop->id])}}" class="btn btn-primary">Edit</a>
                                                    <a href="{{route('shop_delete',[$shop->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer small text-muted">Last Updated
                        at {{$shop->updated_at->format('d M, Y h:i A')}}</div>
                </div>
            </div>
        </div>
    </div>
@endsection


















{{--@extends('layouts.dashboard')--}}

{{--@section('title','Shop List')--}}

{{--@section('body')--}}
{{--    <h1 class="h3 mb-2 text-light">Shop List</h1>--}}

{{--    @if(session('message'))--}}
{{--        <p class="alert alert-success">{{session('message')}}</p>--}}
{{--    @endif--}}
{{--    <!-- DataTales Example -->--}}
{{--    <div class="card shadow mb-4">--}}

{{--        <div class="card-body">--}}
{{--            <div class="table-responsive">--}}
{{--                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                        <th>Shop Name</th>--}}
{{--                        <th>Address</th>--}}
{{--                        <th>Opening Time </th>--}}
{{--                        <th>Closing Time</th>--}}
{{--                        <th>City</th>--}}
{{--                        <th>Action</th>--}}
{{--                    </tr>--}}
{{--                    </thead>--}}

{{--                    <tbody>--}}
{{--                    @foreach($shops as $shop)--}}
{{--                    <tr>--}}
{{--                        <td>{{$shop->name}}</td>--}}
{{--                        <td>{{$shop->address}}</td>--}}
{{--                        <td>{{$shop->opening_time}}</td>--}}
{{--                        <td>{{$shop->closing_time}}</td>--}}
{{--                        <td>{{$shop->city}}</td>--}}
{{--                        <td>--}}
{{--                            <div class="dropdown">--}}
{{--                                <span  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                    <i class="fas fa-ellipsis-h"></i>--}}
{{--                                </span>--}}
{{--                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">--}}
{{--                                    <a href="{{route('shop_edit',[$shop->id])}}" class="btn btn-primary">Edit</a>--}}
{{--                                    <a href="{{route('shop_delete',[$shop->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    </tbody>--}}
{{--                    @endforeach--}}
{{--                </table>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-2 text-center">--}}
{{--                        {{ $shops->links() }}--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}
