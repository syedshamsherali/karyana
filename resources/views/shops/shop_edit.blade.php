@extends('layouts.dashboard')

@section('title','Add Shop')

@section('body')
    <div class="col-md-12">
    <h1 class="h3 mb-4 offset-1">Add Shop</h1>
    </div>
    <!-- Page Heading -->
    <form action="{{route('shop_update')}}" method="post" class="needs-validation offset-1" enctype="multipart/form-data" novalidate>
        {{csrf_field()}}

        @if(session('message'))
            <p class="alert alert-success">{{session('message')}}</p>
        @endif


        <div class="form-group ">
            <input type="hidden" name="id" value="{{$shops->id}}">
        </div>

        <div class="form-row col-md-12 ">
            <div class="col-md-5 mb-3">
                <label for="validationCustom01" class="">Shop name</label>
                <input type="text" name="shop_name" class="form-control" id="validationCustom01" placeholder="Shop name" value="{{$shops->name}}" required>
                <div class="invalid-feedback">
                    Shop name Required
                </div>
            </div>

            <div class="col-md-5 mb-3">
                <label for="validationCustom02" class="">Shop Address</label>
                <textarea name="address"  class="form-control" id="validationCustom02" placeholder="Enter Address of shop..." required>{{$shops->address}}</textarea>
                <div class="invalid-feedback">
                    Shop address Required
                </div>
            </div>
        </div>
        <div class="form-row col-md-12">

            <div class="col-md-5 mb-3">
                <label for="validationCustom03" class="">City</label>
                <input type="text" name="city" class="form-control" id="validationCustom03" placeholder="City" value="{{$shops->city}}" required>
                <div class="invalid-feedback">
                    Please provide a valid city.
                </div>
            </div>
            <div class="col-md-5 mb-3">
                <label for="validationCustom04" class="">State</label>
                <input type="text" name="state" class="form-control" id="validationCustom04" placeholder="State" value="{{$shops->state}}" required>
                <div class="invalid-feedback">
                    Please provide a valid state.
                </div>
            </div>

        </div>

        <div class="form-row col-md-12">

            <div class="col-md-5 mb-3">
                <label class="">Opening Time</label>
                <input id="timepicker" name="opening_time" value="{{$shops->opening_time}}" placeholder="Opening time"  />

            </div>

            <div class="col-md-5 mb-3">
                <label class="">Closing Time</label>
                <input id="timepicker1" name="closing_time" value="{{$shops->opening_time}}" placeholder="Closing time"  />
            </div>


        </div>

        <div class="form-row col-md-12">
            <div class="col-md-5 mb-3">
                <label for="validationCustom05" class="">logo</label>
                <input type="file" accept="image/*" onchange="loadFile(event)" name="thumbnail[]" multiple class="form-control" id="validationCustom05" required>
                <div class="invalid-feedback">
                    Shop logo required
                </div>
            </div>
                <div class="col-md-5">
                  <img id="output" width="200px" height="100px;">
                </div>
        </div>
        <div class="col-md-12">
        <button class="btn mt-2 text-light" style="background: #74ab26"type="submit">Update Shop</button>
        </div>
    </form>

    <script>
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();

    </script>

    <script>
        $('#timepicker').timepicker({
            uiLibrary: 'bootstrap4'
        });

        $('#timepicker1').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection
