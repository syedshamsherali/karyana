@extends('layouts.master')
@section('title', 'Shop Detail')
{{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}

@section('body')
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Shop Detail </a></li>
    </ul>
    <div class="">
        <div id="tabs" class="htabs">
            @if(session()->has('message'))
                <p class="alert alert-success">
                    {{session()->get('message')}}
                </p>
            @endif
            <div class="tab-title-main">
                <ul class='etabs nav nav-tabs'>
                    <li class='nav-item active'>
                        <a href="#" data-toggle="tab" class="nav-link">{{$shop->name}} Prodcuts</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <h1></h1>
    <hr>
    <div class="container">
        <div class="row">
            <aside id="column-left" class="col-sm-3 hidden-xs">
                <div class="box">
                    <div class="box-heading">Categories</div>
                    <div class="box-content">
                        <ul class="box-category treeview-list treeview">
                            @foreach($categories->where('parent_id',0) as $cat)
                                <li>
                                    <a href="#" >{{$cat->name}}</a>
                                    @if($cat->childs->count()>0)
                                        <ul>
                                            @foreach($cat->childs as $submenu)
                                                <li>
                                                    <a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="box">
                    <div class="box-heading">Bestsellers</div>
                    <div class="box-content">
                        <div class="box-product  productbox-grid" id=" bestseller-grid">
                            <div class="product-items col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="product-block product-thumb transition">
                                    <div class="product-block-inner">
                                        <div class="image">
                                            <a href="{{route('productDetail',[$products[0]->id])}}"><img src="{{url('/images/'.$products[0]->image)}}" alt="" title="{{$products[0]->name}}" class="img-responsive" /></a>
                                        </div>
                                        <div class="product-details">
                                            <div class="caption">
                                                <h4><a href="{{route('productDetail',[$products[0]->id])}}">{{$products[0]->name}}</a></h4>
                                                <p class="price">
                                                    Rs.{{$products[0]->price}}
                                                    <span class="price-tax">{{$products[0]->price}}</span>
                                                    <span class="rating">
                                                 </span>
                                                </p>
                                                <div class="button-group">
                                                    <button type="button" class="addtocart" onclick="cart.add('45 ');">Add to Cart </button>
                                                    <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('45 ');"><i class="fa fa-heart"></i></button>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product " onclick="compare.add('45 ');"><i class="fa fa-exchange"></i></button>
                                                    <div class="quickview" data-toggle="tooltip" data-placement="top" title="Quickview"><a href="#">Quick View<i class="fa fa-eye" aria-hidden="true"></i></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box-heading">Latest</div>
                    <div class="box-content">
                        <div class="box-product  productbox-grid" id=" latest-grid">
                            <div class="product-items col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="product-block product-thumb transition">
                                    <div class="product-block-inner">
                                        <div class="image">
                                            @php
                                            $images=$latestProduct->image;
                                            $img=explode(",",$images);
                                            @endphp
{{--                                            @dd($img)--}}
                                            <a href="{{route('productDetail',[$latestProduct->id])}}"><img src="{{url('/images/'.$img[0])}}" alt="" title="" class="img-responsive" /></a>
                                        </div>
                                        <div class="product-details">
                                            <div class="caption">
                                                <h4><a href="{{route('productDetail',[$latestProduct->id])}}">{{$latestProduct->name}} </a></h4>
                                                <div class="rating">
                                                </div>
                                                <p class="price">
                                                    Rs.{{$latestProduct->price}}
                                                    <span class="price-tax">{{$latestProduct->price}}</span>
                                                </p>
                                                <div class="button-group">
                                                    <button type="button" class="addtocart" onclick="cart.add('49 ');"><i class="fa fa-heart"></i></button>
                                                    <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product " onclick="compare.add('49 ');"><i class="fa fa-exchange"></i></button>
                                                    <div class="quickview" data-toggle="tooltip" data-placement="top" title="Quickview"><a href="{{url('/images/'.$latestProduct->image)}}">Quick View<i class="fa fa-eye" aria-hidden="true"></i></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            <div id="content" class="col-sm-9 categorypage">
                <h3 class="refine-search">Refine Search</h3>
                <div class="category_filter">
                    <div class="col-md-4 btn-list-grid">
                        <div class="btn-group">
                            <button type="button" id="grid-view" class="btn btn-default grid" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                            <button type="button" id="list-view" class="btn btn-default list" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                        </div>
                    </div>
                    <div>
                        <div class="sort-by-wrapper">
                            <div class="col-md-2 text-right sort-by">
                                <label class="control-label" for="input-sort">Find:</label>
                            </div>
                            <div class="col-md-3 text-right sort">
                                <form action="{{route('search')}}" autocomplete="off">
                                    <div class="inner-addon right-addon">
                                        <input type="text" name="search" placeholder="Search Product"  class="form-control" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="pagination-right">
                        <div class="sort-by-wrapper">
                            <div class="col-md-2 text-right sort-by">
                                <label class="control-label" for="input-sort">Sort By:</label>
                            </div>
                            <div class="col-md-3 text-right sort">
                                <select id="input-sort" class="form-control" onchange="location = this.value;">
                                    <option value="" selected="selected">Default</option>
                                </select>
                            </div>
                        </div>
                     </div>
                </div>
                <div class="row">
                    <div class="product-info">
                    </div>
                    @foreach($products as $product)
                        @php
                        $images= $product->image;
                        $img=explode(",",$images);
                        @endphp
                        <div class="product-layout product-list col-xs-12">
                            <div class="product-block product-thumb">
                                <div class="product-block-inner">
                                    <div class="image">
                                        <a href="{{route('productDetail',[$product->id])}}">
                                            <img src="{{url('/images/'.$img[0])}}" title="{{$product->name}}" alt="{{$product->name}}" class="img-responsive reg-image" />
                                         </a>
                                    </div>
                                    <div class="product-details">
                                        <div class="btn-list-grid">
                                        </div>
                                        <div class="caption">
                                            <h4><a href="{{$product->id}}">{{$product->name}}</a></h4>
                                            <p class="price">
                                                Rs.{{$product->price}}
                                            </p>
                                            <div class="button-group">
                                                <a onclick="addToCart({{$product->id}})">
                                                    <button  type="button" data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart listaddtocart">
                                                        Add to Cart
                                                    </button>
                                                </a>
{{--                                                <div class="quickview" data-placement="top" data-toggle="" data-target="" value="{{$product->id}}" title="Quickview" onclick="viewDetails({{$product->id}})">Quick View--}}
{{--                                                </div>--}}`
                                                <button class="wishlist" value="{{$product->id}}" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List "><i class="fa fa-heart"></i></button>
                                                <button  class="compare" type="button" value="{{$product->id}}" data-toggle="tooltip" data-placement="top" title="Compare this Product "><i class="fa fa-exchange"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="text-center ">
            {{$products->links() }}
        </div>
    </div>
    <input type="hidden" id="token" value="{{csrf_token()}}">
    <input type="hidden" id="data" value="">
    @include('dashboards.projectjs')

@endsection
