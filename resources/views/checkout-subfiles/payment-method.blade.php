
<h1>Please select the preferred payment method to use on this order.</h1>

<div class="buttons">
        &nbsp;
        <input type="button" value="Cash On Delivery" id="button-payment-method" data-loading-text="Loading..." class="btn btn-primary" />
@if(Auth()->user()->email_verified_at==null)
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            Pay Online
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Verify Your E-mail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        YOUR E-MAIL IS NOT VERIFIED
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if(Auth()->user()->email_verified_at!==null)
    <a  href="{{route('test_view')}}" class="btn btn-primary"  data-loading-text="Loading...">Pay Online</a>
@endif



</div>
</div>
<script>

</script>

