
<p>Please select the preferred shipping method to use on this order.</p>
<p><strong>Flat Rate</strong></p>
<div class="radio">
    <label>         <input type="radio" name="shipping_method" value="flat.flat" checked="checked" />
        Flat Shipping Rate - Rs 100.00</label>
</div>
<p><strong>Add Comments About Your Order</strong></p>
<p>
    <textarea name="comment" rows="8" class="form-control"></textarea>
</p>
<div class="buttons">
    <div class="pull-right">
        <input type="button" value="Continue" id="button-shipping-method" data-loading-text="Loading..." class="btn btn-primary" />
    </div>
</div>
