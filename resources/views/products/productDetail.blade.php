@extends('layouts.master')
@section('title', 'Product Detail')
@section('body')
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Product Detail Page</a></li>
    </ul>
    <div class="tab-title-main">
        <ul class="etabs nav nav-tabs">
            <li class="nav-item active">
                <a href="#" data-toggle="tab" class="nav-link">Product Detail</a>
            </li>
        </ul>
    </div>
    <div id="product-product" class="container">
        <div class="row">
            <div id="content" class="col-sm-12 productpage">
                <div class="row">
                    <div class="col-sm-8 product-left">
                        <div class="product-info">
                            <div class="image"><a class="thumbnail" title=""><img id="tmzoom" src="{{url('/images/'.$parray[0])}}" data-zoom-image="{{url('/images/'.$parray[0])}}" title="" alt="" /></a></div>
                        </div>
                        <div class="additional-carousel">
                            <div id="additional-arrows" class="arrows"></div>
                            <div id="additional-slider" class="image-additional additional-slider">
                                @foreach($parray as $image)
                                <div class="slider-item">
                                    <div class="product-block">
                                        <a title="Aenean quis" class="elevatezoom-gallery" data-image="" data-zoom-image="{{url('/images/'.$image)}}"><img src="{{url('/images/'.$image)}}" onclick="getImage(this)" width="74" height="74" title="Aenean quis" alt="Aenean quis" /></a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 product-right">
                        <h3 class="product-title">Karyana</h3>
                        <div class="rating-wrapper">
                            @if($rating)
                            @foreach($rating as $productRating)
                                <span style="visibility: hidden"> {{$xyz = 5 - $productRating->rating}}</span>
                                @for($i=0;$i<$productRating->rating;$i++)
                                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                @endfor
                                    @for($i=0;$i<$xyz;$i++)
                                        <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                    @endfor
                                @endforeach
                            @else
                                 @for($i=0;$i<5;$i++)
                                     <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                 @endfor
                            @endif
                                <a href="#" class="review-count " @if(Auth::user()) onclick="$('a[href=\'#tab-review\']').trigger('click');
                                         return false;"@endif>
                                <span class="text-success">reviews</span></a>
                                     <a href="#" @if(Auth::user()) onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"@endif class="write-review">
                                <i class="fa fa-pencil" ></i><span class="text-success">Write a review</span></a>
                        </div>
                        <ul class="list-unstyled short-desc">
                            <li>
                                <span class="desc">Name:</span>{{$product->name}}
                            </li>
                            <li>
                                <span class="desc">Shop:</span>{{$product->shop['name']}}
                            </li>
                            <li>
                                <span class="desc">Availability:</span>In Stock
                            </li>
                        </ul>
                        <ul class="list-unstyled price">
                            <li>
                                <h3 class="product-price" >Price: Rs.{{$product->price}}</h3>
                            </li>
                        </ul>
                        <div id="product">
                            <div class="form-group cart">
                                <label class="control-label qty" for="input-quantity">Qty</label>
                                <input type="number" name="quantity" value="1" min="1" size="2" id="input-quantity" class="form-control" />
                                <button type="button" id="button-cart" data-loading-text="Loading..." class="btn btn-primary btn-lg btn-block" onclick="addToCart({{$product->id}})">Add to Cart</button>
                                <!-- <span>  - OR -  </span> -->
                                <div class="btn-group">
{{--                                    <button type="button"  class="btn btn-default wishlist" title="Add to Wish List" onclick="addToCart({{$product->id}})">Add to Wish List</button>--}}
{{--                                    <button type="button" class="btn btn-default compare" title="Compare this Product" onclick="compare.add('48');">Compare this Product</button>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tabs_info" class="product-tab col-sm-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
{{--                            <li><a href="{{ route('login') }}" >Reviews </a></li>--}}
                            <li><a @if(Auth::user()) href="#tab-review" data-toggle="tab" @else href="{{ route('login') }}" @endif>Reviews </a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="">
                                <div id="printTarget"></div>
                            </div>
                            <div class="tab-pane active" id="tab-description">
                                <p>{{$product->description}}</p>
                            </div>
                            <div class="tab-pane" id="tab-review">
                                <form class="needs-validation"  novalidate>
                                    <input type="hidden" name="product_id" id="product_id" value="{{$product->id}}">
                                    <input type="hidden" name="user_id" id="user_id" value="2">
                                    <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
                                    <div id="review">
                                    </div>
                                    <h4>Write a review</h4>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="input-review">Your Review</label>
                                            <textarea name="message" rows="5" id="input-review" class="form-control"></textarea>
                                            <div class="help-block"><span class="text-danger">Note:</span> HTML is not translated!</div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label">Rating</label>
                                            &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                            <input type="radio" name="rating" onclick="rate(1)" value="1" />
                                            &nbsp;
                                            <input type="radio" name="rating" onclick="rate(2)" value="2" />
                                            &nbsp;
                                            <input type="radio" name="rating" onclick="rate(3)" value="3" />
                                            &nbsp;
                                            <input type="radio" name="rating" onclick="rate(4)" value="4" />
                                            &nbsp;
                                            <input type="radio" name="rating" onclick="rate(5)" value="5" />
                                            &nbsp;Good</div>
                                    </div>
                                    <div class="buttons clearfix">
                                        <div class="pull-right">
                                            <button type="button" onclick="feedback()" id="button-review" data-loading-text="Loading..." class="btn btn-primary">Continue</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="box related">
                        <div class="ax-title">
                            <div class="box-heading"><span>Related Products</span></div>
                        </div>
                        <div class="box-content">
                            <div id="products-related" class="related-products">
                                <div id="related-arrows" class="arrows"> </div>
                                <div class="box-product related-slider" id="related-slider">
                                    @foreach($relatedProduct as $relatedProducts)
                                        <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="product-block product-thumb transition">
                                                <div class="product-block-inner">
                                                    <div class="image">
                                                        <a href="{{route('productDetail',[$relatedProducts->id])}}">
                                                            <img src="{{url('/images/'.$relatedProducts->image)}}" title="{{$relatedProducts->name}}" alt="{{$relatedProducts->name}}" class="img-responsive reg-image" />
                                                            <img class="img-responsive hover-image" src="{{url('/images/'.$relatedProducts->image)}}" title="{{$relatedProducts->name}}" alt="{{$relatedProducts->name}}" />
                                                        </a>
                                                    </div>
                                                    <div class="caption">
                                                    <span class="rating">
                                                        <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                    </span>
                                                        <h4><a href="{{route('productDetail',[$relatedProducts->id])}}">{{$relatedProducts->name}}</a></h4>
                                                        <p class="price">
                                                            <span class="price-new">Rs.{{$relatedProducts->price}}</span>
                                                        </p>
                                                        <div class="button-group">
                                                            <a onclick="addToCart({{$relatedProducts->id}})">
                                                               <button  type="button" data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart listaddtocart">
                                                                    Add to Cart
                                                                </button>
                                                            </a>
                                                           <button class="wishlist" value="{{$relatedProducts->id}}" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List "><i class="fa fa-heart"></i></button>
                                                            <button class="compare" type="button" value="{{$relatedProducts->id}}" data-toggle="tooltip" data-placement="top" title="Compare this Product "><i class="fa fa-exchange"></i></button>
                                                         </div>
                                                    </div>
                                                    <!-- Aeipix Related Products Start -->
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboards.projectjs')
    <script>
    window.onload = function() {
        var product = document.getElementById('product_id').value;
        $.ajax({
            type : 'get',
            url  : '{{route("fetch")}}',
            data : {'product_id': product},
            dataType : 'json',
            success : function (response) {
                var length = response.length;
                for(var i = 0 ; i < length ; i++)
                {
                    var printComment = '<p>'+response[i].message+'</p><a class="btn btn-sm btn-outline-danger" onclick="deleteComment('+response[i].id+')">Delete</a><br>';
                    $("#printTarget").append(printComment );
                }
            }
        });
    }
    var ratingValue;
    function rate(value)
    {
        ratingValue = value;
    }
    function feedback() {
        // if (Auth::user){}
        var productID = document.getElementById('product_id').value;
        {{--var userID = {{Auth::user()->id}}--}}
        var userID = document.getElementById('user_id').value;
        // console.log(userID);
        // alert(userID);
        var token = document.getElementById('token').value;
        var message = document.getElementById('input-review').value;
        $('#printTarget').append("<p>"+message+"</p><br>");
        // alert(message);
        $.ajax({
            type : 'post',
            url  : '{{route("post-feedback")}}',
            data : {'product_id': productID , 'user_id': userID, '_token':token, 'message':message, 'rating':ratingValue,},
            success:function () {
                // console.log('yes');
                document.getElementById('input-review').value = '';
            },
        });
    }
    function deleteComment(id)
    {
        $.ajax({
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url : "{{route('deleteComment')}}",
            data : {'id':id},
            success : function (response) {
                if(response){
                    location.reload()
                }
            }
        });
    }


</script>
<script>
    function getImage(x) {
      var src =  document.getElementById('tmzoom');
      var temp = src.getAttribute('src');
      src.setAttribute('src',x.src) ;
      x.src = temp;
    }
</script>
{{--    <script>--}}
{{--        var token=document.getElementById('token').value;--}}
{{--        $(document).ready(function() {--}}
{{--            $(".wishlist").click(function () {--}}
{{--                var id = $(this).val();--}}
{{--                $.ajax({--}}
{{--                    url: '/Wish-list/'+ id,--}}
{{--                    type: 'get',--}}
{{--                    // dataType: 'json',--}}
{{--                    data: {'id': id},--}}
{{--                    success: function (data) {--}}
{{--                        $.notify({--}}
{{--                            message: "Successful Added Into Wish List",--}}
{{--                            target: '_blank'--}}
{{--                        }, {--}}
{{--                            // settings--}}
{{--                            element: 'body',--}}
{{--                            position: null,--}}
{{--                            type: "info",--}}
{{--                            allow_dismiss: true,--}}
{{--                            newest_on_top: false,--}}
{{--                            placement: {--}}
{{--                                from: "top",--}}
{{--                                align: "center"--}}
{{--                            },--}}
{{--                            offset: 0,--}}
{{--                            spacing: 10,--}}
{{--                            z_index: 2031,--}}
{{--                            delay: 5000,--}}
{{--                            timer: 1000,--}}
{{--                            url_target: '_blank',--}}
{{--                            mouse_over: null,--}}
{{--                            animate: {--}}
{{--                                enter: 'animated fadeInDown'--}}
{{--                                //exit: 'animated fadeOutUp'--}}
{{--                            },--}}
{{--                            onShow: null,--}}
{{--                            onShown: null,--}}
{{--                            onClose: null,--}}
{{--                            onClosed: null,--}}
{{--                            icon_type: 'class',--}}
{{--                            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +--}}
{{--                                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">&nbsp;&times;</button>' +--}}
{{--                                '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +--}}
{{--                                '<div class="progress" data-notify="progressbar">' +--}}
{{--                                '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +--}}
{{--                                '</div>' +--}}
{{--                                '<a href="{3}" target="{4}" data-notify="url"></a>' +--}}
{{--                                '</div>'--}}
{{--                        });--}}
{{--                    }--}}
{{--                });--}}
{{--            })--}}
{{--        });--}}


{{--    </script>--}}
@endsection
