@extends('layouts.dashboard')

@section('title','Add Product')

@section('body')
    <head>
{{--                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    </head>
    @if(session('message'))
        <p class="alert alert-success">{{session('message')}}</p>
    @endif
    <div class="col-sm-12 offset-1" >
        <h2 class="align-content-center"> Add Product</h2>
            <form action="{{route('update_product')}}" method="post" class="needs-validation" enctype="multipart/form-data" novalidate>
            {{csrf_field()}}
                <div class="form-group">
{{--                    @dd($product->id)--}}
                    <input type="hidden" name="id" value="{{$product->id}}">
                </div>
            <div class="form-row ">
                <div class="row col-md-12">
                    <div class="col-md-5">
                        <label for="exampleFormControlSelect1" class="">Select Shop</label>
                        <select class="form-control @error('shop_id') is-invalid @enderror" name="shop_id" id="validationCustom06" required>
                        <option value="" selected="" disabled="">Please select shop</option>
                        @foreach($shops as $shop)
                            <option value="{{$shop->id}}">{{$shop->name}}</option>
                        @endforeach
                         </select>
                        <div class="invalid-feedback">
                          Please Add Shop Name
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <label for="exampleFormControlSelect1" class="">Catagory</label>
                        <div class="invalid-feedback">
                            Please Select catagory
                        </div>
                        <select class="form-control" name="catagories_id" id="validationCustom12"  required >
                            <option value="" selected="" disabled=""> Please Select catagory</option>

                            @foreach($catgory as $categories)
                                @if($categories->parent_id==NULL)

                                    <option value="{{$categories->id }}">
                                        {{ $categories->name }}
                                    </option>

                                    @if(count($categories->childs))
                                        @include('products.managProduct',['childs' => $categories->childs,'submark'=> '-'])
                                    @endif

                                @endif

                            @endforeach

                        </select>
                    </div>
                </div>
                <div class="row col-sm-12">
                    <div class="col-sm-5">
                        <label for="validationCustom01" class="">Product Name</label>
                        <input type="text" name="product_name" class="form-control @error('product_name') is-invalid @enderror" id="validationCustom01" placeholder="Product name" value="{{$product->name}}" required>
                        <div class="invalid-feedback">
                        Product Name Required
                        </div>
                        @error('product_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-sm-5">
                        <label for="validationCustom01" class="">Product description</label>
                        <textarea name="description"  class="form-control @error('description') is-invalid @enderror" id="validationCustom02" cols="30" rows="2" placeholder="Add Product Description" required>{{$product->description}}
                            </textarea>
                            <div class="invalid-feedback">
                                Please Add description
                            </div>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                <div class="row col-sm-12">
                    <div class="col-sm-5">
                        <label for="validationCustom03" class="">Price</label>
                            <input type="number" name="price" value="{{$product->price}}" class="form-control @error('price') is-invalid @enderror" min="0" id="validationCustom03" placeholder="Price" required>
                            <div class="invalid-feedback">
                                Please Add Product Price.
                            </div>
                            @error('price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                    </div>
                    <div class="col-sm-2">
                        <label for="start" class="">Manufacture Date:</label>
                        <input type="text" id="datefrom" value="{{$product->mfc_date}}" class="form-control" name="mfc_date" placeholder="Manufacture date" />
                    </div>
                    <div class="col-sm-2 ml-3">
                        <label for="start" class="">Manufacture Date:</label>
                        <input type="text"  value="{{$product->exp_date}}" class="form-control" id="dateTo" name="exp_date" placeholder="Expiry Date"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" style="">Upload Image</label>
                <div class="col-md-8">
                    <div  class="row" id="coba" >
                        <div></div>
                    </div>
                </div>
            </div>
            <button class="btn text-light" style="background: #74ab26" type="submit">Save Product</button>
        </form>
        @include('dashboards.projectjs')
        <script type="text/javascript">
            $(document).ready(function () {
                $("#datefrom").datepicker({
                    onClose: function () {
                        $("#dateTo").datepicker(
                            "change", {
                                minDate: new Date($('#datefrom').val())
                            });

                    },
                    dateFormat: 'yy-mm-dd'
                });

                $("#dateTo").datepicker({
                    onClose: function () {
                        $("#datefrom").datepicker(
                            "change", {
                                maxDate: new Date($('#dateTo').val())
                            });
                    },
                    dateFormat: 'yy-mm-dd'
                });
            });
        </script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script type="text/javascript">
            $(function(){

                $("#coba").spartanMultiImagePicker({
                    fieldName:        'fileUpload[]',
                    maxCount:         5,
                    rowHeight:        '200px',
                    groupClassName:   'col-md-4 col-sm-4 col-xs-6',
                    maxFileSize:      '',
                    placeholderImage: {
                        image: 'placeholder.png',
                        width : '100%'
                    },
                    dropFileLabel : "Drop Here",
                    onAddRow:       function(index){
                        console.log(index);
                        console.log('add new row');
                    },
                    onRenderedPreview : function(index){
                        console.log(index);
                        console.log('preview rendered');
                    },
                    onRemoveRow : function(index){
                        console.log(index);
                    },
                    onExtensionErr : function(index, file){
                        console.log(index, file,  'extension err');
                        alert('Please only input png or jpg type file')
                    },
                    onSizeErr : function(index, file){
                        console.log(index, file,  'file size too big');
                        alert('File size too big');
                    }
                });
            });
        </script>
    </div>

@endsection
