@extends('layouts.dashboard')

@section('title','Add Product')

@section('body')
    <head>
{{--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    </head>
<div class="col-sm-12 offset-1" >
{{--    <a href="#" class=""><img  alt="cat-1"></a>--}}

    <h2 class="align-content-center"> Add Product</h2>
    <!-- Page Heading -->
    <form action="{{route('add_product')}}" method="post" class="needs-validation" enctype="multipart/form-data" novalidate>
        {{csrf_field()}}
        @if(session('message'))
            <p class="alert alert-success">{{session('message')}}</p>
        @endif
        <div class="form-row ">
            <div class="row col-md-12">
                <div class="col-md-5">
                    <label for="exampleFormControlSelect1" class="">Select Shop</label>
                    <select class="form-control multiple @error('shop_id') is-invalid @enderror" value="{{old('shop_id')}}" name="shop_id" id="validationCustom06"  required>
                        <option value="" selected="" disabled="">Please select shop</option>
                        @foreach($shops as $shop)
                            <option value="{{$shop->id}}">{{$shop->name}}</option>
                        @endforeach
                        @error('shop_id')
                        <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </select>
                    <div class="invalid-feedback">
                        Please Add Shop Name
                    </div>
                </div>

{{--                @dd($catgory);--}}
                <div class="col-sm-5">
                    <label for="exampleFormControlSelect1" class="">Catagory</label>
                    <div class="invalid-feedback">
                        Please Select catagory
                    </div>
                    <select class="form-control" name="catagories_id" id="validationCustom12"  required >
                        <option value="" selected="" disabled=""> Please Select catagory</option>

                        @foreach($catgory as $categories)
                            @if($categories->parent_id==NULL)

                            <option value="{{$categories->id }}">
                               {{ $categories->name }}
                            </option>

                                @if(count($categories->childs))
                                    @include('products.managProduct',['childs' => $categories->childs,'submark'=> '-'])
                                @endif

                            @endif

                        @endforeach

                    </select>
                </div>
            </div>
            <div class="row col-sm-12">
                <div class="col-sm-5">
                    <label for="validationCustom01" class="">Product Name</label>
                    <input type="text" name="product_name" class="form-control @error('product_name') is-invalid @enderror" value="{{ old('product_name') }}" id="validationCustom01" placeholder="Product name" value="" required>
                    <div class="invalid-feedback">
                        Product Name Required
                    </div>
                    @error('product_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>
                <div class="col-sm-5">
                    <label for="validationCustom02" class="">Product Description</label>
                    <textarea name="description"  class="form-control @error('description') is-invalid @enderror" value="{{old('description')}}" id="validationCustom02"  placeholder="Add Product Description" required></textarea>
                    <div class="invalid-feedback">
                        Please Add description
                    </div>
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="row col-md-12">
                    <div class="col-md-5">
                        <label for="validationCustom03" class="">Price</label>
                        <input type="number" name="price" class="form-control  @error('price') is-invalid @enderror" value="{{old('price')}}" min="0" id="validationCustom03" placeholder="Price" required>
                        <div class="invalid-feedback">
                            Please Add Product Price.
                        </div>
                        @error('price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                <div class="col-md-2">
                    <label for="start" class="">Manufacture Date:</label>
                    <input type="text" id="datefrom" class="form-control @error('mfc_date') is-invalid @enderror" value="{{old('mfc_date')}}" name="mfc_date" placeholder="Manufacture date" autocomplete="off" />
                    @error('mfc_date')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-md-2 ml-3">
                    <label for="start" class="">Expiry Date:</label>
                    <input type="text" id="dateTo" class="form-control @error('exp_date') is-invalid @enderror" value="{{old('exp_date')}}" name="exp_date" placeholder="Expiry Date" autocomplete="off"/>
                    @error('exp_date')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                    @enderror
                </div>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3" style="">Upload Image</label>
            <div class="col-md-8">
                <div  class="row" id="coba">
                    <div></div>
                </div>
            </div>
        </div>
        <button class="btn text-light" style="background: #74ab26" type="submit">Save Product</button>
    </form>
    @include('dashboards.projectjs')
    <script>
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
 <script>
        $(document).ready(function () {
            $("#datefrom").datepicker({
                onClose: function () {
                    $("#dateTo").datepicker(
                        "change", {
                            minDate: new Date($('#datefrom').val())
                        });
                },
                dateFormat: 'yy-mm-dd'
            });

            $("#dateTo").datepicker({
                onClose: function () {
                    $("#datefrom").datepicker(
                        "change", {
                            maxDate: new Date($('#dateTo').val())
                        });
                },
                    dateFormat: 'yy-mm-dd'
            });
        });
    </script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(function(){

            $("#coba").spartanMultiImagePicker({
                fieldName:        'fileUpload[]',
                maxCount:         5,
                rowHeight:        '200px',
                groupClassName:   'col-md-4 col-sm-4 col-xs-6',
                maxFileSize:      '',
                placeholderImage: {
                    image: 'data-spartanindexi',
                    width : '100%'
                },
                dropFileLabel : "Drop Here",
                onAddRow:       function(index){
                    console.log(index);
                    console.log('add new row');
                },
                onRenderedPreview : function(index){
                    console.log(index);
                    console.log('preview rendered');
                },
                onRemoveRow : function(index){
                    console.log(index);
                },
                onExtensionErr : function(index, file){
                    console.log(index, file,  'extension err');
                    alert('Please only input png or jpg type file')
                },
                onSizeErr : function(index, file){
                    console.log(index, file,  'file size too big');
                    alert('File size too big');
                }
            });
        });
    </script>
</div>

@endsection
                                                                                                                                
