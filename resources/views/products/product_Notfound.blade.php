@extends('layouts.master')
@section('title', 'Product Notfound')
@section('body')
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#">No Result Found </a></li>
    </ul>
    <hr>

    <div id="tabs" class="htabs">
        @if(session()->has('message'))
            <p class="alert alert-success">
                {{session()->get('message')}}
            </p>
        @endif
            <div class="tab-title-main">
                <ul class="etabs nav nav-tabs">
                    <li class="nav-item active">
                        <a href="#" data-toggle="tab" class="nav-link">Product Not Found</a>
                    </li>
                </ul>
            </div>
    </div>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-9 categorypage">
                <h3 class="refine-search">Refine Search</h3>
                <div class="category_filter">
                    <div class="col-md-4 btn-list-grid">
                        <div class="btn-group">
                            <button type="button" id="grid-view" class="btn btn-default grid" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                            <button type="button" id="list-view" class="btn btn-default list" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                        </div>
                    </div>
                    <div>
                        <div class="sort-by-wrapper">
                            <div class="col-md-2 text-right ">
                                <label class="control-label" for="input-sort">Find:</label>
                            </div>
                            <div class="col-md-8 text-right">
                                <form action="{{route('search')}}" autocomplete="off">
                                    <div class="inner-addon right-addon">
                                        <input type="text" name="search" placeholder="Search Product"  class="form-control" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="pagination-right">
                        <div class="sort-by-wrapper">
                            <div class="col-md-2 text-right sort-by">
                                <label class="control-label" for="input-sort">Sort By:</label>
                            </div>
                            <div class="col-md-3 text-right sort">
                                <select id="input-sort" class="form-control" onchange="location = this.value;">
                                    <option value="" selected="selected">Default</option>
                                    <option value="">Name (A - Z) </option>
                                    <option value="">Name (Z - A) </option>
                                    <option value="">Price (Low &gt; High) </option>
                                    <option value="">Price (High &gt; Low) </option>
                                </select>
                            </div>
                        </div>
{{--                        <div class="show-wrapper">--}}
{{--                            <div class="col-md-1 text-right show">--}}
{{--                                <label class="control-label" for="input-limit">Show:</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-2 text-right limit">--}}
{{--                                <select id="input-limit" class="form-control" onchange="location = this.value;">--}}
{{--                                <option>10</option>--}}
{{--                                <option>20</option>--}}
{{--                                <option>30</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>


    {{--    <h1>Error No Result Found</h1>--}}

    <aside id="column-left" class="col-sm-3 hidden-xs">
        <div class="box">
            <div class="box-heading">Categories</div>
            <div class="box-content">
                <ul class="box-category treeview-list treeview">
                    @foreach($categories->where('parent_id',0) as $cat)
                        <li>
                            <a href="#" >{{$cat->name}}</a>
                            @if($cat->childs->count()>0)
                                <ul>
                                    @foreach($cat->childs as $submenu)
                                        <li>
                                            <a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a>
                                        </li>
                                    @endforeach

                                </ul>
                            @endif
                        </li>
                    @endforeach

                </ul>
            </div>
        </div>
        <div class="box">
            <div class="box-heading">Bestsellers</div>
            <div class="box-content">

                <div class="box-product  productbox-grid" id=" bestseller-grid">
                    <div class="product-items col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">

                                <div class="image">
                                    {{--                                            <a href="{{route('productDetail',[$products[0]->id])}}"><img src="{{url('/images/'.$products[0]->image)}}" alt="" title="{{$products[0]->name}}" class="img-responsive" /></a>--}}
                                </div>
                                <div class="product-details">
                                    <div class="caption">
                                        {{--                                                <h4><a href="{{route('productDetail',[$products[0]->id])}}">{{$products[0]->name}}</a></h4>--}}

                                        <p class="price">
                                            {{--                                                    Rs.{{$products[0]->price}}--}}
                                            {{--                                                    <span class="price-tax">{{$products[0]->price}}</span>--}}
                                            <span class="rating">
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                        </span>
                                        </p>
                                        <div class="button-group">
                                            <button type="button" class="addtocart" onclick="cart.add('45 ');">Add to Cart </button>
                                            <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('45 ');"><i class="fa fa-heart"></i></button>
                                            <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product " onclick="compare.add('45 ');"><i class="fa fa-exchange"></i></button>
                                            <div class="quickview" data-toggle="tooltip" data-placement="top" title="Quickview"><a href="#">Quick View<i class="fa fa-eye" aria-hidden="true"></i>
                                                </a></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-heading">Latest</div>
            <div class="box-content">

                <div class="box-product  productbox-grid" id=" latest-grid">
                    <div class="product-items col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <div class="image">
                                    {{--                                            <a href="{{route('productDetail',[$latestProduct->id])}}"><img src="{{url('/images/'.$latestProduct->image)}}" alt="" title="" class="img-responsive" /></a>--}}

                                </div>
                                <div class="product-details">
                                    <div class="caption">
                                        {{--                                                <h4><a href="{{route('productDetail',[$latestProduct->id])}}">{{$latestProduct->name}} </a></h4>--}}
                                        <div class="rating">
                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                        </div>
                                        <p class="price">
                                            {{--                                                    Rs.{{$latestProduct->price}}--}}
                                            {{--                                                    <span class="price-tax">{{$latestProduct->price}}</span>--}}
                                        </p>
                                        <div class="button-group">
                                            <button type="button" class="addtocart" onclick="cart.add('49 ');">Add to Cart </button>
                                            <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('49 ');"><i class="fa fa-heart"></i></button>
                                            <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product " onclick="compare.add('49 ');"><i class="fa fa-exchange"></i></button>
                                            {{--                                                    <div class="quickview" data-toggle="tooltip" data-placement="top" title="Quickview"><a href="{{url('/images/'.$latestProduct->image)}}">Quick View<i class="fa fa-eye" aria-hidden="true"></i>--}}
                                            {{--                                                        </a>--}}
                                            {{--                                                    </div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </aside>
            <div class="row shop_wrapper" id="tag_container">
                <div class="mx-auto">
                    <img src="http://127.0.0.1:8000/assets/frontend/image/NoRecordFound.png">
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
