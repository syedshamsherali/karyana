{{--=================================Product Quick View================================================--}}
<div class="modal " id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Detail</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
            <div class="row">
            <div class="col-md-12 row divider">
{{--                @php--}}
{{--                    $date = Carbon::createFromFormat('Y-m-d', $detail->exp_date)--}}
{{--                @endphp--}}
{{--               @dd(date('d-m-Y', strtotime($detail->exp_date)));--}}
                <div class="col-md-6">

                            @php
                            $images = $detail->image;
                            $img = explode(",",$images);
                            @endphp
{{--                            <img style="width: 400px; height: 400px" src="{{ asset("images/$detail->image")}}">--}}
                            <img style="width: 400px; height: 400px" src="{{ asset("images/$img[0]")}}">


                </div>
                        <div class="col-md-6 pull-left border-left-dark border-bottom-dark">
                           <h5>Name:{{$detail->name}}</h5>
                            <br>
                           <h6>Store:{{$detail->Shop['name']}}</h6>
                            <br>
                            <h6>Description:</h6>
                            <p>{{$detail->description}}</p>
                            <h6>Manufacture:</h6>{{date('d-m-Y', strtotime($detail->exp_date))}}
                            <h6>Expiry:</h6>{{date('d-m-Y', strtotime($detail->exp_date))}}
                        </div>
            </div>
            </div>
            </div>


            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
{{--=================================Product Quick View================================================--}}
