@extends('layouts.dashboard')
@section('title','Product List')
@section('body')
    <div id="mydiv"> </div>
    <div id="content-wrapper">
        <div class="container-fluid">
            @if(session('message'))
                    <p class="alert alert-success">{{session('message')}}</p>
                @endif
            <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        <b>Karyana Products</b>
                    </div>
                    <div class="card-body">
                        <div id="no-more-tables">
                            <table class="table table-responsive table-bordered text-center" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead class="bg-grey">
                                <tr class="">
                                    <th>Product Name</th>
                                    <th>Shop Name</th>
                                    <th>Description</th>
                                    <th>Quick View</th>
                                    <th>Price</th>
                                    <th>Manufacture Date</th>
                                    <th>Expiry Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)

                                    <tr>
                                        <td >{{$product->name}}</td>
                                        <td >{{$product->Shop['name']}}</td>
                                        <td >{{$product->description}}</td>
                                        <td ><!-- Button trigger modal-->
                                            <a href="#" class="bd-example-modal-lg"><i data-myid="{{$product->id}}"  class="fa fa-eye detail bd-example-modal-lg"></i></a>
{{--                                            <a href="" data-toggle="modal" aria-valuemax="" data-target="#modalQuickView"> <i class="fa fa-eye" style="font-size:24px;color: #0b0b0b"></i></a>--}}
                                        </td>
                                        <td >{{$product->price}}</td>
                                        <td >{{$product->mfc_date}}</td>
                                        <td >{{$product->exp_date}}</td>
                                        <td>
                                            <div class="dropdown">
                                                <span  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-h"></i>
                                                </span>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a href="{{route('edit_product',[$product->id])}}" class="btn btn-primary">Edit</a>
                                                    <a href="{{route('delete_product',[$product->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                         </div>
                    </div>
                    <div class="card-footer small text-muted">Last Updated
                        at {{$product->updated_at->format('d M, Y h:i A')}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--end Quick View Modal   --}}
    <input type="hidden" class="token" value="{{csrf_token()}}">
    <script>
        var token = $('.token').val();
        $('.detail').on('click', function () {
            var id = $(this).data('myid');
            $.ajax({
                type: 'post',
                data_type: 'json',
                url: "quickview",
                data: {"_token": token, "id": id},
                success: function (data) {
                    // console.log(data);
                    $("#mydiv").empty().append(data);
                    $('#myModal').modal('show');
                },
            });
        });
    </script>
{{--end Quick View Modal   --}}
@endsection
