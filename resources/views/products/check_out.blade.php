@extends('layouts.master')
@section('title', 'checkout')
{{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
@section('body')


    <style>
        /**
    * The CSS shown here will not be introduced in the Quickstart guide, but shows
    * how you can use CSS to style your Element's container.
    */
        .StripeElement {
            box-sizing: border-box;

            height: 40px;

            padding: 10px 12px;

            border: 1px solid transparent;
            border-radius: 4px;
            background-color: white;

            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
            width: 100%;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }

    </style>

    <? use Illuminate\Support\Facades\Auth;?>
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Check Out</a></li>
    </ul>
    <hr>
    <div id="tabs" class="htabs">
        @if(session()->has('message'))
            <p class="alert alert-success">
                {{session()->get('message')}}
            </p>
        @endif
        <div class="tab-title-main">
            <ul class="etabs nav nav-tabs">
                <li class="nav-item active">
                    <a href="#" data-toggle="tab" class="nav-link">Check Out</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <aside id="" class="col-sm-3">
                <div class="box">
                    <div class="box-heading">Categories</div>
                    <div class="box-content">
                        <ul class="box-category treeview-list treeview">
                            @foreach($categories->where('parent_id',0) as $cat)
                                <li>
                                    <a href="#" >{{$cat->name}}</a>
                                    @if($cat->childs->count()>0)
                                        <ul>
                                            @foreach($cat->childs as $submenu)
                                                <li>
                                                    <a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a>
                                                </li>
                                            @endforeach

                                        </ul>
                                    @endif
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </aside>
            <div id="content" class="col-sm-9">
                <h2 class="page-title">Shop By  categories</h2>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse-checkout-option" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" aria-expanded="false">Step 1: Checkout Options<i class="fa fa-caret-down"></i></a></h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse-checkout-option">
                            <div class="panel-body"></div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" aria-expanded="false">Step 2: Delivery Details <i class="fa fa-caret-down"></i></a></h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse-shipping-address" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle collapsed" aria-expanded="false">Step 3: Delivery Method <i class="fa fa-caret-down"></i></a></h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse-shipping-method" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 4: Payment Method <i class="fa fa-caret-down"></i></a></h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse-payment-method" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Step 5: Confirm Order </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapse-checkout-confirm" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Image</th>
                                            <th class="text-left">Product Name</th>
                                            <th class="text-left">Shop Name</th>
                                            <th class="text-left">Price</th>
                                            <th class="text-left">Quantity</th>
{{--                                            <th class="text-right">Unit Price</th>--}}
                                            <th class="text-right">Total</th>
                                        </tr>
                                        </thead>
                                        @foreach($products as $product)
{{--                                            @dd($product['product']->image)--}}
                                            <tbody>
                                            <tr>
                                                <td>
                                                    @php
                                                        $images=$product['product']->image;
                                                        $img=explode(",",$images);
                                                    @endphp
{{--                                                    @dd($img)--}}
                                                    <img width="60" src="{{asset('images').'/'.$img[0]}}" > </td>
                                                <td class="text-left">{{$product['product']['name']}}</td>
                                                <td class="text-left">{{$product['product']['shop']['name']}}</td>
                                                <td class="text-left" >Rs:{{ $product['product']['price']}}
                                                </td>
                                                <td class="text-left"><div class="input-group btn-block text-center" >
                                                        {{$product['qty']}}
                                                    </div>
                                                </td>
                                                <td class="text-right" >Rs:{{ $product['price']}}</td>

                                            </tr>
                                            </tbody>
                                        @endforeach

                                        <tfoot>

                                        <tr>
                                            <td colspan="5" class="text-right"><strong>Sub-Total:</strong></td>
                                            <td class="text-right">Rs:{{ $totalPrice}}</td>
                                        </tr>
                                        <tr>
{{--                                            <td colspan="4" class="text-right"><strong>Flat Shipping Rate:</strong></td>--}}
{{--                                            <td class="text-right">Rs:{{ $totalPrice /3}}</td>--}}
                                        </tr>
                                        <tr>
                                            <td colspan="5" class="text-right"><strong>Shipping Rate:</strong></td>
                                            {{--                                            <td class="text-right">Rs:{{ $totalPrice + $totalPrice/3}}</td>--}}
                                            <td class="text-right">Rs:{{ $shippingRate }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" class="text-right"><strong>Total:</strong></td>
{{--                                            <td class="text-right">Rs:{{ $totalPrice + $totalPrice/3}}</td>--}}
                                            <td class="text-right">Rs:{{ $totalPrice + $shippingRate  }}</td>
                                        </tr>
                                        </tfoot>

                                    </table>
                                </div>
                                <div class="buttons">


                                    <div class="pull-right">
{{--                                        <a href="{{route('OrderPlacement')}}" type="button" id="button-confirm"  class="btn btn-primary" onClick="alert('Are you sure do you want to place order')">Confirm Order </a>--}}
                                       @if(Auth::check())
                                        @if(Auth()->user()->email_verified_at==!null)
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                            Confirm Order
                                        </button>
                                            @endif
                                        @if(Auth()->user()->email_verified_at==null)
{{--                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">--}}
{{--                                                Confirm Order--}}
{{--                                            </button>--}}

                                        <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModall">
                                                Confirm Order
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModall" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Verify Your E-mail</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            YOUR E-MAIL IS NOT VERIFIED
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @endif

                                    </div>

                                </div>
{{--==============================================================================================================--}}



                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Conformation</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are You Sure Do You Want To Conform This Order
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <a href="{{route('OrderPlacement')}}" type="button" id="button-confirm"  class="btn btn-primary">Confirm Order </a>

{{--                                                <button type="button" class="btn btn-primary">Save changes</button>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
{{--==============================================================================================================--}}

{{--                                <script type="text/javascript"><!----}}
{{--                                    $('#button-confirm').on('click', function() {--}}
{{--                                        location.reload();--}}
{{--                                        $.ajax({--}}
{{--                                            url: '',--}}
{{--                                            dataType: 'json',--}}
{{--                                            beforeSend: function() {--}}
{{--                                                $('#button-confirm').button('loading');--}}
{{--                                            },--}}
{{--                                            complete: function() {--}}
{{--                                                $('#button-confirm').button('reset');--}}
{{--                                            },--}}
{{--                                            success: function(json) {--}}
{{--                                                if (json['redirect']) {--}}
{{--                                                    location = json['redirect'];--}}
{{--                                                }--}}
{{--                                            },--}}
{{--                                            error: function(xhr, ajaxOptions, thrownError) {--}}
{{--                                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);--}}
{{--                                            }--}}
{{--                                        });--}}
{{--                                    });--}}
{{--                                    //--></script>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div></div>
    <script type="text/javascript">
        $(document).on('change', 'input[name=\'account\']', function() {
            if ($('#collapse-payment-address').parent().find('.panel-heading .panel-title > *').is('a')) {
                if (this.value == 'register') {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 2: Account &amp; Billing Details <i class="fa fa-caret-down"></i></a>');
                } else {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 2: Billing Details <i class="fa fa-caret-down"></i></a>');
                }
            } else {
                if (this.value == 'register') {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('Step 2: Account &amp; Billing Details');
                } else {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('Step 2: Billing Details');
                }
            }
        });
        $(document).ready(function() {

                @if(Auth::check()) {

                $.ajax({
                    url: '{{route("shipping-address")}}',
                    dataType: 'html',
                    success: function(html) {
                        $('#collapse-shipping-address .panel-body').html(html);

                        $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 3: Delivery Details <i class="fa fa-caret-down"></i></a>');

                        $('a[href=\'#collapse-shipping-address\']').trigger('click');

                        $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('Step 4: Delivery Method');
                        $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('Step 5: Payment Method');
                        $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('Step 6: Confirm Order');
                    },
                });


            }
                @else{
                // $(document).ready(function () {
                $.ajax({
                    url: '{{route("logincheck")}}',
                    dataType: 'html',
                    success: function (html) {
                        $('#collapse-checkout-option .panel-body').html(html);

                        $('#collapse-checkout-option').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-option" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 1: Checkout Options <i class="fa fa-caret-down"></i></a>');

                        $('a[href="#collapse-checkout-option"]').trigger('click');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
                // });
            }
            @endif
        });
        // Checkout
        $(document).delegate('#button-account', 'click', function() {
            $.ajax({
                url: '{{route("register1")}}',
                dataType: 'html',
                beforeSend: function() {
                    $('#button-account').button('loading');
                },
                complete: function() {
                    $('#button-account').button('reset');
                },
                success: function(html) {

                    $('.form-group').removeClass('has-error');

                    $('#collapse-shipping-address .panel-body').html(html);

                    if ($('input[name=\'account\']:checked').val() == 'register') {
                        $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 2: Register Account<i class="fa fa-caret-down"></i></a>');
                    } else {
                        $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 2: Delivery Details <i class="fa fa-caret-down"></i></a>');
                    }

                    $('a[href=\'#collapse-shipping-address\']').trigger('click');
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
        });

        // Login
        $(document).delegate('#button-login', 'click', function() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("login")}}',
                type: 'post',
                data: $('#collapse-checkout-option :input'),
                // dataType: 'json',
                beforeSend: function() {
                    console.log($('#collapse-checkout-option :input'));
                    $('#button-login').button('loading');
                },
                complete: function() {
                    $('#button-login').button('reset');
                },
                success: function(json) {
                    $('.alert-dismissible, .text-danger').remove();
                    $('.form-group').removeClass('has-error');
                    if (json) {
                        <?php
                            if(Auth::User()){
                                $user=Auth::User();
                                $name=$user->fullname;
                                $address=$user->address;
                                $city=$user->city;
                                $cridancial=[$name,$address,$city];
                            }?>
                            location =  'http://127.0.0.1:8000/check-out';
                    }
                },
                error: function() {
                    // Highlight any found errors
                    $('input[name=\'email\']').parent().addClass('has-error');
                    $('input[name=\'password\']').parent().addClass('has-error');

                }
            });
        });
        // Register
        function abc(){
            // alert('adf');
            var fullname = document.getElementById('fullname');
            var email = document.getElementById('email');
            var user_contact = document.getElementById('user_contact');
            var city = document.getElementById('city');
            var password = document.getElementById('password');
            var password_confirmation = document.getElementById('password_confirmation');
            var address = document.getElementById('address');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("register")}}',
                type: 'post',
                data: {'fullname':fullname.value, 'email':email.value , 'user_contact':user_contact.value, 'city':city.value , 'password':password.value ,'password_confirmation':password_confirmation.value, 'address':address.value},
                // dataType: 'json',
                beforeSend: function() {
                    // $('#button-register').button('loading');
                },
                success: function(json) {
                    location.reload();
                    alert('save');
                    $('.alert-dismissible, .text-danger').remove();
                    $('.form-group').removeClass('has-error');
                    if (json['error']) {
                        $('#button-register').button('reset');
                        //
                        if (json['error']['warning']) {
                            $('#collapse-payment-address .panel-body').prepend('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }
                        //
                        for (i in json['error']) {
                            var element = $('#input-payment-' + i.replace('_', '-'));
                            //
                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }
                        //
                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    } else {
                        var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').prop('value');
                        //
                        if (shipping_address) {
                            $.ajax({
                                url: '{{route("shipping")}}',
                                dataType: 'html',
                                success: function(html) {
                                    // Add the shipping address
                                    $.ajax({
                                        url: '{{route("shipping-address")}}',
                                        dataType: 'html',
                                        success: function(html) {
                                            alert("alert line 2119")
                                            $('#collapse-shipping-address .panel-body').html(html);
                                            //
                                            $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 3z: Delivery Details <i class="fa fa-caret-down"></i></a>');
                                            $('#collapse-shipping-address').show();
                                        },
                                        error: function(xhr, ajaxOptions, thrownError) {
                                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                        }
                                    });
                                    //
                                    $('#collapse-shipping-method .panel-body').html(html);
                                    //
                                    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 4: Delivery Method <i class="fa fa-caret-down"></i></a>');
                                    //
                                    $('a[href=\'#collapse-shipping-method\']').trigger('click');;
                                    //
                                    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('Step 4: Delivery Method');
                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('Step 5: Payment Method');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('Step 6: Confirm Order');
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        } else {
                            $.ajax({
                                url: '{{route("shipping-address")}}',
                                dataType: 'html',
                                success: function(html) {
                                    // alert('line 2147')
                                    $('#collapse-shipping-address .panel-body').html(html);
                                    //
                                    $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 3: Delivery Details <i class="fa fa-caret-down"></i></a>');
                                    //
                                    $('a[href="#collapse-shipping-address"]').trigger('click');
                                    //
                                    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('Step 4: Delivery Method');
                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('Step 5: Payment Method');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('Step 6: Confirm Order');
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        }
                        //
                        $.ajax({
                            url: '{{route("payment-address")}}',
                            dataType: 'html',
                            complete: function() {
                                $('#button-register').button('reset');
                            },
                            success: function(html) {
                                $('#collapse-payment-address .panel-body').html(html);
                                //
                                $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 2: Billing Details <i class="fa fa-caret-down"></i></a>');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        // Payment Address
        $(document).delegate('#button-payment-address', 'click', function() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("payment-address/save")}}',
                type: 'post',
                data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address textarea, #collapse-payment-address select'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-payment-address').button('loading');
                },
                complete: function() {
                    $('#button-payment-address').button('reset');
                },
                success: function(json) {
                    $('.alert-dismissible, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json) {
                        $.ajax({
                            url: '{{route("shipping-address")}}',
                            dataType: 'html',
                            success: function(html) {
                                $('#collapse-shipping-address .panel-body').html(html);

                                $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 2: Delivery Details <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#collapse-shipping-address\']').trigger('click');

                                $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('Step 4: Delivery Method');
                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('Step 5: Payment Method');
                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('Step 6: Confirm Order');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                            }
                        }).done(function() {
                            $.ajax({
                                url: '{{route("payment-address")}}',
                                dataType: 'html',
                                success: function(html) {
                                    $('#collapse-payment-address .panel-body').html(html);
                                },
                                error: function(xhr, ajaxOptions, thrownError) {

                                }
                            });
                        });
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
        });

        // Shipping Address
        $(document).delegate('#button-shipping-address', 'click', function() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("payment-address/save")}}',
                type: 'post',
                data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-shipping-address').button('loading');
                },
                success: function(json) {
                    $('.alert-dismissible, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '{{route("shipping")}}',
                            dataType: 'html',
                            complete: function() {
                                $('#button-shipping-address').button('reset');
                            },
                            success: function(html) {

                                $('#collapse-shipping-method .panel-body').html(html);

                                $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 4: Delivery Method <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#collapse-shipping-method\']').trigger('click');

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('Step 5: Payment Method');
                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('Step 6: Confirm Order');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                            }
                        }).done(function() {
                            $.ajax({
                                url: '{{route("payment-address")}}',
                                dataType: 'html',
                                success: function(html) {
                                    $('#collapse-payment-address .panel-body').html(html);
                                },
                                error: function(xhr, ajaxOptions, thrownError) {

                                }
                            });
                        });
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
        });

        // Guest
        $(document).delegate('#button-guest', 'click', function() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'index.php?route=checkout/guest/save',
                type: 'post',
                data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address textarea, #collapse-payment-address select'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-guest').button('loading');
                },
                success: function(json) {
                    $('.alert-dismissible, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-guest').button('reset');

                        for (i in json['error']) {
                            var element = $('#input-payment-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    } else {
                        var shipping_address = $('#collapse-payment-address input[name=\'shipping_address\']:checked').prop('value');

                        if (shipping_address) {
                            $.ajax({
                                url: '{{route("shipping")}}',
                                dataType: 'html',
                                complete: function() {
                                    $('#button-guest').button('reset');
                                },
                                success: function(html) {
                                    // Add the shipping address
                                    $.ajax({
                                        url: 'index.php?route=checkout/guest_shipping',
                                        dataType: 'html',
                                        success: function(html) {
                                            $('#collapse-shipping-address .panel-body').html(html);

                                            $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 2: Delivery Details <i class="fa fa-caret-down"></i></a>');
                                        },
                                        error: function(xhr, ajaxOptions, thrownError) {

                                        }
                                    });

                                    $('#collapse-shipping-method .panel-body').html(html);

                                    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 3: Delivery Method <i class="fa fa-caret-down"></i></a>');

                                    $('a[href=\'#collapse-shipping-method\']').trigger('click');

                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('Step 5: Payment Method');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('Step 6: Confirm Order');
                                },
                                error: function(xhr, ajaxOptions, thrownError) {

                                }
                            });
                        } else {
                            $.ajax({
                                url: '{{route("shipping-address")}}',
                                dataType: 'html',
                                complete: function() {
                                    $('#button-guest').button('reset');
                                },
                                success: function(html) {

                                    $('#collapse-shipping-address .panel-body').html(html);

                                    $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 3: Delivery Details <i class="fa fa-caret-down"></i></a>');

                                    $('a[href=\'#collapse-shipping-address\']').trigger('click');

                                    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('Step 4: Delivery Method');
                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('Step 5: Payment Method');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('Step 6: Confirm Order');
                                },
                                error: function(xhr, ajaxOptions, thrownError) {

                                }
                            });
                        }
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
        });

        // Guest Shipping
        $(document).delegate('#button-guest-shipping', 'click', function() {
            $.ajax({
                //Guest User shipping data save into Db other wise data reutrn into Json for erros   //index.php?route=checkout/guest_shipping/save
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("payment-address/save")}}',
                type: 'post',
                data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-guest-shipping').button('loading');
                },
                success: function(json) {
                    $('.alert-dismissible, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-guest-shipping').button('reset');

                        if (json['error']['warning']) {
                            // $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-danger alert-dismissible">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            var element = $('#input-shipping-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    } else {
                        $.ajax({
                            url: '{{route("shipping")}}',
                            dataType: 'html',
                            complete: function() {
                                $('#button-guest-shipping').button('reset');
                            },
                            success: function(html) {
                                $('#collapse-shipping-method .panel-body').html(html);

                                $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 3: Delivery Method <i class="fa fa-caret-down"></i>');

                                $('a[href=\'#collapse-shipping-method\']').trigger('click');

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('Step 5: Payment Method');
                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('Step 6: Confirm Order');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                            }
                        });
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
        });

        $(document).delegate('#button-shipping-method', 'click', function() {
            $.ajax({
                //shipping method save into db otherwise json will retuen error sample in this url //index.php?route=checkout/shipping_method/save
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("payment-address/save")}}',
                type: 'post',
                data: $('#collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-shipping-method').button('loading');
                },
                success: function(json) {

                    $('.alert-dismissible, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-shipping-method').button('reset');

                        if (json['error']['warning']) {
                            // $('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-danger alert-dismissible">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }
                    } else {
                        $.ajax({
                            url: '{{route("payment-method")}}',
                            dataType: 'html',
                            complete: function() {
                                $('#button-shipping-method').button('reset');
                            },
                            success: function(html) {
                                $('#collapse-payment-method .panel-body').html(html);

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 5: Payment Method <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#collapse-payment-method\']').trigger('click');

                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('Step 6: Confirm Order');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                            }
                        });
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {

                }
            });
        });

        $(document).delegate('#button-payment-method', 'click', function() {
            $.ajax({
                // after save payment into DB json will return redirect message otherwise json will return error message sample url //index.php?route=checkout/payment_method/save
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("payment-address/save")}}',
                type: 'post',
                data: $('#collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-payment-method').button('loading');
                },
                success: function(json) {
                    $('.alert-dismissible, .text-danger').remove();

                    if (json){
                        $.ajax({
                            url: '{{route('order-confirm')}}',
                            dataType: 'html',
                            complete: function() {
                                $('#button-payment-method').button('reset');
                            },
                            success: function(html) {
                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-confirm" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 6: Confirm Order <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#collapse-checkout-confirm\']').trigger('click');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                            }
                        });
                    }
                },
            });
        });
        //--></script>
    <script>

        function quickbox(){
            if ($(window).width() > 767) {
                $('.quickview').magnificPopup({
                    type:'iframe',
                    delegate: 'a',
                    preloader: true,
                    tLoading: 'Loading image #%curr%...',
                });
            }
        }
        jQuery(document).ready(function() {quickbox();});
        jQuery(window).resize(function() {quickbox();});

    </script>
    <script type="text/javascript"><!--
        $('input[name=\'payment_address\']').on('change', function() {
            if (this.value == 'new') {
                $('#payment-existing').hide();
                $('#payment-new').show();
            } else {
                $('#payment-existing').show();
                $('#payment-new').hide();
            }
        });
        //--></script>
    <script type="text/javascript"><!--
        // Sort the custom fields
        $('#collapse-payment-address .form-group[data-sort]').detach().each(function() {
            if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#collapse-payment-address .form-group').length-2) {
                $('#collapse-payment-address .form-group').eq(parseInt($(this).attr('data-sort'))+2).before(this);
            }

            if ($(this).attr('data-sort') > $('#collapse-payment-address .form-group').length-2) {
                $('#collapse-payment-address .form-group:last').after(this);
            }

            if ($(this).attr('data-sort') == $('#collapse-payment-address .form-group').length-2) {
                $('#collapse-payment-address .form-group:last').after(this);
            }

            if ($(this).attr('data-sort') < -$('#collapse-payment-address .form-group').length-2) {
                $('#collapse-payment-address .form-group:first').before(this);
            }
        });
        //--></script>
    <script type="text/javascript"><!--
        $('#collapse-payment-address button[id^=\'button-payment-custom-field\']').on('click', function() {
            var element = this;

            $('#form-upload').remove();

            $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

            $('#form-upload input[name=\'file\']').trigger('click');

            if (typeof timer != 'undefined') {
                clearInterval(timer);
            }

            timer = setInterval(function() {
                if ($('#form-upload input[name=\'file\']').val() != '') {
                    clearInterval(timer);

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: 'index.php?route=tool/upload',
                        type: 'post',
                        dataType: 'json',
                        data: new FormData($('#form-upload')[0]),
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function() {
                            $(element).button('loading');
                        },
                        complete: function() {
                            $(element).button('reset');
                        },
                        success: function(json) {
                            $(element).parent().find('.text-danger').remove();

                            if (json['error']) {
                                $(element).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
                            }

                            if (json['success']) {
                                alert(json['success']);

                                $(element).parent().find('input[name^=\'custom_field\']').val(json['code']);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            }, 500);
        });
        //--></script>








@endsection
