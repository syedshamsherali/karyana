@extends('layouts.master')
@section('title', 'Product Compare')
{{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
@section('body')

    <!-- ======= Quick view JS ========= -->
    <script>

        function quickbox(){
            if ($(window).width() > 767) {
                $('.quickview').magnificPopup({
                    type:'iframe',
                    delegate: 'a',
                    preloader: true,
                    tLoading: 'Loading image #%curr%...',
                });
            }
        }
        jQuery(document).ready(function() {quickbox();});
        jQuery(window).resize(function() {quickbox();});

    </script>
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Product Comparison</a></li>
    </ul>

        <h1></h1>
        <div id="tabs" class="htabs">
            @if(session()->has('message'))
                <p class="alert alert-success">
                    {{session()->get('message')}}
                </p>
            @endif
            <div class="tab-title-main">
                <ul class="etabs nav nav-tabs">
                    <li class="nav-item active">
                        <a href="#" data-toggle="tab" class="nav-link">Product Comparison</a>
                    </li>
                </ul>
            </div>
        </div>
        <hr>
    <div id="product-compare" class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="" class="col-sm- hidden-xs">
                    <aside id="column-left" class="col-sm-3 hidden-xs">
                        <div class="box">
                            <div class="box-heading">Categories</div>
                            <div class="box-content">
                                <ul class="box-category treeview-list treeview">

                                    @foreach($categories->where('parent_id',0) as $cat)
                                        <li>
                                            <a href="#" >{{$cat->name}}</a>
                                            @if($cat->childs->count()>0)
                                                <ul>
                                                    @foreach($cat->childs as $submenu)
                                                        <li>
                                                            <a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                    </aside>
                </div>
            </div>
            <div class="col-sm-9" id="content">

                <table class="table table-bordered table-responsive">
                    <thead>
                    <tr>
                        <td colspan="6"><strong>Product Details</strong></td>
                    </tr>
                    </thead>
                    <tbody>
{{--                    @dd(session()->get('key'))--}}
                    @if(session()->get('key'))
                        <tr>
                            <td><strong>Product Name</strong></td>
                            @foreach(session()->get('key') as $products)
{{--                                @dd($products)--}}
                                <td><strong>{{$products->name}}</strong></td>
                            @endforeach
                        </tr>

                        <tr>
                            <td><strong>Image</strong></td>
                            @foreach(session()->get('key') as $products)
                                @php
                                $images=$products->image;
                                $img=explode(",",$images);
                                @endphp
                                <td><img style="height: 50px ;width: 50px; " src="{{url('images/'.$img[0])}}"></td>
                            @endforeach
                        </tr>

                        <tr>
                            <td><strong>Price</strong></td>
                            @foreach(session()->get('key') as $products)
                                <td>Rs.{{$products->price}}</td>
                            @endforeach
                        </tr>

                        <tr>
                            <td><strong>Shop</strong></td>
                            @foreach(session()->get('key') as $products)
                                <td>{{$products->shop['name']}}</td>
                            @endforeach
                        </tr>

                        <tr>
                            <td><strong>Availability</strong></td>
                            @foreach(session()->get('key') as $products)
                                <td>In Stock</td>
                            @endforeach
                        </tr>

                        <tr>
                            <td><strong>Description</strong></td>
                            {{--                        @dd(session()->get('key'))--}}
                            @foreach(session()->get('key') as $products)
                                <td>{{$products->description}}</td>
                            @endforeach
                        </tr>
                    </tbody>

                    <tbody>
                    <tr>
                        <td></td>
{{--                        <span style="visibility: hidden">{{$i = 0}}</span>--}}
{{--                        <span style="visibility: hidden;"> {{$i=0}}</span>--}}
                        @foreach(session()->get('key') as $products)
                                    <td>
                                <a onclick="addToCart({{$products->id}})">
                                    <input class="btn btn-primary btn-block" onclick="cart.add('43', '1');" type="button" value="Add to Cart">
                                </a>
                                <br>
                                 <button class="btn btn-primary btn-block remove" style="margin-top: 5px" type="button" onclick="removeitem({{$products->id}})"><a>Remove</a></button>
                                    </td>
{{--                                    <span style="visibility: hidden">{{$i++}}</span>--}}
{{--                                    <span style="visibility: hidden">{{$i++}}</span>--}}
                        @endforeach
                    </tr>
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
    <input type="hidden" id="token" value="{{csrf_token()}}">
    <input type="hidden" id="data" value="">
    @include('dashboards.projectjs')
{{--    <script>--}}

{{--        --}}{{--function removeitem(id) {--}}
{{--        --}}{{--    // alert(id);--}}
{{--        --}}{{--    // console.log(id);--}}
{{--        --}}{{--    $.ajax({--}}
{{--        --}}{{--        headers: {--}}
{{--        --}}{{--            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
{{--        --}}{{--        },--}}

{{--        --}}{{--        type: "post",--}}
{{--        --}}{{--        url: '{{route("remove_Item")}}',--}}
{{--        --}}{{--        data: {'id': id},--}}
{{--        --}}{{--        success : function (data) {--}}
{{--        --}}{{--            console.log(data);--}}
{{--        --}}{{--            // location.reload();--}}
{{--        --}}{{--        }--}}
{{--        --}}{{--    });--}}
{{--        --}}{{--}--}}

{{--        function removeRow(id) {--}}
{{--            var qty =  document.getElementById(id);--}}
{{--            var price =  document.getElementById(id+'a');--}}
{{--            $.ajax({--}}
{{--                headers: {--}}
{{--                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
{{--                },--}}
{{--                type : "post",--}}
{{--                url :  '{{"remove-cart"}}',--}}
{{--                data : {'id': id , 'qty': qty.value ,'price':price.innerHTML},--}}
{{--                success : function (response) {--}}
{{--                    location.reload();--}}
{{--                }--}}
{{--            });--}}
{{--        }--}}
{{--    </script>--}}
@endsection






















{{--@extends('layouts.master')--}}
{{--@section('title', 'Product Comparison')--}}
{{--@section('body')--}}
{{--    <ul class="breadcrumb">--}}
{{--        <li><a href="#"><i class="fa fa-home"></i></a></li>--}}
{{--        <li><a href="#">Product Comparison </a></li>--}}
{{--    </ul>--}}
{{--    <hr>--}}
{{--    <div id="tabs" class="htabs">--}}
{{--        @if(session()->has('message'))--}}
{{--            <p class="alert alert-success">--}}
{{--                {{session()->get('message')}}--}}
{{--            </p>--}}
{{--        @endif--}}
{{--        <div class="tab-title-main">--}}
{{--            <ul class="etabs nav nav-tabs">--}}
{{--                <li class="nav-item active">--}}
{{--                    <a href="#" data-toggle="tab" class="nav-link">Search Product To Compare</a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div id="content" class="col-sm-9 categorypage">--}}
{{--                <h3 class="refine-search">Refine Search</h3>--}}
{{--                <div class="category_filter">--}}
{{--                    <div class="col-md-4 btn-list-grid">--}}
{{--                        <div class="btn-group">--}}
{{--                            <button type="button" id="grid-view" class="btn btn-default grid" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>--}}
{{--                            <button type="button" id="list-view" class="btn btn-default list" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        <div class="sort-by-wrapper">--}}
{{--                            <div class="col-md-2 text-right ">--}}
{{--                                <label class="control-label" for="input-sort">Find:</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-8 text-right ">--}}
{{--                                <form action="{{route('search')}}" autocomplete="off">--}}
{{--                                    <div class="inner-addon right-addon">--}}
{{--                                        --}}{{--                                        <i class="glyphicon glyphicon-search"></i>--}}
{{--                                        <input type="text" name="search" placeholder="Search Product"  class="form-control" />--}}
{{--                                    </div>--}}
{{--                                </form>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div> <div class="pagination-right">--}}
{{--                        <div class="sort-by-wrapper">--}}
{{--                            <div class="col-md-2 text-right sort-by">--}}
{{--                                <label class="control-label" for="input-sort">Sort By:</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-3 text-right sort">--}}
{{--                                <select id="input-sort" class="form-control" onchange="location = this.value;">--}}
{{--                                    <option value="" selected="selected">Default</option>--}}
{{--                                    <option value="">Name (A - Z) </option>--}}
{{--                                    <option value="">Name (Z - A) </option>--}}
{{--                                    <option value="">Price (Low &gt; High) </option>--}}
{{--                                    <option value="">Price (High &gt; Low) </option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="show-wrapper">--}}
{{--                            <div class="col-md-1 text-right show">--}}
{{--                                <label class="control-label" for="input-limit">Show:</label>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-2 text-right limit">--}}
{{--                                <select id="input-limit" class="form-control" onchange="location = this.value;">--}}
{{--                                    <option>10</option>--}}
{{--                                    <option>20</option>--}}
{{--                                    <option>30</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <aside id="column-left" class="col-sm-3 hidden-xs">--}}
{{--                    <div class="box">--}}
{{--                        <div class="box-heading">Categories</div>--}}
{{--                        <div class="box-content">--}}
{{--                            <ul class="box-category treeview-list treeview">--}}
{{--                                @foreach($categories->where('parent_id',0) as $cat)--}}
{{--                                    <li>--}}
{{--                                        <a href="#" >{{$cat->name}}</a>--}}
{{--                                        @if($cat->childs->count()>0)--}}
{{--                                            <ul>--}}
{{--                                                @foreach($cat->childs as $submenu)--}}
{{--                                                    <li>--}}
{{--                                                        <a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a>--}}
{{--                                                    </li>--}}
{{--                                                @endforeach--}}
{{--                                            </ul>--}}
{{--                                        @endif--}}
{{--                                    </li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="box">--}}
{{--                        <div class="box-heading">Bestsellers</div>--}}
{{--                        <div class="box-content">--}}
{{--                            <div class="box-product  productbox-grid" id=" bestseller-grid">--}}
{{--                                <div class="product-items col-lg-3 col-md-3 col-sm-6 col-xs-12">--}}
{{--                                    <div class="product-block product-thumb transition">--}}
{{--                                        <div class="product-block-inner">--}}
{{--                                            <div class="image">--}}
{{--                                                <a href="{{route('productDetail',[$products[0]->id])}}"><img src="{{url('/images/'.$products[0]->image)}}" alt="" title="{{$products[0]->name}}" class="img-responsive" /></a>--}}
{{--                                            </div>--}}
{{--                                            <div class="product-details">--}}
{{--                                                <div class="caption">--}}
{{--                                                    <h4><a href="{{route('productDetail',[$products[0]->id])}}">{{$products[0]->name}}</a></h4>--}}
{{--                                                    <p class="price"> <span class="rating">--}}
{{--                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>--}}
{{--                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>--}}
{{--                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>--}}
{{--                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                        </span>--}}
{{--                                                    </p>--}}
{{--                                                    <div class="button-group">--}}
{{--                                                        <button type="button" class="addtocart" onclick="cart.add('45 ');">Add to Cart </button>--}}
{{--                                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('45 ');"><i class="fa fa-heart"></i></button>--}}
{{--                                                        <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product " onclick="compare.add('45 ');"><i class="fa fa-exchange"></i></button>--}}
{{--                                                        <div class="quickview" data-toggle="tooltip" data-placement="top" title="Quickview"><a href="#">Quick View<i class="fa fa-eye" aria-hidden="true"></i>--}}
{{--                                                            </a></div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="box">--}}
{{--                        <div class="box-heading">Latest</div>--}}
{{--                        <div class="box-content">--}}
{{--                            <div class="box-product  productbox-grid" id=" latest-grid">--}}
{{--                                <div class="product-items col-lg-3 col-md-3 col-sm-6 col-xs-12">--}}
{{--                                    <div class="product-block product-thumb transition">--}}
{{--                                        <div class="product-block-inner">--}}
{{--                                            <div class="image">--}}
{{--                                            </div>--}}
{{--                                            <div class="product-details">--}}
{{--                                                <div class="caption">--}}
{{--                                                     <div class="rating">--}}
{{--                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>--}}
{{--                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>--}}
{{--                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>--}}
{{--                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>--}}
{{--                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>--}}
{{--                                                    </div>--}}
{{--                                                    </p>--}}
{{--                                                    <div class="button-group">--}}
{{--                                                        <button type="button" class="addtocart" onclick="cart.add('49 ');">Add to Cart </button>--}}
{{--                                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('49 ');"><i class="fa fa-heart"></i></button>--}}
{{--                                                        <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product " onclick="compare.add('49 ');"><i class="fa fa-exchange"></i></button>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </aside>--}}
{{--                <div class="row shop_wrapper" id="tag_container">--}}
{{--                    <div class="mx-auto">--}}
{{--                        <img src="http://127.0.0.1:8000/assets/frontend/image/find.jpg">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}

