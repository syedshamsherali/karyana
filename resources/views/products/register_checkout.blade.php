<div class="row">
{{--    <div id="content" class="col-sm-9">--}}

{{--        <h1>Register</h1>--}}
{{--        <form method="POST" action="{{ route('register') }}" class="needs-validation" novalidate>--}}
{{--            @csrf--}}
{{--            <div class="">--}}
{{--                <div class="row form-group">--}}
{{--                    <div class="col-sm-3"></div>--}}
{{--                    <div class="col-sm-6">--}}
{{--                        <input id="fullname" type="text" class="form-control   @error('fullname') is-invalid @enderror" name="fullname" value="{{ old('fullname') }}" autocomplete="fullname" autofocus placeholder="Full Name" required>--}}
{{--                        @error('fullname')--}}
{{--                        <span class="invalid-feedback" role="alert">--}}
{{--                            <strong>{{ $message }}</strong>--}}
{{--                        </span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="form-group row">--}}
{{--                    <div class="col-sm-3"></div>--}}
{{--                    <div class="col-sm-6">--}}
{{--                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" placeholder="E-Mail" required>--}}
{{--                        @error('email')--}}
{{--                        <span class="invalid-feedback" role="alert">--}}
{{--                            <strong>{{ $message }}</strong>--}}
{{--                        </span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="form-group row">--}}
{{--                    <div class="col-sm-3"></div>--}}
{{--                    <div class="col-sm-6">--}}
{{--                        <input id="user_contact" type="tel" class="form-control @error('user_contact') is-invalid @enderror" name="user_contact" value="{{ old('user_contact') }}"  autocomplete="user_contact" placeholder="Phone" required>--}}
{{--                        @error('user_contact')--}}
{{--                        <span class="invalid-feedback" role="alert">--}}
{{--                            <strong>{{ $message }}</strong>--}}
{{--                        </span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="form-group row">--}}
{{--                    <div class="col-sm-3"></div>--}}
{{--                    <div class="col-sm-6">--}}
{{--                        <input id="city" type="text" class="form-control" name="city" placeholder="City">--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="form-group row">--}}
{{--                    <div class="col-sm-3"></div>--}}
{{--                    <div class="col-sm-6">--}}
{{--                        <textarea id="address" type="" class="form-control" name="address" placeholder="address"></textarea>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="form-group row">--}}
{{--                    <div class="col-sm-3"></div>--}}
{{--                    <div class="col-sm-6">--}}
{{--                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password" placeholder="Password" required>--}}
{{--                        @error('password')--}}
{{--                        <span class="invalid-feedback" role="alert">--}}
{{--                            <strong>{{ $message }}</strong>--}}
{{--                        </span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="form-group row">--}}
{{--                    <div class="col-sm-3"></div>--}}
{{--                    <div class="col-sm-6">--}}
{{--                        <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" autocomplete="new-password" placeholder="Confirm Password" required>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="form-group row mb-0">--}}
{{--                    <div class="offset-md-4">--}}
{{--                        <button type="submit" class="btn btn-primary" onclick="abc()" id="#button-register" >--}}
{{--                            {{ __('Register') }}--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                Have an account?--}}
{{--                <div class="nav-item">--}}
{{--                    <a class="nav-link" href="{{ route('login') }}">{{ __('Sign in') }}</a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </form>--}}
{{--    </div>--}}
    <div id="content" class="col-sm-9">
{{--        <form method="POST" action="{{ route('register') }}" class="needs-validation" novalidate>--}}
            @csrf
            <fieldset id="account">
                <legend>Your Personal Details</legend>
                <div class="form-group required" style="display:  none ;">
                    <label class="col-sm-2 control-label">Customer Group</label>
                    <div class="col-sm-10">
                        <div class="radio">
                            <label>
                                <input type="radio" name="customer_group_id" value="1" checked="checked">
                                Default</label>
                        </div>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-firstname">Full Name:</label>
                    <div class="col-sm-10">
                        <input id="fullname" type="text" class="form-control   @error('fullname') is-invalid @enderror" name="fullname" value="{{ old('fullname') }}" autocomplete="fullname" autofocus placeholder="Full Name" required>
                        @error('fullname')
                        <span class="invalid-feedback" role="">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group required" >
                    <label class="col-sm-2 control-label" for="input-lastname " style="margin-top: 12px;" >Email:</label>
                    <div class="col-sm-10">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" placeholder="E-Mail" required style="margin-top: 12px;">
                        @error('email')
                        <span class="invalid-feedback" role="">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-email" style="margin-top: 12px;">Phone:</label>
                    <div class="col-sm-10">
                        <input id="user_contact" type="tel" class="form-control @error('user_contact') is-invalid @enderror" name="user_contact" value="{{ old('user_contact') }}"  autocomplete="user_contact" placeholder="Phone" required style="margin-top: 12px;">
                        @error('user_contact')
                        <span class="invalid-feedback" role="">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="" style="margin-top: 12px;">City</label>
                    <div class="col-sm-10">
                        <input id="city" type="text" class="form-control" name="city" placeholder="City" style="margin-top: 12px;">
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-password" style="margin-top: 12px;">Address</label>
                    <div class="col-sm-10">
                        <textarea id="address" type="" class="form-control" name="address" placeholder="address" style="margin-top: 12px;"></textarea>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Your Password</legend>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-confirm" style="margin-top: 12px;">Password</label>
                    <div class="col-sm-10">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password" placeholder="Password" required style="margin-top: 12px;">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-confirm" style="margin-top: 12px;">Password</label>
                    <div class="col-sm-10">
                        <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" autocomplete="new-password" placeholder="Confirm Password" required style="margin-top: 12px;">

                    </div>
                </div>
            </fieldset>


            <div class="buttons">
{{--                <div class="pull-right">I have read and agree to the <a href="" class="agree"><b class="text-danger">Privacy Policy</b></a>--}}
{{--                    <input type="checkbox" name="agree" value="1">--}}
                    &nbsp;
                    <button type="submit" class="btn btn-primary" onclick="abc()" id="#button-register" >
                        {{ __('Register') }}
                    </button>
                </div>

{{--        </form>--}}
    </div>
</div>
<script type="text/javascript"><!--
    // Sort the custom fields
    $('#account .form-group[data-sort]').detach().each(function() {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
            $('#account .form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('#account .form-group').length) {
            $('#account .form-group:last').after(this);
        }

        if ($(this).attr('data-sort') == $('#account .form-group').length) {
            $('#account .form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('#account .form-group').length) {
            $('#account .form-group:first').before(this);
        }
    });

    $('input[name=\'customer_group_id\']').on('change', function() {
        $.ajax({
            url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
            dataType: 'json',
            success: function(json) {
                $('.custom-field').hide();
                $('.custom-field').removeClass('required');

                for (i = 0; i < json.length; i++) {
                    custom_field = json[i];

                    $('#custom-field' + custom_field['custom_field_id']).show();

                    if (custom_field['required']) {
                        $('#custom-field' + custom_field['custom_field_id']).addClass('required');
                    }
                }
            },
            // error: function(xhr, ajaxOptions, thrownError) {
            //     alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            // }
        });
    });

    $('input[name=\'customer_group_id\']:checked').trigger('change');
    //--></script>
<script type="text/javascript"><!--
    $('button[id^=\'button-custom-field\']').on('click', function() {
        var element = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(element).button('loading');
                    },
                    complete: function() {
                        $(element).button('reset');
                    },
                    success: function(json) {
                        $(element).parent().find('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(element).parent().find('input').val(json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    {{--// Register--}}
    {{--function abc(){--}}
    {{--    // alert('adf');--}}
    {{--    var fullname = document.getElementById('fullname');--}}
    {{--    var email = document.getElementById('email');--}}
    {{--    var user_contact = document.getElementById('user_contact');--}}
    {{--    var city = document.getElementById('city');--}}
    {{--    var password = document.getElementById('password');--}}
    {{--    var password_confirmation = document.getElementById('password_confirmation');--}}
    {{--    var address = document.getElementById('address');--}}
    {{--    $.ajax({--}}
    {{--        headers: {--}}
    {{--            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
    {{--        },--}}
    {{--        url: '{{route("register")}}',--}}
    {{--        type: 'post',--}}
    {{--        data: {'fullname':fullname.value, 'email':email.value , 'user_contact':user_contact.value, 'city':city.value , 'password':password.value ,'password_confirmation':password_confirmation.value, 'address':address.value},--}}
    {{--        // dataType: 'json',--}}
    {{--        beforeSend: function() {--}}
    {{--            // $('#button-register').button('loading');--}}
    {{--        },--}}
    {{--        success: function(json) {--}}
    {{--            location.reload();--}}
    {{--            alert('save');--}}
    {{--            $('.alert-dismissible, .text-danger').remove();--}}
    {{--            $('.form-group').removeClass('has-error');--}}
    {{--            if (json['error']) {--}}
    {{--                $('#button-register').button('reset');--}}
    {{--    //--}}
    {{--                if (json['error']['warning']) {--}}
    {{--                    $('#collapse-payment-address .panel-body').prepend('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');--}}
    {{--                }--}}
    {{--    //--}}
    {{--                for (i in json['error']) {--}}
    {{--                    var element = $('#input-payment-' + i.replace('_', '-'));--}}
    {{--    //--}}
    {{--                    if ($(element).parent().hasClass('input-group')) {--}}
    {{--                        $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');--}}
    {{--                    } else {--}}
    {{--                        $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');--}}
    {{--                    }--}}
    {{--                }--}}
    {{--    //--}}
    {{--                // Highlight any found errors--}}
    {{--                $('.text-danger').parent().addClass('has-error');--}}
    {{--            } else {--}}
    {{--                var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').prop('value');--}}
    {{--    //--}}
    {{--                if (shipping_address) {--}}
    {{--                    $.ajax({--}}
    {{--                        url: '{{route("shipping")}}',--}}
    {{--                        dataType: 'html',--}}
    {{--                        success: function(html) {--}}
    {{--                            // Add the shipping address--}}
    {{--                            $.ajax({--}}
    {{--                                url: '{{route("shipping-address")}}',--}}
    {{--                                dataType: 'html',--}}
    {{--                                success: function(html) {--}}
    {{--                                    alert("alert line 2119")--}}
    {{--                                    $('#collapse-shipping-address .panel-body').html(html);--}}
    {{--    //--}}
    {{--                                    $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 3z: Delivery Details <i class="fa fa-caret-down"></i></a>');--}}
    {{--                                    $('#collapse-shipping-address').show();--}}
    {{--                                },--}}
    {{--                                error: function(xhr, ajaxOptions, thrownError) {--}}
    {{--                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);--}}
    {{--                                }--}}
    {{--                            });--}}
    {{--    //--}}
    {{--                            $('#collapse-shipping-method .panel-body').html(html);--}}
    {{--    //--}}
    {{--                            $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 4: Delivery Method <i class="fa fa-caret-down"></i></a>');--}}
    {{--    //--}}
    {{--                            $('a[href=\'#collapse-shipping-method\']').trigger('click');;--}}
    {{--    //--}}
    {{--                            $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('Step 4: Delivery Method');--}}
    {{--                            $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('Step 5: Payment Method');--}}
    {{--                            $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('Step 6: Confirm Order');--}}
    {{--                        },--}}
    {{--                        error: function(xhr, ajaxOptions, thrownError) {--}}
    {{--                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);--}}
    {{--                        }--}}
    {{--                    });--}}
    {{--                } else {--}}
    {{--                    $.ajax({--}}
    {{--                        url: '{{route("shipping-address")}}',--}}
    {{--                        dataType: 'html',--}}
    {{--                        success: function(html) {--}}
    {{--                            // alert('line 2147')--}}
    {{--                            $('#collapse-shipping-address .panel-body').html(html);--}}
    {{--    //--}}
    {{--                            $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 3: Delivery Details <i class="fa fa-caret-down"></i></a>');--}}
    {{--    //--}}
    {{--                            $('a[href="#collapse-shipping-address"]').trigger('click');--}}
    {{--    //--}}
    {{--                            $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('Step 4: Delivery Method');--}}
    {{--                            $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('Step 5: Payment Method');--}}
    {{--                            $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('Step 6: Confirm Order');--}}
    {{--                        },--}}
    {{--                        error: function(xhr, ajaxOptions, thrownError) {--}}
    {{--                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);--}}
    {{--                        }--}}
    {{--                    });--}}
    {{--                }--}}
    {{--    //--}}
    {{--                $.ajax({--}}
    {{--                    url: '{{route("payment-address")}}',--}}
    {{--                    dataType: 'html',--}}
    {{--                    complete: function() {--}}
    {{--                        $('#button-register').button('reset');--}}
    {{--                    },--}}
    {{--                    success: function(html) {--}}
    {{--                        $('#collapse-payment-address .panel-body').html(html);--}}
    {{--    //--}}
    {{--                        $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Step 2: Billing Details <i class="fa fa-caret-down"></i></a>');--}}
    {{--                    },--}}
    {{--                    error: function(xhr, ajaxOptions, thrownError) {--}}
    {{--                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);--}}
    {{--                    }--}}
    {{--                });--}}
    {{--            }--}}
    {{--        },--}}
    {{--        error: function(xhr, ajaxOptions, thrownError) {--}}
    {{--            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);--}}
    {{--        }--}}
    {{--    });--}}
    {{--}--}}
    //-->
</script>