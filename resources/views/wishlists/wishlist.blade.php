@extends('layouts.master')
@section('title', 'Wishlist')



@section('body')

    <!-- ======= Quick view JS ========= -->
    <script>

        function quickbox(){
            if ($(window).width() > 767) {
                $('.quickview').magnificPopup({
                    type:'iframe',
                    delegate: 'a',
                    preloader: true,
                    tLoading: 'Loading image #%curr%...',
                });
            }
        }
        jQuery(document).ready(function() {quickbox();});
        jQuery(window).resize(function() {quickbox();});

    </script>

    <div id="product-compare" class="container">
        <ul class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i></a></li>
            <li><a href="#">Wishlist</a></li>

        </ul>
        <div class="row">
            <div class="col-sm-12">
                <div id="" class="col-sm- hidden-xs">
                    <aside id="column-left" class="col-sm-3 hidden-xs">
                        <div class="box">
                            <div class="box-heading">Categories</div>
                            <div class="box-content">
                                <ul class="box-category treeview-list treeview">
                                    @foreach($categories->where('parent_id',0) as $cat)
                                        <li>
                                            <a href="#" >{{$cat->name}}</a>
                                            @if($cat->childs->count()>0)
                                                <ul>
                                                    @foreach($cat->childs as $submenu)
                                                        <li>
                                                            <a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a>
                                                        </li>
                                                    @endforeach

                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>


            <div id="content" class="col-sm-9">
                <h2>Wish List</h2>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-center">Image</td>
                            <td class="text-left">Product Name</td>
                            <td class="text-left">description</td>
                            <td class="text-right">Shop</td>
                            <td class="text-right">price</td>
                            <td class="text-right">Action</td>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($allwhishs as $allwhish )
                                @php
                                   $pid=$allwhish->product_id;
                                    $productid=\App\Models\Product::find($pid);
                                    $shop=\App\Models\Shop::find($productid->shop_id);

                                @endphp
                                <tr>

{{--                            <td class="text-center"><a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=36"><img src="{{asset('asset/images/')}}" .$productid->image)}}" alt="Aenean quis" title="Aenean quis"></a></td>--}}
                            <td class="text-center"><a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=36"><img style="  width:40px;height:40px;  " src="{{url('/images/'.$productid->image)}}" /></a></td>
                            <td class="text-left">{{$productid->name}}</td>
                            <td class="text-left">{{$productid->description}}</td>
                            <td class="text-right">{{$shop->name}}</td>
                            <td class="text-right"><div class="price">  <b>Rs.{{$productid->price}}</b></div>
                            </td>

                            <td class="text-right"><a onclick="addToCart({{$allwhish->id}})"><button data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add to Cart"><i class="fa fa-shopping-cart"></i></button></a>
                                <a href="{{route('wish_delete',[$allwhish->id])}}" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Remove"><i class="fa fa-times"></i></a></td>
                        </tr>
                              @endforeach
                        </tbody>

                    </table>
                </div>
                <div class="buttons clearfix">
                    <div class="pull-right"><a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=account/account" class="btn btn-primary">Continue</a></div>
                </div>
            </div>


        </div>
    </div>
    @include('dashboards.projectjs')
@endsection
