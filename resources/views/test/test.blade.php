@extends('layouts.master')
@section('title', 'CategoryProduct Detail')
@section('body')
    <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i></a></li>
        <li><a href="#">Product By Cotegory </a></li>
    </ul>
    <hr>
    <div class=" ">
        <!-- <div class="box-heading"><span>Our Products</span></div> -->
        @if(session()->has('message'))
            <p class="alert alert-success">
                {{session()->get('message')}}
            </p>
        @endif
        <div id="tabs" class="htabs">
            <div class="tab-title-main">
                <ul class='etabs nav nav-tabs'>
                    <li class='nav-item active'>
                        <a href="#" data-toggle="tab" class="nav-link">{{$catg->name}}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            <aside id="column-left" class="col-sm-3 hidden-xs">
                    <div class="box">
                        <div class="box-heading">Categories</div>
                        <div class="box-content">
                            <ul class="box-category treeview-list treeview">
                                @foreach($categories->where('parent_id',0) as $cate)
                                    <li>
                                        <a href="#" >{{$cate->name}}</a>
                                        @if($cate->childs->count()>0)
                                            <ul>
                                                @foreach($cate->childs as $submenu)
                                                    <li>
                                                        <a href="{{route('test',[$submenu->id])}}">{{$submenu->name}}</a>
                                                    </li>
                                                @endforeach

                                            </ul>
                                        @endif
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-heading">Bestsellers</div>
                        <div class="box-content">

                            <div class="box-product  productbox-grid" id=" bestseller-grid">
                                <div class="product-items col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="product-block product-thumb transition">
                                        <div class="product-block-inner">

                                            <div class="image">
                                                <a href="{{route('productDetail',[$cat[0]->id])}}"><img src="{{url('/images/'.$cat[0]->image)}}" alt="" title="{{$cat[0]->name}}" class="img-responsive" /></a>


                                            </div>
                                            <div class="product-details">
                                                <div class="caption">
                                                    <h4><a href="{{route('productDetail',[$cat[0]->id])}}">{{$cat[0]->name}}</a></h4>
                                                    <p class="price">
                                                        Rs.{{$cat[0]->price}}
                                                        <span class="price-tax">{{$cat[0]->price}}</span>
                                                        <span class="rating">
{{--                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>--}}
{{--                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>--}}
{{--                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>--}}
{{--                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
                                                        </span>
                                                    </p>
                                                    <div class="button-group">
                                                        <button type="button" class="addtocart" onclick="cart.add('45 ');">Add to Cart </button>
                                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('45 ');"><i class="fa fa-heart"></i></button>
                                                        <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product " onclick="compare.add('45 ');"><i class="fa fa-exchange"></i></button>
                                                        <div class="quickview" data-toggle="tooltip" data-placement="top" title="Quickview"><a href="#">Quick View<i class="fa fa-eye" aria-hidden="true"></i>
                                                            </a></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-heading">Latest</div>
                        <div class="box-content">

                            <div class="box-product  productbox-grid" id=" latest-grid">
                                <div class="product-items col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="product-block product-thumb transition">
                                        <div class="product-block-inner">
                                            <div class="image">
                                                @php
                                                $images=$latestProduct->image;
                                                $img=explode(",",$images);
                                                @endphp
                                                <a href="{{route('productDetail',[$latestProduct->id])}}"><img src="{{url('/images/'.$img[0])}}" alt="" title="" class="img-responsive" /></a>
                                            </div>
                                            <div class="product-details">
                                                <div class="caption">
                                                    <h4><a href="{{route('productDetail',[$latestProduct->id])}}">{{$latestProduct->name}} </a></h4>
                                                    <div class="rating">
{{--                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>--}}
{{--                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>--}}
{{--                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>--}}
{{--                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>--}}
{{--                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>--}}
                                                    </div>
                                                    <p class="price">
                                                        Rs.{{$latestProduct->price}}
                                                        <span class="price-tax">{{$latestProduct->price}}</span>
                                                    </p>
                                                    <div class="button-group">
                                                        <button type="button" class="addtocart" onclick="cart.add('49 ');">Add to Cart </button>
                                                        <button class="wishlist" type="button" data-toggle="tooltip" title="Add to Wish List " onclick="wishlist.add('49 ');"><i class="fa fa-heart"></i></button>
                                                        <button class="compare" type="button" data-toggle="tooltip" title="Compare this Product " onclick="compare.add('49 ');"><i class="fa fa-exchange"></i></button>
                                                        <div class="quickview" data-toggle="tooltip" data-placement="top" title="Quickview"><a href="{{url('/images/'.$latestProduct->image)}}">Quick View<i class="fa fa-eye" aria-hidden="true"></i>
                                                            </a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </aside>

            <div id="content" class="col-sm-9 categorypage">
                <h3 class="refine-search">Refine Search</h3>
                <div class="category_filter">
                    <div class="col-md-4 btn-list-grid">
                        <div class="btn-group">
                            <button type="button" id="grid-view" class="btn btn-default grid" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                            <button type="button" id="list-view" class="btn btn-default list" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>

                        </div>
                    </div>
                    <div class="col-md-3 text-right sort">
                        <form action="{{route('find')}}" autocomplete="off">
                            <div class="inner-addon right-addon">
                                {{--                                        <i class="glyphicon glyphicon-search"></i>--}}
                                <input type="text" name="search" placeholder="Search Product"  class="form-control" />
                            </div>
                        </form>
                    </div>
                     <div class="pagination-right">
                        <div class="sort-by-wrapper">
                            <div class="col-md-2 text-right sort-by">
                                <label class="control-label" for="input-sort">Sort By:</label>
                            </div>
                            <div class="col-md-3 text-right sort">
                                <select id="input-sort" class="form-control" onchange="location = this.value;">

                                    <option value="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/category&amp;path=18&amp;sort=p.sort_order&amp;order=ASC " selected="selected">Default</option>

{{--                                    <option value="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/category&amp;path=18&amp;sort=pd.name&amp;order=ASC ">Name (A - Z) </option>--}}

{{--                                    <option value="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/category&amp;path=18&amp;sort=pd.name&amp;order=DESC ">Name (Z - A) </option>--}}

{{--                                    <option value="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/category&amp;path=18&amp;sort=p.price&amp;order=ASC ">Price (Low &gt; High) </option>--}}

{{--                                    <option value="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/category&amp;path=18&amp;sort=p.price&amp;order=DESC ">Price (High &gt; Low) </option>--}}

{{--                                    <option value="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/category&amp;path=18&amp;sort=rating&amp;order=DESC ">Rating (Highest) </option>--}}

{{--                                    <option value="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/category&amp;path=18&amp;sort=rating&amp;order=ASC ">Rating (Lowest) </option>--}}

{{--                                    <option value="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/category&amp;path=18&amp;sort=p.model&amp;order=ASC ">Model (A - Z) </option>--}}

{{--                                    <option value="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/category&amp;path=18&amp;sort=p.model&amp;order=DESC ">Model (Z - A) </option>--}}
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    @foreach($cat as $product)
                        @php
                        $images=$product->image;
                        $img=explode(",",$images);
                        @endphp
{{--                    @dd($img)--}}
                        <div class="product-layout product-list col-xs-12">
                            <div class="product-block product-thumb">
                                <div class="product-block-inner">
                                    <div class="image">
                                        <a href="{{route('productDetail',[$product->id])}}">
                                            <img src="{{url('/images/'.$img[0])}}" title="{{$product->name}}" alt="{{$product->name}}" class="img-responsive reg-image" />
                                            <img class="img-responsive hover-image" src="{{url('/images/'.$img[0])}}" title="{{$product->name}}" alt="{{$product->name}}" />
                                        </a>
                                    </div>
                                    <div class="product-details">
                                        <div class="btn-list-grid"></div>
                                        <div class="caption">
                                            <span class="rating">
{{--                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>--}}
{{--                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>--}}
{{--                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>--}}
{{--                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
{{--                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>--}}
                                            </span>
                                            <h4><a href="{{$product->id}}">{{$product->name}}</a></h4>
                                            <p class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ..</p>
                                            <p class="price">
                                                Rs.{{$product->price}}
                                            </p>
                                            <div class="btn-wish-compare">
                                               <div class="button-group">
                                                    <button type="button" data-toggle="tooltip" data-placement="top" title="Add to cart" class="addtocart" onclick="cart.add('45 ');">Add to Cart
                                                        <!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                                    </button>
                                                    <div class="quickview" data-toggle="tooltip" data-placement="top" title="Quickview"><a href="{{url('/images/'.$product->image)}}">Quick View
                                                            <!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                                    <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('45 ');"><i class="fa fa-heart"></i></button>
                                                    <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('45 ');"><i class="fa fa-exchange"></i></button>
                                                </div>
                                            </div>
                                            <div class="button-group">
                                                <a onclick="addToCart({{$product->id}})">
                                                <button type="button" data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart listaddtocart" onclick="cart.add('45 ');">Add to Cart
                                                    <!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                                </button>
                                                </a>
                                               <button class="wishlist" type="button" value="{{$product->id}}" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('45 ');"><i class="fa fa-heart"></i></button>
                                                <button class="compare" type="button" value="{{$product->id}}" data-toggle="tooltip" data-placement="top" title="Compare this Product "><i class="fa fa-exchange"></i></button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="text-center ">
            {{$cat->links() }}
        </div>
    </div>
    <input type="hidden" id="token" value="{{csrf_token()}}">
    <input type="hidden" id="data" value="">
    @include('dashboards.projectjs')
    <script>
        var token=document.getElementById('token').value;
        $(document).ready(function(){
            $(".compare").click(function(){
                var id=$(this).val();
                $.ajax({
                    url: '/addToCompare/'+id,
                    type : 'get',
                    dataType:'json',
                    data:{'id':id,'token':token},
                    success: function(data){
                        $('#data').html(data);
                        console.log(data);
                    }});
            });
        });
    </script>
{{--    <script>--}}
        {{--function addToCart(id)--}}
        {{--{--}}
        {{--    // alert(id);--}}
        {{--    $.ajax({--}}
        {{--        type : 'get',--}}
        {{--        headers: {--}}
        {{--            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--        },--}}
        {{--        url : "{{route('addToCart')}}",--}}
        {{--        data : {'id':id},--}}
        {{--        success : function (response) {--}}
        {{--            if(response){--}}
        {{--                var a = document.getElementById('cart-total').innerHTML;--}}
        {{--                var b = 1;--}}
        {{--                var value = +a + +b;--}}
        {{--                document.getElementById('cart-total').innerHTML = value;--}}
        {{--            }--}}
        {{--        }--}}
        {{--    });--}}
        {{--}--}}

{{--        var token=document.getElementById('token').value;--}}
    {{--    $(document).ready(function(){--}}
    {{--        $(".compare").click(function(){--}}
    {{--            var id=$(this).val();--}}
    {{--            $.ajax({--}}
    {{--                url: '/addToCompare/'+id,--}}
    {{--                type : 'get',--}}
    {{--                // dataType:'json',--}}
    {{--                data:{'id':id},--}}
    {{--                success: function(data){--}}
    {{--                    // if(data){--}}
    {{--                    // $('#data').html(data);--}}
    {{--                    // console.log('hello');--}}
    {{--                    // }--}}
    {{--                    $.notify({--}}
    {{--                        message: "Successful Added "+data+" Into Product Compare",--}}
    {{--                        target: '_blank'--}}
    {{--                    },{--}}
    {{--                        // settings--}}
    {{--                        element: 'body',--}}
    {{--                        position: null,--}}
    {{--                        type: "info",--}}
    {{--                        allow_dismiss: true,--}}
    {{--                        newest_on_top: false,--}}
    {{--                        placement: {--}}
    {{--                            from: "top",--}}
    {{--                            align: "center"--}}
    {{--                        },--}}
    {{--                        offset: 0,--}}
    {{--                        spacing: 10,--}}
    {{--                        z_index: 2031,--}}
    {{--                        delay: 5000,--}}
    {{--                        timer: 1000,--}}
    {{--                        url_target: '_blank',--}}
    {{--                        mouse_over: null,--}}
    {{--                        animate: {--}}
    {{--                            enter: 'animated fadeInDown'--}}
    {{--                            //exit: 'animated fadeOutUp'--}}
    {{--                        },--}}
    {{--                        onShow: null,--}}
    {{--                        onShown: null,--}}
    {{--                        onClose: null,--}}
    {{--                        onClosed: null,--}}
    {{--                        icon_type: 'class',--}}
    {{--                        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +--}}
    {{--                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">&nbsp;&times;</button>' +--}}
    {{--                            '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +--}}
    {{--                            '<div class="progress" data-notify="progressbar">' +--}}
    {{--                            '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +--}}
    {{--                            '</div>' +--}}
    {{--                            '<a href="{3}" target="{4}" data-notify="url"></a>' +--}}
    {{--                            '</div>'--}}
    {{--                    });--}}
    {{--                    $('#compare-total').html(data['total']);--}}
    {{--                    // alert(total);--}}
    {{--                }--}}
    {{--            });--}}
    {{--        });--}}
    {{--    });--}}
    {{--</script>--}}

@endsection
