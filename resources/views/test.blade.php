{{--<!doctype html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
{{--    <meta charset="UTF-8">--}}
{{--    <meta name="viewport"--}}
{{--          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="ie=edge">--}}
{{--    <title>test</title>--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />--}}
{{--    <link rel="stylesheet" href="{{asset('assets\frontend\payment\stripe.css')}}" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--}}
{{--    <style>--}}
{{--        /**--}}
{{-- * The CSS shown here will not be introduced in the Quickstart guide, but shows--}}
{{-- * how you can use CSS to style your Element's container.--}}
{{-- */--}}
{{--        .StripeElement {--}}
{{--            box-sizing: border-box;--}}

{{--            height: 40px;--}}

{{--            padding: 10px 12px;--}}

{{--            border: 1px solid transparent;--}}
{{--            border-radius: 4px;--}}
{{--            background-color: white;--}}

{{--            box-shadow: 0 1px 3px 0 #e6ebf1;--}}
{{--            -webkit-transition: box-shadow 150ms ease;--}}
{{--            transition: box-shadow 150ms ease;--}}
{{--        }--}}

{{--        .StripeElement--focus {--}}
{{--            box-shadow: 0 1px 3px 0 #cfd7df;--}}
{{--        }--}}

{{--        .StripeElement--invalid {--}}
{{--            border-color: #fa755a;--}}
{{--        }--}}

{{--        .StripeElement--webkit-autofill {--}}
{{--            background-color: #fefde5 !important;--}}
{{--        }--}}
{{--    </style>--}}
{{--</head>--}}
{{--<body>--}}
{{--<form action="{{route('payment')}}" method="post" id="payment-form">--}}
{{--    <div class="form-row justify-content-center ">--}}
{{--        <script src="https://js.stripe.com/v3/"></script>--}}
{{--        @csrf--}}
{{--        <label for="card-element">--}}
{{--            Credit or debit card--}}
{{--        </label>--}}
{{--        <div id="card-element" style="width:50%;">--}}
{{--            <!-- A Stripe Element will be inserted here. -->--}}

{{--        </div>--}}

{{--        <!-- Used to display form errors. -->--}}
{{--        <div id="card-errors" role="alert"></div>--}}
{{--        <button>Submit Payment</button>--}}
{{--    </div>--}}


{{--</form>--}}


{{--<script>--}}
{{--    var stripe = Stripe('pk_test_iOO6NQVwi7P4rgeTGXN7sh5V00HeKTTQey');--}}

{{--    // Create an instance of Elements.--}}
{{--    var elements = stripe.elements();--}}

{{--    // Custom styling can be passed to options when creating an Element.--}}
{{--    // (Note that this demo uses a wider set of styles than the guide below.)--}}
{{--    var style = {--}}
{{--        base: {--}}
{{--            color: '#32325d',--}}
{{--            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',--}}
{{--            fontSmoothing: 'antialiased',--}}
{{--            fontSize: '16px',--}}
{{--            '::placeholder': {--}}
{{--                color: '#aab7c4'--}}
{{--            }--}}
{{--        },--}}
{{--        invalid: {--}}
{{--            color: '#fa755a',--}}
{{--            iconColor: '#fa755a'--}}
{{--        }--}}
{{--    };--}}

{{--    // Create an instance of the card Element.--}}
{{--    var card = elements.create('card', {style: style});--}}

{{--    // Add an instance of the card Element into the `card-element` <div>.--}}
{{--    card.mount('#card-element');--}}

{{--    // Handle real-time validation errors from the card Element.--}}
{{--    card.addEventListener('change', function(event) {--}}
{{--        var displayError = document.getElementById('card-errors');--}}
{{--        if (event.error) {--}}
{{--            displayError.textContent = event.error.message;--}}
{{--        } else {--}}
{{--            displayError.textContent = '';--}}
{{--        }--}}
{{--    });--}}

{{--    // Handle form submission.--}}
{{--    var form = document.getElementById('payment-form');--}}
{{--    form.addEventListener('submit', function(event) {--}}
{{--        event.preventDefault();--}}

{{--        stripe.createToken(card).then(function(result) {--}}
{{--            if (result.error) {--}}
{{--                // Inform the user if there was an error.--}}
{{--                var errorElement = document.getElementById('card-errors');--}}
{{--                errorElement.textContent = result.error.message;--}}
{{--            } else {--}}
{{--                // Send the token to your server.--}}
{{--                stripeTokenHandler(result.token);--}}
{{--            }--}}
{{--        });--}}
{{--    });--}}

{{--    // Submit the form with the token ID.--}}
{{--    function stripeTokenHandler(token) {--}}
{{--        // Insert the token ID into the form so it gets submitted to the server--}}
{{--        var form = document.getElementById('payment-form');--}}
{{--        var hiddenInput = document.createElement('input');--}}
{{--        hiddenInput.setAttribute('type', 'hidden');--}}
{{--        hiddenInput.setAttribute('name', 'stripeToken');--}}
{{--        hiddenInput.setAttribute('value', token.id);--}}
{{--        form.appendChild(hiddenInput);--}}

{{--        // Submit the form--}}
{{--        form.submit();--}}
{{--    }--}}

{{--</script>--}}







{{--<script>--}}
{{--    var stripe = Stripe('pk_test_iOO6NQVwi7P4rgeTGXN7sh5V00HeKTTQey');--}}

{{--    // Create an instance of Elements.--}}
{{--    var elements = stripe.elements();--}}

{{--    // Custom styling can be passed to options when creating an Element.--}}
{{--    // (Note that this demo uses a wider set of styles than the guide below.)--}}
{{--    var style = {--}}
{{--        base: {--}}
{{--            color: '#32325d',--}}
{{--            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',--}}
{{--            fontSmoothing: 'antialiased',--}}
{{--            fontSize: '16px',--}}
{{--            '::placeholder': {--}}
{{--                color: '#aab7c4'--}}
{{--            }--}}
{{--        },--}}
{{--        invalid: {--}}
{{--            color: '#fa755a',--}}
{{--            iconColor: '#fa755a'--}}
{{--        }--}}
{{--    };--}}

{{--    // Create an instance of the card Element.--}}
{{--    var card = elements.create('card', {style: style});--}}

{{--    // Add an instance of the card Element into the `card-element` <div>.--}}
{{--    card.mount('#card-element');--}}

{{--    // Handle real-time validation errors from the card Element.--}}
{{--    card.addEventListener('change', function(event) {--}}
{{--        var displayError = document.getElementById('card-errors');--}}
{{--        if (event.error) {--}}
{{--            displayError.textContent = event.error.message;--}}
{{--        } else {--}}
{{--            displayError.textContent = '';--}}
{{--        }--}}
{{--    });--}}

{{--    // Handle form submission.--}}
{{--    var form = document.getElementById('payment-form');--}}
{{--    form.addEventListener('submit', function(event) {--}}
{{--        event.preventDefault();--}}

{{--        stripe.createToken(card).then(function(result) {--}}
{{--            if (result.error) {--}}
{{--                // Inform the user if there was an error.--}}
{{--                var errorElement = document.getElementById('card-errors');--}}
{{--                errorElement.textContent = result.error.message;--}}
{{--            } else {--}}
{{--                // Send the token to your server.--}}
{{--                stripeTokenHandler(result.token);--}}
{{--            }--}}
{{--        });--}}
{{--    });--}}

{{--    // Submit the form with the token ID.--}}
{{--    function stripeTokenHandler(token) {--}}
{{--        // Insert the token ID into the form so it gets submitted to the server--}}
{{--        var form = document.getElementById('payment-form');--}}
{{--        var hiddenInput = document.createElement('input');--}}
{{--        hiddenInput.setAttribute('type', 'hidden');--}}
{{--        hiddenInput.setAttribute('name', 'stripeToken');--}}
{{--        hiddenInput.setAttribute('value', token.id);--}}
{{--        form.appendChild(hiddenInput);--}}

{{--        // Submit the form--}}
{{--        form.submit();--}}
{{--    }--}}

{{--</script>--}}

{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
{{--</body>--}}
{{--</html>--}}
{{--=====================================================================================================================--}}
@extends('layouts.master')
@section('title', 'payments')
{{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}


@section('body')

    <style type="text/css">
        .panel-title {
            display: inline;
            font-weight: bold;
        }
        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }
    </style>

<div class="container">

    <h1><br/></h1>

    <div class="row">
        <aside id="column-left" class="col-md-3" >
            <span class="latest_default_width" style="display:none; visibility:hidden"></span>
            <div class="swiper-viewport">
                <div id="banner0" class="swiper-container  single-banner ">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide"><a href="{{route('home')}}"><img src="assets/frontend/image/shop_now.jpg" alt="leftbanner" class="img-responsive" /></a></div>
                    </div>
                </div>
            </div>
        </aside>
        <div class="col-md-6  ">

            <div class="panel panel-default credit-card-box">
                <div class="panel-heading display-table justify-content-center" style="width: 100%;" >
                    <div class="row display-tr" >
                        <h3 class="panel-title display-td "style="font-weight: bold; margin-left:-10px;padding-left: 171px ">Payment Details</h3>
                        <div class="display-td" >
                            <img class="img-responsive " src="http://i76.imgup.net/accepted_c22e0.png">
                        </div>
                    </div>
                </div>
                <div class="panel-body">

                    @if (Session::has('success'))
                        <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif

                    <form role="form" action="{{ route('stripe.post') }}" method="post" class="require-validation"
                          data-cc-on-file="false"
                          data-stripe-publishable-key="pk_test_iOO6NQVwi7P4rgeTGXN7sh5V00HeKTTQey"
                          id="payment-form">
                        @csrf

                        <div class='form-row row'>
                            <div class='col-xs-12 form-group'>
                                <label class='control-label pull-left'>Name on Card</label> <input
                                    class='form-control' size='4' type='text'>
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-xs-12 form-group card required'>
                                <label class='control-label pull-left'>Card Number</label> <input
                                    autocomplete='off' class='form-control card-number' size='20'
                                    type='text'>
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                <label class='control-label pull-left'>CVC</label> <input autocomplete='off'
                                                                                class='form-control card-cvc' placeholder='ex. 311' size='4'
                                                                                type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label pull-left'>Expiration Month</label> <input
                                    class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label pull-left'>Expiration Year</label> <input
                                    class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='text'>
                            </div>
                        </div>

                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>
<?php
//                        use App\Models\Cart;
//                        use Session;
                        if(\Session::has('cart')) {
                            $cart = \Session::get('cart');
                            $amount = $cart->totalPrice + $cart->shippingRate;
//                            $a = $cart->totalPrice;
//                            $b = $cart->shippingRate;
//                            $amount = $a + $b;
//dd($amount);
                        }

                        ?>
{{--===================================================================================--}}
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Conformation</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are You Sure Do You Want To Pay <h2>( Rs. {{$amount + 100}} )</h2> This Amount And Conform This Order
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       <button class="btn btn-primary btn-lg btn-block" type="submit">Conform Order</button>

                                    </div>
                                </div>
                            </div>
                        </div>
{{--===================================================================================--}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                        Pay Now ( Rs. {{$amount + 100}} )
                                    </button>
{{--                                    <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now ( Rs. {{$amount}} )</button>--}}
                                </div>
                                <div class="pull-right">
{{--                                    <input type="button" value="Cash On Delivery" id="button-payment-method" data-loading-text="Loading..." class="btn btn-primary">--}}

{{--                                    <a class="btn btn-primary btn-lg btn-block pull-right" href="{{route('StripeOrder-Placement')}}">Continue</a>--}}
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
    $(function() {
        var $form         = $(".require-validation");
        $('form.require-validation').bind('submit', function(e) {
            var $form         = $(".require-validation"),
                inputSelector = ['input[type=email]', 'input[type=password]',
                    'input[type=text]', 'input[type=file]',
                    'textarea'].join(', '),
                $inputs       = $form.find('.required').find(inputSelector),
                $errorMessage = $form.find('div.error'),
                valid         = true;
            $errorMessage.addClass('hide');

            $('.has-error').removeClass('has-error');
            $inputs.each(function(i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorMessage.removeClass('hide');
                    e.preventDefault();
                }
            });

            if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month').val(),
                    exp_year: $('.card-expiry-year').val()
                }, stripeResponseHandler);
            }

        });

        function stripeResponseHandler(status, response) {
            if (response.error) {
                $('.error')
                    .removeClass('hide')
                    .find('.alert')
                    .text(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }

    });
</script>
@endsection
