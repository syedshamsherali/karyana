@extends('layouts.master')
@section('title','Review')

@section('body')

<script>
    function quickbox(){
    if ($(window).width() > 767) {
    $('.quickview').magnificPopup({
    type:'iframe',
    delegate: 'a',
    preloader: true,
    tLoading: 'Loading image #%curr%...',
    });
    }
    }
    jQuery(document).ready(function() {quickbox();});
    jQuery(window).resize(function() {quickbox();});

    </script>
<link href="{{asset('catalog\view\javascript\jquery\datetimepicker\bootstrap-datetimepicker.min.css')}}">
    <div class="productpage">
        <ul class="breadcrumb">
        <li><a href="#?route=common/home"><i class="fa fa-home"></i></a></li>
    <li><a href="index73b3.html?route=product/category&amp;path=59">Hybrid</a></li>
    <li><a href="index0a08.html?route=product/product&amp;path=59&amp;product_id=40">feugiat diam</a></li>
    </ul>
    <div id="product-product" class="container">
        <div class="row">
        <div id="content" class="col-sm-12 productpage">
        <div class="row">                         <div class="col-sm-8 product-left">
        <div class="product-info">
        <div class="left product-image thumbnails">

        <!-- Aeipix Cloud-Zoom Image Effect Start -->
        <div class="image"><a class="thumbnail" href="{{asset('image/cache/catalog/6-1000x1000.jpg')}}" title="feugiat diam"><img id="tmzoom" src="image/cache/catalog/6-1000x1000.jpg" data-zoom-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/6-1000x1000.jpg" title="feugiat diam" alt="feugiat diam" /></a></div>


    <div class="additional-carousel">
        <div id="additional-arrows" class="arrows"></div>

        <div id="additional-slider" class="image-additional additional-slider">

        <div class="slider-item">
        <div class="product-block">
        <a href="{{asset('image/cache/catalog/6-1000x1000.jpg')}}" title="feugiat diam" class="elevatezoom-gallery" data-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/6-1000x1000.jpg" data-zoom-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/6-1000x1000.jpg"><img src="image/cache/catalog/6-1000x1000.jpg" width="74" height="74" title="feugiat diam" alt="feugiat diam" /></a>
        </div>
        </div>

        <div class="slider-item">
        <div class="product-block">
        <a href="{{asset('image/cache/catalog/7-1000x1000.jpg')}}" title="feugiat diam" class="elevatezoom-gallery" data-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/7-1000x1000.jpg" data-zoom-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/7-1000x1000.jpg"><img src="image/cache/catalog/7-1000x1000.jpg" width="74" height="74" title="feugiat diam" alt="feugiat diam" /></a>
        </div>
        </div>
        <div class="slider-item">
        <div class="product-block">
        <a href="{{asset('image/cache/catalog/8-1000x1000.jpg')}}" title="feugiat diam" class="elevatezoom-gallery" data-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/8-1000x1000.jpg" data-zoom-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/8-1000x1000.jpg"><img src="image/cache/catalog/8-1000x1000.jpg" width="74" height="74" title="feugiat diam" alt="feugiat diam" /></a>
        </div>
        </div>
        <div class="slider-item">
        <div class="product-block">
        <a href="{{asset('image/cache/catalog/9-1000x1000.jpg')}}" title="feugiat diam" class="elevatezoom-gallery" data-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/9-1000x1000.jpg" data-zoom-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/9-1000x1000.jpg"><img src="image/cache/catalog/9-1000x1000.jpg" width="74" height="74" title="feugiat diam" alt="feugiat diam" /></a>
        </div>
        </div>
        <div class="slider-item">
        <div class="product-block">
        <a href="{{asset('image/cache/catalog/10-1000x1000.jpg')}}" title="feugiat diam" class="elevatezoom-gallery" data-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/10-1000x1000.jpg" data-zoom-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/10-1000x1000.jpg"><img src="image/cache/catalog/10-1000x1000.jpg" width="74" height="74" title="feugiat diam" alt="feugiat diam" /></a>
        </div>
        </div>
        <div class="slider-item">
        <div class="product-block">
        <a href="{{asset('image/cache/catalog/11-1000x1000.jpg')}}" title="feugiat diam" class="elevatezoom-gallery" data-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/11-1000x1000.jpg" data-zoom-image="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/11-1000x1000.jpg"><img src="image/cache/catalog/11-1000x1000.jpg" width="74" height="74" title="feugiat diam" alt="feugiat diam" /></a>
        </div>
        </div>

        </div>
        </div>


        <!-- Aeipix Cloud-Zoom Image Effect End-->
        </div>



        </div>

        </div>
        <div class="col-sm-4 product-right text-left">

        <h3 class="product-title">feugiat diam</h3>
    <div class="rating-wrapper">
        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
    <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
    <a href="#" class="review-count" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">2 reviews</a>  <a href="#" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;" class="write-review"><i class="fa fa-pencil"></i>Write a review</a>

    </div>
    <ul class="list-unstyled">
        <li><span class="desc">Brands</span><a href="index98fa.html?route=product/manufacturer/info&amp;manufacturer_id=8">Apple</a></li>
    <li><span class="desc">Product Code:</span> product 11</li>
    <li><span class="desc">Availability:</span> In Stock</li>
    </ul>
    <ul class="list-unstyled price">
        <li><span class="price-old" style="text-decoration: line-through;">$123.20</span></li>
    <li><h2 class="special-price">$110.00</h2></li>
    <li class="price-tax">Ex Tax: $90.00</li>

    <li class="discount">30 or more $110.00</li>
    </ul>
    <div id="product">
        <div class="form-group cart">
        <label class="control-label qty" for="input-quantity">Qty</label>
        <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control" />
        <button type="button" id="button-cart" data-loading-text="Loading..." class="btn btn-primary btn-lg btn-block">Add to Cart</button>
    <!-- <span>  - OR -  </span> -->
    <div class="btn-group">

        <button type="button"  class="btn btn-default wishlist" title="Add to Wish List" onclick="wishlist.add('40');">Add to Wish List</button>
    <button type="button" class="btn btn-default compare" title="Compare this Product" onclick="compare.add('40');">Compare this Product</button>
    </div>

    </div>
    <input type="hidden" name="product_id" value="40" />

        <hr>
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style" data-url="index9144.html?route=product/product&amp;product_id=40"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
{{--    <script type="text/javascript" src="{{asset('../../../s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e')}}"></script>--}}
    <!-- AddThis Button END -->
    </div>
    </div>

    <!-- product page tab code start-->
    <div id="tabs_info" class="product-tab col-sm-12">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
            <li><a href="#tab-review" data-toggle="tab">Reviews (2)</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab-description"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>
            <div class="tab-pane" id="tab-review">
                <form action="{{route('post-feedback')}}" method="post" class="form-horizontal" id="form-review" class="needs-validation"  novalidate>
                    <div id="review"></div>
                    @csrf
                    <h4>Write a review</h4>
                    <div class="form-group required">
                        <div class="col-sm-12">
                            <label class="control-label" for="input-review">Your Review</label>
                            <textarea name="message" rows="5" id="input-review" class="form-control"></textarea>
                            <div class="help-block"><span class="text-danger">Note:</span> HTML is not translated!</div>
                        </div>
                    </div>
                    <div class="form-group required">
                        <div class="col-sm-12">
                            <label class="control-label">Rating</label>
                            &nbsp;&nbsp;&nbsp; Bad&nbsp;
                            <input type="radio" name="rating" value="1" />
                            &nbsp;
                            <input type="radio" name="rating" value="2" />
                            &nbsp;
                            <input type="radio" name="rating" value="3" />
                            &nbsp;
                            <input type="radio" name="rating" value="4" />
                            &nbsp;
                            <input type="radio" name="rating" value="5" />
                            &nbsp;Good</div>
                    </div>

                    <div class="buttons clearfix">
                        <div class="pull-right">
                            <button type="submit" id="button-review" data-loading-text="Loading..." class="btn btn-primary">Continue</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>


    <div class="box related">
        <div class="ax-title">
            <div class="box-heading"><span>Related Products</span></div>
        </div>
        <div class="box-content">
            <div id="products-related" class="related-products">



                <div id="related-arrows" class="arrows"> </div>

                <div class="box-product related-slider" id="related-slider">
                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <!-- <div class="image">
                                                <a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=28 "><img src="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/4-300x300.jpg " alt="consectetur elit " title="consectetur elit " class="img-responsive" /></a>

                                                <span class="saleicon sale">Sale</span>
                                                                                       </div> -->
                                <div class="image">
                                    <a href="index6320.html?route=product/product&amp;product_id=28">
                                        <img src="{{asset('image/cache/catalog/4-300x300.jpg')}}" title="consectetur elit" alt="consectetur elit" class="img-responsive reg-image"/>
                                        <img class="img-responsive hover-image" src="{{asset('image/cache/catalog/5-300x300.jpg')}}" title="consectetur elit" alt="consectetur elit"/>
                                    </a>

                                    <div class="saleback">
                                        <span class="saleicon sale">Sale</span>
                                    </div>


                                </div>

                                <div class="caption">


                           <span class="rating">
                                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                 </span>
                                    <h4><a href="#?route=product/product&amp;product_id=28">consectetur elit </a></h4>

                                    <p class="price">
                                        <span class="price-new">$110.00</span> <span class="price-old">$122.00</span>
                                        <span class="price-tax">Ex Tax: $90.00</span>

                                        <span class="percentsaving">10% off</span>

                                    </p>
                                    <div class="button-group">
                                        <button type="button"   data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart" onclick="cart.add('28 ');">Add to Cart<!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                        </button>
                                        <div class="quickview"  data-toggle="tooltip" data-placement="top" title="Quickview" ><a href="index097a.html?route=product/quick_view&amp;product_id=28">Quick View<!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                        <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('28 ');"><i class="fa fa-heart"></i></button>
                                        <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('28 ');"><i class="fa fa-exchange"></i></button>

                                    </div>



                                </div>
                                <!-- Aeipix Related Products Start -->


                            </div>
                        </div>
                    </div>
                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <!-- <div class="image">
                                                <a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=32 "><img src="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/10-300x300.jpg " alt="Suspendisse eget " title="Suspendisse eget " class="img-responsive" /></a>

                                                <span class="saleicon sale">Sale</span>
                                                                                       </div> -->
                                <div class="image">
                                    <a href="indexa17e.html?route=product/product&amp;product_id=32">
                                        <img src="{{asset('image/cache/catalog/10-300x300.jpg')}}" title="Suspendisse eget" alt="Suspendisse eget" class="img-responsive reg-image"/>
                                        <img class="img-responsive hover-image" src="{{asset('image/cache/catalog/11-300x300.jpg')}}" title="Suspendisse eget" alt="Suspendisse eget"/>
                                    </a>

                                    <div class="saleback">
                                        <span class="saleicon sale">Sale</span>
                                    </div>


                                </div>

                                <div class="caption">


                           <span class="rating">
                                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                 </span>
                                    <h4><a href="indexa17e.html?route=product/product&amp;product_id=32">Suspendisse eget </a></h4>

                                    <p class="price">
                                        <span class="price-new">$116.00</span> <span class="price-old">$122.00</span>
                                        <span class="price-tax">Ex Tax: $95.00</span>

                                        <span class="percentsaving">5% off</span>

                                    </p>
                                    <div class="button-group">
                                        <button type="button"   data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart" onclick="cart.add('32 ');">Add to Cart<!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                        </button>
                                        <div class="quickview"  data-toggle="tooltip" data-placement="top" title="Quickview" ><a href="indexcc95.html?route=product/quick_view&amp;product_id=32">Quick View<!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                        <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('32 ');"><i class="fa fa-heart"></i></button>
                                        <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('32 ');"><i class="fa fa-exchange"></i></button>

                                    </div>



                                </div>
                                <!-- Aeipix Related Products Start -->


                            </div>
                        </div>
                    </div>
                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <!-- <div class="image">
                                                <a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=34 "><img src="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/9-300x300.jpg " alt="convallis justo " title="convallis justo " class="img-responsive" /></a>

                                                                                          </div> -->
                                <div class="image">
                                    <a href="indexc21e.html?route=product/product&amp;product_id=34">
                                        <img src="{{asset('image/cache/catalog/9-300x300.jpg')}}" title="convallis justo" alt="convallis justo" class="img-responsive reg-image"/>
                                        <img class="img-responsive hover-image" src="{{asset('image/cache/catalog/10-300x300.jpg')}}" title="convallis justo" alt="convallis justo"/>
                                    </a>




                                </div>

                                <div class="caption">


                           <span class="rating">
                                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                 </span>
                                    <h4><a href="indexc21e.html?route=product/product&amp;product_id=34">convallis justo </a></h4>

                                    <p class="price">
                                        $122.00
                                        <span class="price-tax">Ex Tax: $100.00</span>


                                    </p>
                                    <div class="button-group">
                                        <button type="button"   data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart" onclick="cart.add('34 ');">Add to Cart<!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                        </button>
                                        <div class="quickview"  data-toggle="tooltip" data-placement="top" title="Quickview" ><a href="indexa341.html?route=product/quick_view&amp;product_id=34">Quick View<!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                        <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('34 ');"><i class="fa fa-heart"></i></button>
                                        <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('34 ');"><i class="fa fa-exchange"></i></button>

                                    </div>



                                </div>
                                <!-- Aeipix Related Products Start -->


                            </div>
                        </div>
                    </div>
                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <!-- <div class="image">
                                                <a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=36 "><img src="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/8-300x300.jpg " alt="Aenean quis " title="Aenean quis " class="img-responsive" /></a>

                                                                                          </div> -->
                                <div class="image">
                                    <a href="index5e0b.html?route=product/product&amp;product_id=36">
                                        <img src="{{asset('image/cache/catalog/8-300x300.jpg')}}" title="Aenean quis" alt="Aenean quis" class="img-responsive reg-image"/>
                                        <img class="img-responsive hover-image" src="{{asset('image/cache/catalog/13-300x300.jpg')}}" title="Aenean quis" alt="Aenean quis"/>
                                    </a>




                                </div>

                                <div class="caption">


                           <span class="rating">
                                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                 </span>
                                    <h4><a href="index5e0b.html?route=product/product&amp;product_id=36">Aenean quis </a></h4>

                                    <p class="price">
                                        $122.00
                                        <span class="price-tax">Ex Tax: $100.00</span>


                                    </p>
                                    <div class="button-group">
                                        <button type="button"   data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart" onclick="cart.add('36 ');">Add to Cart<!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                        </button>
                                        <div class="quickview"  data-toggle="tooltip" data-placement="top" title="Quickview" ><a href="index0d3e.html?route=product/quick_view&amp;product_id=36">Quick View<!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                        <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('36 ');"><i class="fa fa-heart"></i></button>
                                        <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('36 ');"><i class="fa fa-exchange"></i></button>

                                    </div>



                                </div>
                                <!-- Aeipix Related Products Start -->


                            </div>
                        </div>
                    </div>
                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <!-- <div class="image">
                                                <a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=40 "><img src="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/6-300x300.jpg " alt="feugiat diam " title="feugiat diam " class="img-responsive" /></a>

                                                <span class="saleicon sale">Sale</span>
                                                                                       </div> -->
                                <div class="image">
                                    <a href="index9144.html?route=product/product&amp;product_id=40">
                                        <img src="{{asset('image/cache/catalog/6-300x300.jpg')}}" title="feugiat diam" alt="feugiat diam" class="img-responsive reg-image"/>
                                        <img class="img-responsive hover-image" src="{{asset('image/cache/catalog/7-300x300.jpg')}}" title="feugiat diam" alt="feugiat diam"/>
                                    </a>

                                    <div class="saleback">
                                        <span class="saleicon sale">Sale</span>
                                    </div>


                                </div>

                                <div class="caption">


                           <span class="rating">
                                                                                 <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                 </span>
                                    <h4><a href="index9144.html?route=product/product&amp;product_id=40">feugiat diam </a></h4>

                                    <p class="price">
                                        <span class="price-new">$110.00</span> <span class="price-old">$123.20</span>
                                        <span class="price-tax">Ex Tax: $90.00</span>

                                        <span class="percentsaving">11% off</span>

                                    </p>
                                    <div class="button-group">
                                        <button type="button"   data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart" onclick="cart.add('40 ');">Add to Cart<!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                        </button>
                                        <div class="quickview"  data-toggle="tooltip" data-placement="top" title="Quickview" ><a href="index9aba.html?route=product/quick_view&amp;product_id=40">Quick View<!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                        <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('40 ');"><i class="fa fa-heart"></i></button>
                                        <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('40 ');"><i class="fa fa-exchange"></i></button>

                                    </div>



                                </div>
                                <!-- Aeipix Related Products Start -->


                            </div>
                        </div>
                    </div>
                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <!-- <div class="image">
                                                <a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=42 "><img src="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/3-300x300.jpg " alt="Lorem ipsum dolor " title="Lorem ipsum dolor " class="img-responsive" /></a>

                                                <span class="saleicon sale">Sale</span>
                                                                                       </div> -->
                                <div class="image">
                                    <a href="indexbb02.html?route=product/product&amp;product_id=42">
                                        <img src="{{asset('image/cache/catalog/3-300x300.jpg')}}" title="Lorem ipsum dolor" alt="Lorem ipsum dolor" class="img-responsive reg-image"/>
                                        <img class="img-responsive hover-image" src="{{'image/cache/catalog/8-300x300.jpg'}}" title="Lorem ipsum dolor" alt="Lorem ipsum dolor"/>
                                    </a>

                                    <div class="saleback">
                                        <span class="saleicon sale">Sale</span>
                                    </div>


                                </div>

                                <div class="caption">


                           <span class="rating">
                                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                 </span>
                                    <h4><a href="indexbb02.html?route=product/product&amp;product_id=42">Lorem ipsum dolor </a></h4>

                                    <p class="price">
                                        <span class="price-new">$110.00</span> <span class="price-old">$122.00</span>
                                        <span class="price-tax">Ex Tax: $90.00</span>

                                        <span class="percentsaving">10% off</span>

                                    </p>
                                    <div class="button-group">
                                        <button type="button"   data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart" onclick="cart.add('42 ');">Add to Cart<!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                        </button>
                                        <div class="quickview"  data-toggle="tooltip" data-placement="top" title="Quickview" ><a href="index6d6f.html?route=product/quick_view&amp;product_id=42">Quick View<!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                        <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('42 ');"><i class="fa fa-heart"></i></button>
                                        <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('42 ');"><i class="fa fa-exchange"></i></button>

                                    </div>



                                </div>
                                <!-- Aeipix Related Products Start -->


                            </div>
                        </div>
                    </div>
                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <!-- <div class="image">
                                                <a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=43 "><img src="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/11-300x300.jpg " alt="non nibh hendrerit " title="non nibh hendrerit " class="img-responsive" /></a>

                                                <span class="saleicon sale">Sale</span>
                                                                                       </div> -->
                                <div class="image">
                                    <a href="indexb8ca.html?route=product/product&amp;product_id=43">
                                        <img src="{{asset('image/cache/catalog/11-300x300.jpg')}}" title="non nibh hendrerit" alt="non nibh hendrerit" class="img-responsive reg-image"/>
                                        <img class="img-responsive hover-image" src="{{asset('image/cache/catalog/12-300x300.jpg')}}" title="non nibh hendrerit" alt="non nibh hendrerit"/>
                                    </a>

                                    <div class="saleback">
                                        <span class="saleicon sale">Sale</span>
                                    </div>


                                </div>

                                <div class="caption">


                           <span class="rating">
                                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                 </span>
                                    <h4><a href="indexb8ca.html?route=product/product&amp;product_id=43">non nibh hendrerit </a></h4>

                                    <p class="price">
                                        <span class="price-new">$422.00</span> <span class="price-old">$602.00</span>
                                        <span class="price-tax">Ex Tax: $350.00</span>

                                        <span class="percentsaving">30% off</span>

                                    </p>
                                    <div class="button-group">
                                        <button type="button"   data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart" onclick="cart.add('43 ');">Add to Cart<!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                        </button>
                                        <div class="quickview"  data-toggle="tooltip" data-placement="top" title="Quickview" ><a href="indexff8a.html?route=product/quick_view&amp;product_id=43">Quick View<!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                        <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('43 ');"><i class="fa fa-heart"></i></button>
                                        <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('43 ');"><i class="fa fa-exchange"></i></button>

                                    </div>



                                </div>
                                <!-- Aeipix Related Products Start -->


                            </div>
                        </div>
                    </div>
                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <!-- <div class="image">
                                                <a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=44 "><img src="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/12-300x300.jpg " alt="Proin vitae mi " title="Proin vitae mi " class="img-responsive" /></a>

                                                                                          </div> -->
                                <div class="image">
                                    <a href="indexd94c.html?route=product/product&amp;product_id=44">
                                        <img src="{{asset('image/cache/catalog/12-300x300.jpg')}}" title="Proin vitae mi" alt="Proin vitae mi" class="img-responsive reg-image"/>
                                        <img class="img-responsive hover-image" src="{{asset('image/cache/catalog/13-300x300.jpg')}}" title="Proin vitae mi" alt="Proin vitae mi"/>
                                    </a>




                                </div>

                                <div class="caption">


                           <span class="rating">
                                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                 </span>
                                    <h4><a href="indexd94c.html?route=product/product&amp;product_id=44">Proin vitae mi </a></h4>

                                    <p class="price">
                                        $1,202.00
                                        <span class="price-tax">Ex Tax: $1,000.00</span>


                                    </p>
                                    <div class="button-group">
                                        <button type="button"   data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart" onclick="cart.add('44 ');">Add to Cart<!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                        </button>
                                        <div class="quickview"  data-toggle="tooltip" data-placement="top" title="Quickview" ><a href="index5b4c.html?route=product/quick_view&amp;product_id=44">Quick View<!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                        <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('44 ');"><i class="fa fa-heart"></i></button>
                                        <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('44 ');"><i class="fa fa-exchange"></i></button>

                                    </div>



                                </div>
                                <!-- Aeipix Related Products Start -->


                            </div>
                        </div>
                    </div>
                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <!-- <div class="image">
                                                <a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=45 "><img src="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/13-300x300.jpg " alt="Aenean sodales " title="Aenean sodales " class="img-responsive" /></a>

                                                                                          </div> -->
                                <div class="image">
                                    <a href="index531d.html?route=product/product&amp;product_id=45">
                                        <img src="{{asset('image/cache/catalog/13-300x300.jpg')}}" title="Aenean sodales" alt="Aenean sodales" class="img-responsive reg-image"/>
                                        <img class="img-responsive hover-image" src="{{asset('image/cache/catalog/14-300x300.jpg')}}" title="Aenean sodales" alt="Aenean sodales"/>
                                    </a>




                                </div>

                                <div class="caption">


                           <span class="rating">
                                                                                 <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                 </span>
                                    <h4><a href="index531d.html?route=product/product&amp;product_id=45">Aenean sodales </a></h4>

                                    <p class="price">
                                        $2,000.00
                                        <span class="price-tax">Ex Tax: $2,000.00</span>


                                    </p>
                                    <div class="button-group">
                                        <button type="button"   data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart" onclick="cart.add('45 ');">Add to Cart<!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                        </button>
                                        <div class="quickview"  data-toggle="tooltip" data-placement="top" title="Quickview" ><a href="index63bb.html?route=product/quick_view&amp;product_id=45">Quick View<!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                        <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('45 ');"><i class="fa fa-heart"></i></button>
                                        <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('45 ');"><i class="fa fa-exchange"></i></button>

                                    </div>



                                </div>
                                <!-- Aeipix Related Products Start -->


                            </div>
                        </div>
                    </div>
                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <!-- <div class="image">
                                                <a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=47 "><img src="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/2-300x300.jpg " alt="consequat tincidunt " title="consequat tincidunt " class="img-responsive" /></a>

                                                                                          </div> -->
                                <div class="image">
                                    <a href="indexd21c.html?route=product/product&amp;product_id=47">
                                        <img src="{{asset('image/cache/catalog/2-300x300.jpg')}}" title="consequat tincidunt" alt="consequat tincidunt" class="img-responsive reg-image"/>
                                        <img class="img-responsive hover-image" src="{{asset('image/cache/catalog/8-300x300.jpg')}}" title="consequat tincidunt" alt="consequat tincidunt"/>
                                    </a>




                                </div>

                                <div class="caption">


                           <span class="rating">
                                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                 </span>
                                    <h4><a href="indexd21c.html?route=product/product&amp;product_id=47">consequat tincidunt </a></h4>

                                    <p class="price">
                                        $122.00
                                        <span class="price-tax">Ex Tax: $100.00</span>


                                    </p>
                                    <div class="button-group">
                                        <button type="button"   data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart" onclick="cart.add('47 ');">Add to Cart<!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                        </button>
                                        <div class="quickview"  data-toggle="tooltip" data-placement="top" title="Quickview" ><a href="index7f5a.html?route=product/quick_view&amp;product_id=47">Quick View<!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                        <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('47 ');"><i class="fa fa-heart"></i></button>
                                        <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('47 ');"><i class="fa fa-exchange"></i></button>

                                    </div>



                                </div>
                                <!-- Aeipix Related Products Start -->


                            </div>
                        </div>
                    </div>
                    <div class="slider-item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="product-block product-thumb transition">
                            <div class="product-block-inner">
                                <!-- <div class="image">
                                                <a href="https://demoopc.aeipix.com/AX02/metro23/index.php?route=product/product&amp;product_id=48 "><img src="https://demoopc.aeipix.com/AX02/metro23/image/cache/catalog/7-300x300.jpg " alt="Mauris sed sapien " title="Mauris sed sapien " class="img-responsive" /></a>

                                                <span class="saleicon sale">Sale</span>
                                                                                       </div> -->
                                <div class="image">
                                    <a href="indexb77e.html?route=product/product&amp;product_id=48">
                                        <img src="{{asset('image/cache/catalog/7-300x300.jpg')}}" title="Mauris sed sapien" alt="Mauris sed sapien" class="img-responsive reg-image"/>
                                        <img class="img-responsive hover-image" src="{{asset('image/cache/catalog/8-300x300.jpg')}}" title="Mauris sed sapien" alt="Mauris sed sapien"/>
                                    </a>

                                    <div class="saleback">
                                        <span class="saleicon sale">Sale</span>
                                    </div>


                                </div>

                                <div class="caption">


                           <span class="rating">
                                                                                 <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                                                           <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                                 </span>
                                    <h4><a href="indexb77e.html?route=product/product&amp;product_id=48">Mauris sed sapien </a></h4>

                                    <p class="price">
                                        <span class="price-new">$110.00</span> <span class="price-old">$122.00</span>
                                        <span class="price-tax">Ex Tax: $90.00</span>

                                        <span class="percentsaving">10% off</span>

                                    </p>
                                    <div class="button-group">
                                        <button type="button"   data-placement="top" data-toggle="tooltip" title="Add to cart" class="addtocart" onclick="cart.add('48 ');">Add to Cart<!-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> -->
                                        </button>
                                        <div class="quickview"  data-toggle="tooltip" data-placement="top" title="Quickview" ><a href="indexdd37.html?route=product/quick_view&amp;product_id=48">Quick View<!-- <i class="fa fa-eye" aria-hidden="true"></i> --></a></div>
                                        <button class="wishlist" type="button" data-toggle="tooltip" data-placement="top" title="Add to Wish List " onclick="wishlist.add('48 ');"><i class="fa fa-heart"></i></button>
                                        <button class="compare" type="button" data-toggle="tooltip" data-placement="top" title="Compare this Product " onclick="compare.add('48 ');"><i class="fa fa-exchange"></i></button>

                                    </div>



                                </div>
                                <!-- Aeipix Related Products Start -->


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    </div>
    </div>
    </div>
{{--<script type="text/javascript" src="{{asset('catalog\view\javascript\jquery\datetimepicker\bootstrap-datetimepicker.min.js')}}"></script>--}}
    <script type="text/javascript"><!--
        $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
            $.ajax({
                url: 'index.php?route=product/product/getRecurringDescription',
                type: 'post',
                data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
                dataType: 'json',
                beforeSend: function() {
                    $('#recurring-description').html('');
                },
                success: function(json) {
                    $('.alert-dismissible, .text-danger').remove();

                    if (json['success']) {
                        $('#recurring-description').html(json['success']);
                    }
                }
            });
        });
        //--></script>
    <script type="text/javascript"><!--
        $('#button-cart').on('click', function() {
            $.ajax({
                {{--url: '{{route('index.php?route=checkout/cart/add')}}',--}}
                type: 'post',
                data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
                dataType: 'json',
                beforeSend: function() {
                    $('#button-cart').button('loading');
                },
                complete: function() {
                    $('#button-cart').button('reset');
                },
                success: function(json) {
                    $('.alert-dismissible, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json['error']) {
                        if (json['error']['option']) {
                            for (i in json['error']['option']) {
                                var element = $('#input-option' + i.replace('_', '-'));

                                if (element.parent().hasClass('input-group')) {
                                    element.parent().before('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                } else {
                                    element.before('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                }
                            }
                        }

                        if (json['error']['recurring']) {
                            $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    }

                    if (json['success']) {
                        $.notify({
                            message: json['success'],
                            target: '_blank'
                        },{
                            // settings
                            element: 'body',
                            position: null,
                            type: "info",
                            allow_dismiss: true,
                            newest_on_top: false,
                            placement: {
                                from: "top",
                                align: "center"
                            },
                            offset: 0,
                            spacing: 10,
                            z_index: 2031,
                            delay: 5000,
                            timer: 1000,
                            url_target: '_blank',
                            mouse_over: null,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            onShow: null,
                            onShown: null,
                            onClose: null,
                            onClosed: null,
                            icon_type: 'class',
                            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +
                                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">&nbsp;&times;</button>' +
                                '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
                                '<div class="progress" data-notify="progressbar">' +
                                '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                                '</div>' +
                                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                                '</div>'
                        });

                        // $('#cart > button').html('<i class="fa fa-shopping-cart"></i><span id="cart-total"> ' + json['total'] + '</span>');
                        $('#cart-total').html( json['total']);

                        //$('html, body').animate({ scrollTop: 0 }, 'slow');

                        $('#cart > ul').load('index1e1c.html?route=common/cart/info%20ul%20li');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
        //--></script>
{{--    <script type="text/javascript"><!----}}
{{--        $('.date').datetimepicker({--}}
{{--            language: 'en-gb',--}}
{{--            pickTime: false--}}
{{--        });--}}

{{--        $('.datetime').datetimepicker({--}}
{{--            language: 'en-gb',--}}
{{--            pickDate: true,--}}
{{--            pickTime: true--}}
{{--        });--}}

{{--        $('.time').datetimepicker({--}}
{{--            language: 'en-gb',--}}
{{--            pickDate: false--}}
{{--        });--}}

{{--        $('button[id^=\'button-upload\']').on('click', function() {--}}
{{--            var node = this;--}}

{{--            $('#form-upload').remove();--}}

{{--            $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');--}}

{{--            $('#form-upload input[name=\'file\']').trigger('click');--}}

{{--            if (typeof timer != 'undefined') {--}}
{{--                clearInterval(timer);--}}
{{--            }--}}

{{--            timer = setInterval(function() {--}}
{{--                if ($('#form-upload input[name=\'file\']').val() != '') {--}}
{{--                    clearInterval(timer);--}}

{{--                    $.ajax({--}}
{{--                        url: 'index.php?route=tool/upload',--}}
{{--                        type: 'post',--}}
{{--                        dataType: 'json',--}}
{{--                        data: new FormData($('#form-upload')[0]),--}}
{{--                        cache: false,--}}
{{--                        contentType: false,--}}
{{--                        processData: false,--}}
{{--                        beforeSend: function() {--}}
{{--                            $(node).button('loading');--}}
{{--                        },--}}
{{--                        complete: function() {--}}
{{--                            $(node).button('reset');--}}
{{--                        },--}}
{{--                        success: function(json) {--}}
{{--                            $('.text-danger').remove();--}}

{{--                            if (json['error']) {--}}
{{--                                $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');--}}
{{--                            }--}}

{{--                            if (json['success']) {--}}
{{--                                alert(json['success']);--}}

{{--                                $(node).parent().find('input').val(json['code']);--}}
{{--                            }--}}
{{--                        },--}}
{{--                        error: function(xhr, ajaxOptions, thrownError) {--}}
{{--                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);--}}
{{--                        }--}}
{{--                    });--}}
{{--                }--}}
{{--            }, 500);--}}
{{--        });--}}
{{--        //--></script>--}}
    <script type="text/javascript"><!--
        $('#review').delegate('.pagination a', 'click', function(e) {
            e.preventDefault();

            $('#review').fadeOut('slow');

            $('#review').load(this.href);

            $('#review').fadeIn('slow');
        });

        $('#review').load('index0845.html?route=product/product/review&amp;product_id=40');

        $('#button-review').on('click', function() {
            $.ajax({
                url: 'index.php?route=product/product/write&product_id=40',
                type: 'post',
                dataType: 'json',
                data: $("#form-review").serialize(),
                beforeSend: function() {
                    $('#button-review').button('loading');
                },
                complete: function() {
                    $('#button-review').button('reset');
                },
                success: function(json) {
                    $('.alert-dismissible').remove();

                    if (json['error']) {
                        $('#review').after('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        $('#review').after('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                        $('input[name=\'name\']').val('');
                        $('textarea[name=\'text\']').val('');
                        $('input[name=\'rating\']:checked').prop('checked', false);
                    }
                }
            });
        });

        //$(document).ready(function() {
        //	$('.thumbnails').magnificPopup({
        //		type:'image',
        //		delegate: 'a',
        //		gallery: {
        //			enabled: true
        //		}
        //	});
        //});


        $(document).ready(function() {
            if ($(window).width() > 767) {
                $("#tmzoom").elevateZoom({

                    gallery:'additional-carousel',
                    //inner zoom

                    zoomType : "inner",
                    cursor: "crosshair"

                    /*//tint

                    tint:true,
                    tintColour:'#F90',
                    tintOpacity:0.5

                    //lens zoom

                    zoomType : "lens",
                    lensShape : "round",
                    lensSize : 200

                    //Mousewheel zoom

                    scrollZoom : true*/


                });
                var z_index = 0;

                $(document).on('click', '.thumbnail', function () {
                    $('.thumbnails').magnificPopup('open', z_index);
                    return false;
                });

                $('.additional-carousel a').click(function() {
                    var smallImage = $(this).attr('data-image');
                    var largeImage = $(this).attr('data-zoom-image');
                    var ez =   $('#tmzoom').data('elevateZoom');
                    $('.thumbnail').attr('href', largeImage);
                    ez.swaptheimage(smallImage, largeImage);
                    z_index = $(this).index('.additional-carousel a');
                    return false;
                });

            }else{
                $(document).on('click', '.thumbnail', function () {
                    $('.thumbnails').magnificPopup('open', 0);
                    return false;
                });
            }
        });
        $(document).ready(function() {
            $('.thumbnails').magnificPopup({
                delegate: 'a.elevatezoom-gallery',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-with-zoom',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                },
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function(item) {
                        return item.el.attr('title');
                    }
                }
            });
        });


        //--></script>


@endsection
