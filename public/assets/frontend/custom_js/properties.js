function bindChosenPlugin() {
    for (let i = 0; i < $('select').length; i++) {
        $('select').eq(i).chosen();
    }
    $('.chosen-container-single .chosen-single').addClass('input-text');
    $('.chosen-choices').addClass('input-text');
}

bindChosenPlugin();
// Variable to store message about property saving/submitting======//
let message = '';
let redirectTo = '';
// =================================================================//
$('.add-room-button').on('click', function (event) {
    event.preventDefault();
    $(document).off('ajaxstop');
    let successCallback = function (room) {
        $('.submit-address').append(
            "<div class='add-room'>" +
            room +
            "</div>"
        );
        $('.remove-room:last').show();
        $('.room-no:last').html($('.room-form').length);
        $('.swiper-container:last').removeClass('container-1').addClass('container-' + $('.swiper-container').length).attr('data-container', $('.swiper-container').length);
        bindChosenPlugin();
        bindSwiperPlugin();
    };
    let errorCallback = function (error) {
        console.log(error);
    };
    let params = {
        'successCallback': successCallback,
        'errorCallback': errorCallback,
        'url': getRoomFormRoute,
        'type': 'get',
        'data': ''
    };
    doAjaxCall(params);
});
//============ remove Room ================//
$(document).on('click', '.remove-room', function (event) {
    event.preventDefault();
    let button = $(this);
    if ($(button).attr('data-id') !== "") {
        let successCallback = function (response) {
                console.log('Room Deleted...');
            },
            errorCallback = function (response) {
                console.log('error deleting room...');
            },
            url = $(button).attr('href');

        let params = {
            'successCallback': successCallback,
            'errorCallback': errorCallback,
            'url': url,
            'type': 'get',
            'data': ''
        };
        doAjaxCall(params);
    }
    $(button).parents('.add-room').remove();
});

// Validating and setting property image =========================
$('.property-image-picker').on('click', function (e) {
    e.preventDefault();
    $('#property_image_button').trigger('click');
});
$('#property_image_button').on('change', function (event) {
    let imagePath = $(this).val();
    let allowedExtensions = /(\.jpg|\.png|\.jpeg)$/i;

    if (!allowedExtensions.exec(imagePath)) {
        alert('Image format is not correct');
        $(this).val('');
        $('#property-image-preview').html('');
        return false;
    } else {
        imagePickerType = 'property';
        imagePickerButton = $(this);
        showImageCropper(event);
    }
});

// Validating and setting room images
$(document).on('click', '.room-image-picker', function () {
    $(this).parent().find('.room-image').trigger('click');
});
$(document).on('change', '.room-image', function (event) {
    let button = $(this);
    let imagePath = $(this).val();
    let allowedExtensions = /(\.jpg|\.png|\.jpeg)$/i;

    if (!allowedExtensions.exec(imagePath)) {
        alert('Image format is not correct');
        $(button).val('');
        return false;
    } else {
        imagePickerType = 'room';
        imagePickerButton = button;
        showImageCropper(event);
    }
});

// =====================================================================//
$('#submitProperty').on('click', function (e) {
    $('.is_submitted').val(1);
    $(document).off('ajaxsstop');
    redirectTo = 'mySubmittedProperties';
    message = 'Your Property has been submitted to admin for approval';

    e.preventDefault();
    if (isFormValidated()) {
        var ale = $.confirm({
            title: 'Alert!',
            content: 'Submit property required ' + creditonProperty + ' credits',
            buttons: {
                confirm: function () {
                    let errorCallback = function (response) {
                        console.log(response);
                    };
                    let successCallback = function (response) {
                        if (response === "1") {
                            $('.page_loader').show(); // show loader
                            addProperty();
                        } else {
                            $.confirm({
                                title: 'Alert!',
                                content: 'You dont have enough credits',
                                buttons: {
                                    buy: function () {
                                        $('.property-title').val($('.title').val());
                                        $('#payment-modal').modal('show');
                                    },
                                    cancel: function () {
                                    }
                                }
                            });
                        }
                    };
                    let title=$('.title').val();
                    let url = '/credit-deduction/'+title;
                    let params = {
                        'errorCallback': errorCallback,
                        'successCallback': successCallback,
                        'url': url,
                        'data':'',
                        'type': 'post'
                    };
                    doAjaxCall(params);
                },
                cancel: function () {
                }
            }
        });
    }
});

// ======================== Ajax code to send data =====================//
$('#saveProperty').on('click', function (e) {
    e.preventDefault();
    if (isFormValidated()){
        $(document).off('ajaxsstop');
        $('.page_loader').show(); // show loader
        message = 'Your Property details has been saved';
        redirectTo = 'mySavedProperties';
        $('.is_submitted').val(0);
        // $('#property-form').submit();
        addProperty();
    }
});

function addProperty() {
    let context = $('.property-form')[0];
    let propertyForm = new FormData(context);
    let url = addPropertyRoute;
    let propertyId = $(context).find('.id');
    if ($(propertyId).val() !== "") {
        url += '/' + $(propertyId).val();
    }
    let errorCallback = function (response) {
        let errors = response.responseJSON.errors;
        for (let key in errors) {
            let element = $(context).find('.' + key);
            showFieldError(errors[key][0], element);
        }
        showAndScrollToMessage();
    };
    let successCallback = function (response) {
        $('.property_id').val(parseInt(response));
        $(propertyId).val(parseInt(response));
        addRoom();
    };
    let params = {
        'errorCallback': errorCallback,
        'successCallback': successCallback,
        'url': url,
        'data': propertyForm,
        'type': 'post'
    };
    doAjaxCall(params);
}

function addRoom() {
    let rooms = $('.add-room');
    let isDataSaved = true;
    $(rooms).each(function () {
        let room = $(this);
        let roomIdField = $(room).find('id');
        let roomForm = $(this).find('.room-form')[0];
        let roomData = new FormData(roomForm);
        let url = addRoomRoute;
        if ($(room).find('.id').val() !== "") {
            url += '/' + $(room).find('.id').val();
        }
        let errorCallback = function (response) {
            let errors = response.responseJSON.errors;
            console.log(errors);
            for (let key in errors) {
                let element = $(room).find('.' + key);
                showFieldError(errors[key][0], element);
            }
            isDataSaved = false;
        };
        let successCallback = function (response) {
            console.log(response);
            let roomId = parseInt(response);
            $(roomIdField).val(roomId);
        };
        let params = {
            'errorCallback': errorCallback,
            'successCallback': successCallback,
            'url': url,
            'data': roomData,
            'type': 'post'
        };
        doAjaxCall(params);
    });
    $(document).ajaxStop(function () {
        if (!isDataSaved) {
            showAndScrollToMessage();
        } else {
            $('#redirect_route').val(redirectTo);
            $('#message').val(message);
            $('#redirect-form').submit();
        }
    });
}

//================ show errors ====================//
function showFieldError(message, element) {
    $(element).siblings('.invalid-feedback').html(message).show();
    $(element).addClass('invalid-field');
}

function showError(element) {
    $(element).siblings('.invalid-feedback').show();
    $(element).addClass('invalid-field');
}

//=============== hide error =====================//
function hideFieldError(element) {
    $(element).siblings('.invalid-feedback').hide();
    $(element).removeClass('invalid-field');
}

function hideFieldErrorWithMessage(message, element) {
    $(element).siblings('.invalid-feedback').html(message).hide();
    $(element).removeClass('invalid-field');
}

// ======== On focus remove error feedback ===========//
onFocusHideErrors();

function onFocusHideErrors() {
    $('input').on('focus change paste', function () {
        hideFieldError($(this));
    });
    $('#description').on('focus change paste', function () {
        if (isDescriptionProof) {
            hideFieldError($(this));
        }
    });
    $(document).on('change', 'select', function () {
        hideFieldError($(this));
    });
}

//========= Form validation =============//
function isFormValidated() {
    let isDataValid = true;
    let fields = $('.validate');
    for (let i = 0; i < fields.length; i++) {
        if ($(fields).eq(i).val() === "" || $(fields).eq(i).val() === null) {
            isDataValid = false;
            showError($(fields).eq(i));
        }
    }
    $('.rent').each(function () {
        if ($(this).val() <= 0) {
            isDataValid = false;
            showError($(this));
            // showFieldError('Rent should be greater than ZERO', $(this));
        }
    });
    $('.room-image').each(function () {
        if (!$(this).parent().find('.image_name').length && $(this).parents('form').find('.id').val() === ""){
            isDataValid = false;
        }
    });
    if ($('#description').val().length < 20) {
        isDataValid = false;
        showFieldError('Description should be minimum 20 characters long', $('#description'));
    }
    if (!$(document).find('.room-form').length > 0) {
        isDataValid = false;
        alert('Please add at least one room');
    }
    if (!isDataValid) {
        showAndScrollToMessage()
    }
    if (!isPlaceChanged) {
        isDataValid = false;
        $(input).addClass('invalid-field').siblings('.invalid-feedback').html('Please select valid location').show();
    }
    return isDataValid && isDescriptionProof;
}

//===================================================================//
//========= Fetch and set location cords from Google Map API=========//
let input = document.getElementById('route');
let isPlaceChanged = false;
if ($('#property_id').val() !== '') {
    isPlaceChanged = true;
}
let componentForm = {
    route: 'long_name',
    locality: 'long_name', //city
    administrative_area_level_1: 'short_name', //state
    postal_code: 'short_name' //zip code
};

// if user type/paste location placeChanged variable set to false
$(document).on('input', '#route', function () {
    isPlaceChanged = false;
});
$(document).on('paste', '#route', function (e) {
    e.preventDefault();
});

function fillInAddress() {
    let place = autocomplete.getPlace();
    isPlaceChanged = true;
    console.log(place);
    // Get each component of the address from the place details,
    // and then fill-in the corresponding field on the form.
    let address = '';
    for (let i = 0; i < place.address_components.length; i++) {
        let addressType = place.address_components[i].types[0];
        if (addressType === 'political') {
            address = place.address_components[i]['long_name'];
        }
        if (componentForm[addressType]) {
            let val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
            hideFieldError(document.getElementById(addressType));
        }
    }
    if (address !== '') {
        $('#route').val(address);
        $('#route').val(address);
    }
    GEO_LAT = place.geometry.location.lat();
    GEO_LONG = place.geometry.location.lng();
    $('#geo_lat').val(GEO_LAT);
    $('#geo_long').val(GEO_LONG);
    updateAddress();
    displayMap('google-map', false);
}

function initAutocomplete() {
    // Create the autocomplete object, restricting the search predictions to
    // geographical location types.
    autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});
    // Set initial restrict to the greater list of countries.
    autocomplete.setComponentRestrictions(
        {'country': ['pk']});
    // place fields that are returned to just the address components.
    autocomplete.setFields(['address_component', 'geometry']);

    // When the user selects an address from the drop-down, populate the
    // address fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);

    // in case of update property load previously stored map
    if ($('#geo_lat').val() !== "" && $('#geo_long').val() !== "") {
        GEO_LAT = parseFloat($('#geo_lat').val());
        GEO_LONG = parseFloat($('#geo_long').val());
        displayMap('google-map', false);
    }
}

let infowindow = null;

function displayMap(container, isDraggable) {
    let myLatLng = {lat: GEO_LAT, lng: GEO_LONG};
    infowindow = new google.maps.InfoWindow({
        content: 'Waiting for address..'
    });
    let mapOptions = {
        zoom: 12,
        center: myLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    let mapContainer = document.getElementById(container);
    let map = new google.maps.Map(mapContainer, mapOptions);
    let marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        draggable: isDraggable
    });
    getAddressFromLatLng(GEO_LAT, GEO_LONG);
    infowindow.open(map, marker);
    if (isDraggable) {
        google.maps.event.addListener(marker, 'dragend', function (e) {
            GEO_LAT = e.latLng.lat();
            GEO_LONG = e.latLng.lng();
            getAddressFromLatLng(GEO_LAT, GEO_LONG);
        });
    }
    showMap();
}

function showMap() {
    // noinspection JSJQueryEfficiency
    $('#google-map').css('min-height', '200px');
}

function showAndScrollToMessage() {
    $('.page_loader').hide(); // hide loader
    $('.error-message').show();
    $('html, body').animate({
        scrollTop: $("#messageArea").offset()
    }, 1000);
}

$(document).on('change', '.no_of_beds', function () {
    let beds = parseInt($(this).val());
    let availableBeds = $(this).parents('.total-beds').siblings('.available-beds').find('select');
    $(availableBeds).next('div').remove();
    let bedsOption = '<option></option>';
    for (let i = 0; i <= beds; i++) {
        bedsOption += '<option>' + i + '</option>';
    }
    $(availableBeds).replaceWith(
        '<select class="available_beds search-fields bootstrap-select input-text validate"' +
        ' data-placeholder="Available beds"' +
        ' name="available_beds">' +
        bedsOption +
        '</select>'
    );
    bindChosenPlugin();
});

// ========== room type change ===============
$(document).on('change', '.type', function (event) {
    event.preventDefault();
    /* Act on the event */
    let availableBeds = $(this).parents('.room-type').siblings('.available-beds').find('.available_beds');
    let totalBeds = $(this).parents('.room-type').siblings('.total-beds').find('.no_of_beds');
    let roomRent = $(this).parents('.room-type').siblings('.room-rent').find('label');
    $(totalBeds).next('.chosen-container').remove();
    $(availableBeds).next('.chosen-container').remove();
    if ($(this).val() === 'single') {
        $(roomRent).each(function () {
            $(this).html($(this).html().replace('/bed', ''));
        });
        $(totalBeds).replaceWith(
            '<input type="text" class="no_of_beds search-fields bootstrap-select input-text validate" value="1" readonly name="no_of_beds">'
        );
        $(availableBeds).replaceWith(
            '<select class="available_beds search-fields bootstrap-select input-text validate"' +
            ' data-placeholder="Available beds"' +
            ' name="available_beds">' +
            ' <option></option>' +
            ' <option>0</option>' +
            ' <option>1</option>' +
            '</select>'
        );
    } else {
        $(roomRent).each(function () {
            $(this).html($(this).html() + '/bed');
        });
        let beds = 10;
        let bedsOption = '<option></option>';
        for (let i = 1; i <= beds; i++) {
            bedsOption += '<option>' + i + '</option>';
        }
        $(totalBeds).replaceWith(
            '<select class="no_of_beds search-fields bootstrap-select input-text validate"' +
            ' data-placeholder="Total beds"' +
            ' name="no_of_beds">' +
            bedsOption +
            '</select>'
        );
        $(availableBeds).replaceWith(
            '<select class="available_beds search-fields bootstrap-select input-text validate"' +
            ' data-placeholder="Available beds"' +
            ' name="available_beds">' +
            ' <option></option>' +
            '</select>'
        );
    }
    bindChosenPlugin();
});

//=============== Ajax Calls =====================//
function doAjaxCall(params) {
    let successCallback = params['successCallback'],
        errorCallback = params['errorCallback'],
        url = params['url'],
        data = params['data'],
        type = params['type'];
    $.ajax({
        url: url,
        type: type,
        contentType: false,
        processData: false,
        data: data,
        success: function (response) {
            successCallback(response);
        },
        error: function (response) {
            console.log(response);
            errorCallback(response);
        }
    });
}

// ================ Show map in modal =================//
$(document).on('click', '#google-map', function (event) {
    event.preventDefault();
    /* Act on the event */
    let container = 'modal-map';
    GEO_LAT = parseFloat($('#geo_lat').val());
    GEO_LONG = parseFloat($('#geo_long').val());
    displayMap(container, true);
    $('#mapModal').modal('show');
});

$(document).on('click', '#currentLocation', function (event) {
    event.preventDefault();
    /* Act on the event */
    setCurrentLocation();
});
$(document).on('click', '#saveLocation', function (event) {
    event.preventDefault();
    /* Act on the event */
    $('#geo_lat').val(GEO_LAT);
    $('#geo_long').val(GEO_LONG);
    updateAddress();
    displayMap('google-map', false);
    $('#mapModal').modal('hide');
});
// Try HTML5 geolocation.
let GEO_LAT, GEO_LONG;

function setCurrentLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            let pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            GEO_LAT = pos.lat;
            GEO_LONG = pos.lng;
            displayMap('modal-map', true);
        }, function () {
            handleLocationError(true, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, map.getCenter());
    }

    function handleLocationError(browserHasGeolocation, pos) {
        // browserHasGeolocation ?
        //   'Error: The Geolocation service failed.' :
        //   'Error: Your browser doesn\'t support geolocation.';
    }
}

// // list of words to exclude from description
const badWords = ["kill", "bad", "shit", "bomb", "ass"];
// Profanity filter check for property description
let isDescriptionProof = true;

function getSmileyDayString(input) {
    const inp = input.split(" ");
    const intersection = inp.filter(element => badWords.includes(element));
    if (intersection.length !== 0) {
        showFieldError('Highlighted bad words are not allowed in description', $('#description'));
        isDescriptionProof = false;
    } else {
        hideFieldErrorWithMessage('Property Description is required', $('#description'));
        isDescriptionProof = true;
    }
    return intersection;
}

$('#description').highlightWithinTextarea({
    highlight: getSmileyDayString,
    className: 'red'
}).after('<div class="invalid-feedback">\n' +
    '    Property Description Is required\n' +
    '</div>');

/**
 *
 * @param lat
 * @param lng
 */
function getAddressFromLatLng(lat, lng) {
    let geocoder = new google.maps.Geocoder;
    let latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
    geocoder.geocode({'location': latlng}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            infowindow.setContent(results[0].formatted_address);
            console.log(results);
        }
    });
}

/**
 * update Address in address field as location changes
 */
function updateAddress() {
    let geocoder = new google.maps.Geocoder;
    let latlng = {lat: parseFloat(GEO_LAT), lng: parseFloat(GEO_LONG)};
    geocoder.geocode({'location': latlng}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            let place = results[0];
            $('#address').val(place.formatted_address).removeClass('invalid-field').siblings('.invalid-feedback').hide();

            let address = '';
            for (let i = 0; i < place.address_components.length; i++) {
                let addressType = place.address_components[i].types[0];
                if (addressType === 'political') {
                    address = place.address_components[i]['long_name'];
                }
                if (componentForm[addressType]) {
                    let val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                    hideFieldError(document.getElementById(addressType));
                }
            }
            if (address) {
                $('#route').val(address);
            }
        }
    });
}

bindSwiperPlugin();
var swipers;

function bindSwiperPlugin() {
    let swiperContainers = $('.swiper-container');
    let mySwipers = [];
    for (let i = 1; i <= swiperContainers.length; i++) {
        mySwipers[i] = new Swiper('.container-' + i, {
            // Optional parameters
            direction: 'horizontal',
            loop: false,
            simulateTouch: false,
            slidesPerView: 4,
            spaceBetween: 30,

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        })
    }
    swipers = mySwipers;
}

function addSwiperSlide(swiperNumber, img) {
    swipers[swiperNumber].prependSlide('<div class="swiper-slide"><img src="' + img + '" class="img-fluid"></div>');
    swipers[swiperNumber].slideTo(0);
}

//
$(document).on('click', '.swal-button', function () {
    addProperty();
});
//======================== Crop image ======================//
let croppedImage, cropperModal;
let cropper; //Store cropper object
let imagePickerButton;
$(document).ready(function () {
    croppedImage = document.getElementById('croppedImage'); //image cropper in modal
    cropperModal = $('#cropperModal'); //Modal to show cropper

    $(cropperModal).on('shown.bs.modal', function () {
        cropper = new Cropper(croppedImage, {
            aspectRatio: 1.5,
            viewMode: 1,
        });
    }).on('hidden.bs.modal', function () {
        cropper.destroy();
        cropper = null;
        $(imagePickerButton).val('');
    });

    $('#cropImage').on('click', function () {
        let canvas;
        $(cropperModal).modal('hide');
        if (cropper){
            canvas = cropper.getCroppedCanvas();
            //Set image preview
            if (imagePickerType === 'property'){
                $('#property-image-preview').html(
                    '<div class="col-lg-3 col-md-4">' +
                    '<img alt="" class="img-fluid" src="' + canvas.toDataURL() + '">' +
                    '</div>'
                );
                    $(imagePickerButton).siblings('.image-input').html(
                    '<input type="hidden" name="property_image"  class="property_image" value="'+canvas.toDataURL()+'">')
            }else if (imagePickerType === 'room'){
                let swiperNumber = $(imagePickerButton).parents('div.pad-20').siblings('.swiper-container').attr('data-container');
                addSwiperSlide(swiperNumber, canvas.toDataURL());
                $(imagePickerButton).after(
                    '<input type="hidden" name="image_name[]"  class="image_name" value="'+canvas.toDataURL()+'">'
                );
            }
        }
    });
});

let imagePickerType;
function showImageCropper(e) {
    let files = e.target.files;
    let done = function (url) {
        $(croppedImage).attr('src', url);
        $(cropperModal).modal('show');
    };

    let reader, file, url;
    if (files && files.length > 0) {
        file = files[0];
        if (URL) {
            done(URL.createObjectURL(file));
        } else if (FileReader) {
            reader = new FileReader();
            reader.onload = function (e) {
                done(reader.result);
            };
            reader.readAsDataURL(file);
        }
    }
}
