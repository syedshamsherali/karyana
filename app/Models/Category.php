<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    protected $fillable=['name','parent_id'];

//    public function sub_category(){
//        return $this->belongsTo(self::class , 'parent_id','id');
//    }
//
//    public function categories(){
//        return $this->hasMany(self::class,'id','parent_id');
//    }
//    public function saveCat($cate){
//        $qry = Category::create($cate);
//        return $qry;
//    }
    public function childs() {
        return $this->hasMany(self::class,'parent_id','id') ;
    }

    public function products(){
        return $this->belongsToMany(Product::class,'categories_products','category_id','product_id');
    }


}
