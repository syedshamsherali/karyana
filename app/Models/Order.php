<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = ['user_id', 'total_ammount', 'order_status','payment_status','transaction_number',];


    //
    public function orderDetails(){
        return $this->hasMany(OrderDetail::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function riders(){
        return $this->belongsTo(Rider::class);
    }

    public function assignOrder(){
        return $this->belongsTo(AssignOrder::class);
    }

    /**
     * @param $cart
     * @return mixed
     */

    public function saveorder($cart){
//dd($cart);
        $addorder=Order::create([
            'user_id'=>auth()->user()->id,
            'total_ammount'=>$cart->totalPrice + $cart->shippingRate,
            'order_status'=>'pending',
            'transaction_number'=>rand(1000000000,9999999999),
        ]);

        $lastOrderId=($addorder->id);
        $contents=$cart->contents;

        foreach ($contents as $data){
            $orderDetail = new OrderDetail();
            $orderDetail->order_id = $lastOrderId;
            $orderDetail->product_id=$data['product']->id;
            $orderDetail->quantity=$data['qty'];
            $orderDetail->amount=$data['price'];

            $orderDetail->save();

        }




        return $addorder;

    }

    /**
     * @param $formdata
     * @return mixed
     */
    public function changeStatus($formdata){
        $order_status=Order::find($formdata->id);
        $order_status->order_status='approved';
        $updated=$order_status->save();
        if($updated){
            return $updated;
        }
    }

    public function discardOrder($formdata){
        $order=Order::find($formdata->id);
        $order->delete();
        if($order){
            return $order;
        }
    }
}
