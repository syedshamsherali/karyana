<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    //

    protected $fillable = ['name', 'city', 'address', 'state', 'opening_time', 'closing_time', 'logo'];


    public function product()
    {
        return $this->hasMany(Product::class);
    }
    public function orderDetails(){
        return $this->hasManyThrough('App\Models\OrderDetail','App\Models\Product');
    }


    /**
     * @param $shopdata
     * @return bool
     */
    public function saveshop($shopdata)
    {

        if ($shopdata->hasFile('thumbnail')) {
            foreach ($shopdata->file('thumbnail') as $image) {
                $imageName = time() . "-" . $image->extension();
                $imagePath = public_path() . '/images';
                $image->move($imagePath, $imageName);
                $imageDbPath[] = $imageName;
            }
            $data = implode(',', $imageDbPath);
            $addshop = Shop::create([
                'name' => $shopdata->shop_name,
                'city' => $shopdata->city,
                'state' => $shopdata->state,
                'address' => $shopdata->address,
                'closing_time' => $shopdata->opening_time,
                'opening_time' => $shopdata->closing_time,
                'logo' => $data,

            ]);
            if ($addshop) {
                return $addshop;
            } else {
                return false;
            }


        }
    }

    /**
     * @param $request
     * @return mixed
     */

    public function updatashop($request)
    {
//        dd($request);
        if ($request->hasFile('thumbnail')) {
            foreach ($request->file('thumbnail') as $image) {
                $imageName = time() . "-" . $image->extension();
                $imagePath = public_path() . '/images';
                $image->move($imagePath, $imageName);
                $imageDbPath[] = $imageName;
            }
            $data = implode(',', $imageDbPath);
            $shop = $this::find($request->id);
            $shop->name = $request->shop_name;
            $shop->city = $request->city;
            $shop->state = $request->state;
            $shop->address = $request->address;
            $shop->opening_time = $request->opening_time;
            $shop->closing_time = $request->closing_time;
            $shop->logo = $data;

            $updated = $shop->save();
            if ($updated) {
                return $updated;
            }


        }

    }
}
