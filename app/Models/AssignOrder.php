<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AssignOrder
 * @property $rider_id
 * @property $order_id
 * @package App
 */
class AssignOrder extends Model
{
    //

    public function rider(){
        return $this->hasOne(Rider::class,'rider_id');
    }

    public function order(){
        return $this->hasOne(Order::class,'order_id');
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function checkOrderStatus($orderId){
        return $this->select('status')->where('order_id',$orderId)->first();
    }
}

