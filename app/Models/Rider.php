<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Rider extends Model
{
    protected $fillable = ['user_id', 'vehicle_number', 'thumbnail','licence_number','date_of_joining', ];

    //
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function orders(){
        return $this->hasOne(Order::class);
    }
    public function assignOrders(){
        return $this->belongsTo(AssignOrder::class);
    }

    /**
     * @param $formData
     * @return bool
     */
    public function saveRider($formData){
//dd($formData);
        if ($formData->hasFile('thumbnail')) {
            $image = $formData->file('thumbnail');
            $imageName = time() . "-" .$image->extension();
            $imagePath = public_path() . '/images';
            $image->move($imagePath, $imageName);
            $imageDbPath = $imageName;
        }


        $user = User::create([
                "fullname" => $formData['fullname'],
                "email" => $formData['email'],
                "user_contact"=>$formData['user_contact'],
                "address"=>$formData['address'],
                "city"=>$formData['city'],
                "password"=>Hash::make($formData['password']),
                "type"=>'rider'
         ]);
        if ($user){
            $user_id = $user->id;
            $riderData = [
                "user_id" => $user_id,
                "licence_number" => $formData['licence_number'],
                "vehicle_number" => $formData['vehicle_number'],
                "date_of_joining"=>$formData['date_of_joining'],
                "thumbnail"=>$imageDbPath
            ];

            $rider = $this::create($riderData);

            if ($rider){
                /*  Do something */
                return $rider;
            }else{
                /* Do something */
                return false;
            }
        }
        return false;

    }

public function updateUser($updatedformData ,$userId )
{
//    dd($updatedformData['thumbnail']);
    $formData = $updatedformData;
//    dd($formData);

    if ($formData->hasFile('thumbnail')) {
        $image = $formData->file('thumbnail');
        $imageName = time() . "-" .$image->extension();
        $imagePath = public_path() . '/images';
        $image->move($imagePath, $imageName);
        $imageDbPath = $imageName;
    }
//    dd($imageDbPath);

    $user = User::where('id', $userId)->update([
        'fullname' => $updatedformData['fullname'],
        'email' => $updatedformData['email'],
        'password' => $updatedformData['password'],
        'address' => $updatedformData['address'],
        'city' => $updatedformData['city'],

        'user_contact' => $updatedformData['user_contact'],
    ]);
    if ($user) {
        $id = $userId;
        $qry = Rider::where('user_id', $id)->update([
            'vehicle_number' => $updatedformData['vehicle_number'],
            'licence_number' => $updatedformData['licence_number'],
            'date_of_joining' => $updatedformData['date_of_joining'],
            'thumbnail' => $imageDbPath
        ]);
        return $qry;
    }
}
    public function changeStatus($formdata){

        $order_status=Order::find($formdata->id);

        $rider=Rider::where('user_id',auth()->user()->id)->get();


        $order_status->order_status='accepted';
        foreach ($rider as $riders) {
            $assignOrder = new AssignOrder();
            $assignOrder->rider_id = $riders->id;
            $assignOrder->order_id = $formdata->id;
            $assignOrder->order_status = "on the way";
            $assignOrder->save();
        }
//        $assignOrder=AssignOrder::create([
//            'rider_id'=>auth()->user()->id,
//            'order_id'=>$formdata->id,
//        'order_status'=>"On The Way",
//        ]);
        $updated=$order_status->save();
        return $updated;
    }

    public function delivered ($formdata){
        $order_status=Order::find($formdata);
        $order_status->order_status='delivered';
        $updated= $order_status->save();

        $assign_order=AssignOrder::where('order_id',$formdata)->get();
        foreach ($assign_order as $data) {
            $data->order_status = 'delivered';
            $assignUpdated = $data->save();
        }

         return true;

    }

}
Username: Qdn1ejxSST
Password: 8D!kzQE*En1/