<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    //
    protected $fillable = ['order_id','product_id','quantity','amount' ];
    public function order(){
        return $this->belongsTo(Order::class);
    }
    public function products(){
        return $this->hasMany(Product::class);
    }
}
