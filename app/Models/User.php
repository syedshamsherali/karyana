<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class   User extends Authenticatable implements MustVerifyEmail
{


    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'email', 'user_contact', 'password','address' ,'city','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//    public static function create($formData)
//    {
//    }

    public function feedbacks(){
        return $this->hasMany(feedback::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function rider(){
        return $this->hasOne(Rider::class);
    }

    public function saveRider($userTableAttr)
    {
        $userQry = User::create(array_merge($userTableAttr, ['type' => 'rider', 'is_active' => 'yes']));
        return $userQry;
    }
    public function assignOrders(){
        return $this->hasManyThrough('App\Models\AssignOrder','App\Models\Order');
    }

    /**
     * @param $userData
     * @return bool
     */
    public function saveUser($userData){
        $user = $this->Create($userData);
        if ($user){
            return $user;
        }
        return false;
    }


}
