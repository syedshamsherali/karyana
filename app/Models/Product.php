<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable=['name','description','image','price','mfc_date','exp_date' ,'shop_id'];

    public function shop(){
        return $this->belongsTo(Shop::class);
    }
    public function feedback(){
        return $this->hasMany(feedback::class);
    }
    public function orderDetail(){
        return $this->belongsTo(OrderDetail::class);
    }
    public function categories(){
        return $this->belongsToMany(Category::class,'categories_products','product_id','category_id');
    }



    public function saveproduct($formdata){
//        dd($formdata);
        $filename = "";


        if ($formdata->hasFile('fileUpload')){

            foreach ($formdata->file('fileUpload') as $image){
                $imageName = time()."-".$image->getClientOriginalName();
                $imagePath = public_path().'/images';
                $image->move($imagePath,$imageName);
                $imageDbPath[] = $imageName;
            }
            $data  =  implode(',' , $imageDbPath);
//dd($data);
        $addproduct=Product::create([
            'name'=>$formdata->product_name,
            'description'=>$formdata->description,
            'image'=>$data,
            'price'=>$formdata->price,
            'mfc_date'=>$formdata->mfc_date,
            'exp_date'=>$formdata->exp_date,
            'shop_id'=>$formdata->shop_id,
        ]);
        $category = Category::find($formdata->catagories_id);
            $addproduct->categories()->attach($category);
            return $addproduct;
    }
    }


    public function updatesaveproduct($formdata)

    {
//        if ($formdata->hasFile('thumbnail')) {
//            foreach ($formdata->file('thumbnail') as $image) {
//                $imageName = time() . "-" . $image->getClientOriginalName();
//                $imagePath = public_path() . '/images';
//                $image->move($imagePath, $imageName);
//                $imageDbPath[] = $imageName;
//            }
//            $data = implode(',', $imageDbPath);
        if ($formdata->hasFile('fileUpload')){

            foreach ($formdata->file('fileUpload') as $image){
                $imageName = time()."-".$image->getClientOriginalName();
                $imagePath = public_path().'/images';
                $image->move($imagePath,$imageName);
                $imageDbPath[] = $imageName;
            }
            $data  =  implode(',' , $imageDbPath);
//            dd($data);
            $product = Product::find($formdata->id);
            $product->name = $formdata->product_name;
            $product->shop_id = $formdata->shop_id;
            $product->description = $formdata->description;
            $product->price = $formdata->price;
            $product->mfc_date = $formdata->mfc_date;
            $product->exp_date = $formdata->exp_date;
            $product->image = $data;
            $updated = $product->save();
            if ($updated) {
                return $updated;
            }
            $category = Category::find($formdata->catagories_id);
            $product->categories()->attach($category);
            return $product;
        }
    }
}
