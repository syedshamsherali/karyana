<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'shop_id'=>'required',
            'catagories_id' => 'required',
            'product_name' =>'required',
            'description' => 'required',
            'price' => 'required',
            'mfc_date' => 'required',
            'exp_date' => 'required',
//            'thumbnail' => 'required',
        ];
    }
}
