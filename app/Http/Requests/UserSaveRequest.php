<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'fullname' => 'required|min:3|max:30',
            'email' => 'string | email | unique:users',
            'password' => 'required|min:8|max:20',
            'address' => 'required',
            'vehicle_number' => 'required',
            'licence_number' => 'required',
            'city' => 'required',
            'user_contact' => 'required|digits:11|numeric',
            'date_of_joining' => 'required|date',
            'thumbnail' => 'required',
        ];
    }
}
