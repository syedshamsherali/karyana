<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductValidationRequest;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\Shop;
use App\Models\User;
use App\Notifications\OrderPlacement;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

//use mysql_xdevapi\Session;



class
ProductController extends Controller
{
    private $product;


        public function __construct( Product $product , Order $order)
        {

            $this->product=$product;
            $this->order=$order;
        }

//
            public function  quickview(Request $request){

                $id = $request->id;

                $detail = $this->product->find($id);
//                dd($detail);
                return view('products.modal_quickview',compact('detail'));
            }






    public function add_product(){
        $shops=Shop::all();

        $catgory = Category::all();
//        $subcatogries = Category::where('parent_id',!null)->get();
        return view('products.add_product',compact('shops', 'catgory'));
    }

    /**
     * @param Request $
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subcatogries(Request $request){
        $formdata=$request->all();
        $id = $request->id;
        $subcatogries = Category::where('parent_id',$id)->get();

        return json_decode($subcatogries);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(ProductValidationRequest $request){
//    public function create(Request $request){
//        dd($request);
        $formdata=$request;
        $addproduct=$this->product->saveproduct($formdata);

        if($addproduct){
            return redirect()->back()->with('message','product has been added successfully');
        }
    }
    public function product_list(){
        $products=Product::all();
//        dd($products);
        return view('products.product_list',compact('products'));
    }

    public function edit_product($id){
        $shops=Shop::all();
        $cats = Category::all();
        $product=Product::find($id);
        $catgory = Category::all();
            return view('products.product_edit',compact('product','shops','cats','catgory'));
    }

    public function update_product(ProductValidationRequest $request){

        $formdata=$request;
        $update=$this->product->updatesaveproduct($formdata);
//        dd($update);
        if($update){
            return redirect('/product-list')->with('message','Product successfully updated');
        }
    }

    public function delete_product($id){
        $product=Product::find($id);

        $deleted=$product->delete($product);

        if($deleted){
            return redirect('/product-list')->with('message','Product successfully Deleted');
        }

    }


    public function addToCompare($id){

        $product = Product::find($id);
        Session()->push('key', $product);
        return $product->name;
    }
    public function removeItem(Request $request){
//        @dd($request);
//        $request->session()->pull('key' . $request->input('id'));
//        dd($request->id);
//        dd(\Session::get('key'));

        $id=$request->id;
//        @dd([$id]);
//        @dd(pull('key')[$id]);
        unset(\Session::pull('key')[$id]);
//        @dd([$id]);
//        $request->session()->pull('key',$id);

        //        $seesion=\Session::pull('key');
//        \Session::pull('key')[0];
//        $id=$request->id;
//        $session=\Session::pull('key');
//        $value = $id->forget($session);
//        foreach ($seesion as $data) {
//
//            unset($data->$request->id);
//        }
        return "true";
//        $product = session()->get('key', []); // Second argument is a default value
//        unset($product[$request->id]);
//        session()->put('key', $product);
//            return "deleted";
        }
//        session()->pull('products');

//        return redirect()->route("compare")

        ///////////////////////
//        $products = session()->pull('products', []); // Second argument is a default value
//        if(($key = array_search($idToDelete, $products)) !== false) {
//            unset($products[$key]);
//        }
//        session()->put('products', $products);
        /////////////////////////////////////
//        $request->session()->pull('key', 'default');
//            $request->(session()->forget(('key')[$request->id]);
//    }
////    public function addToCompare($id){
////        $products = Product::find($id);
////        \Session::push('key',$products);
////        return $products->name;
////    }
//
//    public function removeItem(Request $request){
////        dd($request->id);
//        unset(\Session::pull('key')[$request->id]);
//    }


//    public function addToCompare(Request $request){
//       $product = Product::find($request->id);
//       $request->session()->put('compare', $product);
//       return $product->name;
//    }

    public function addToCart(Request $request){
        $product = Product::find($request->id);
        $oldCart = \Session::has('cart')? \Session::get('cart'):null;
        $cart = new Cart($oldCart);
//dd($oldCart);
        $data= $cart->addProduct($product, $product->id);
//        dd($data);
        $request->session()->put('cart',$cart);
        return $product->name;
    }
    public function cartView(){
        $latestProduct=DB::table('products')
            ->latest()->first();
//        dd($latestProduct);
        if(!\Session::has('cart')){

//            return view('cart.cart',compact('latestProduct'));
            return view('cart.cart');
        }

        $oldCart = \Session::get('cart');
        $cart = new Cart($oldCart);
//        dd($cart);
        return view('cart.cart',['products'=>$cart->contents, 'totalPrice'=>$cart->totalPrice,'shippingRate'=>$cart->shippingRate]);
    }
    public function removeCart(Request $request){

        if ($request->addQuantity == 'true') {
            $qty = \Session::get('cart')->contents[$request->id]['qty'];
            \Session::get('cart')->contents[$request->id]['price'] = ($request->price * $request->qty);
            \Session::get('cart')->totalQty = \Session::get('cart')->totalQty + ($request->qty - $qty);
            \Session::get('cart')->totalPrice = (\Session::get('cart')->totalPrice - $request->totalPrice) + ($request->price * $request->qty);
            \Session::get('cart')->contents[$request->id]['qty'] =  $request->qty;
            return "true";
        }
        else {
            unset(\Session::get('cart')->contents[$request->id]);

            \Session::get('cart')->totalQty = \Session::get('cart')->totalQty - $request->qty;
//                @dd(\Session::get('cart');
            \Session::get('cart')->totalPrice = \Session::get('cart')->totalPrice - ($request->price * $request->qty);
            return "true";
        }

    }
    public function checkOut(){

        if(!\Session::has('cart')){

            return view('products.check_out');
        }
        $oldCart = \Session::get('cart');
        $cart = new Cart($oldCart);
        return view('products.check_out',['products'=>$cart->contents, 'totalPrice'=>$cart->totalPrice,'shippingRate'=>$cart->shippingRate]);
         }

    public function search(Request $request){

            $search = $request->search;
//            $data = Product::where('name' , 'like' , '%' . $search . '%')->orderBy('price','asc')->get();
            $data = Product::where('name' , 'like' , '%' . $search . '%')->orderBy('price','asc')->paginate(6);
        if (!$data->isEmpty()){
            return view('products.searched_Product',compact('data'));
        }else{
            return view('products.product_Notfound');
        }
    }
    public function find(Request $request){
           $products=Product::all();
        $latestProduct=DB::table('products')->latest()->first();
//            dd($latestProduct);
        $find = $request->search;
        $data = Product::where('name' , 'like' , '%' . $find . '%')->paginate(6);
        if ($data->isEmpty()){
            return view('products.product_Notfound');
        }else {
            return view('products.searched_Product',compact('data','latestProduct','products'));
        }
    }
    public function viewdetails(Request $request){
//        dd($request->id);
          $product =  Product::all()->where('id',$request->id);
        return $product;
    }
    public function registerCheckout(){

        return view('products.register_checkout');
    }


    ///////SELF CREATE CONTROLLERS
    public function logincheck(){
        return view('checkout-subfiles.checkout-login');
    }
    public function shipping(Request $request){
        return view('checkout-subfiles.shipping-method');
    }
    public function shippingAddress(Request $request){
//        dd($request);
        return view('checkout-subfiles.shipping-address');
    }
    public function paymentAddress(Request $request){
//        dd($request);
        return view('checkout-subfiles.payment-address');
    }
    public function paymentMethod(Request $request){
//        dd($request);
        return view('checkout-subfiles.payment-method');
    }
    public function orderConfirm(Request $request){
//        dd($request);
        return "fghfdgjghjg";
    }
    public function paymentAddressSave(Request $request){
//        dd($request);
        $redirect = array([
            'redirect' => 'http://127.0.0.1:8000/check-out'
        ]);
        return json_encode($redirect) ;
    }

/////////////////////SELF CONTROLLER END HERE




    public function comparison(){
        return view('products.compare_Product');

    }

}
