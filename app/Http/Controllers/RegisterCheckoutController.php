<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterCheckoutController extends Controller
{
  public function register(Request $request){
      User::create([
          'fullname' => $request['fullname'],
          'email' => $request['email'],
          'user_contact'=> $request['user_contact'],
          'password' => Hash::make($request['password']),
          'city' => $request['city'],
          'address' => $request['address'],
      ]);
      return "data saved";
  }
}
