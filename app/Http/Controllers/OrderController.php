<?php

namespace App\Http\Controllers;
use App\Models\AssignOrder;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Rider;
use App\Models\Shop;
use App\Models\User;
use App\Notifications\OrderPlacement;
use http\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    private $order;

    public function __construct(Order $order)
    {
        $this->order=$order;
    }

    public function testview(){

        return view('test');


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Stripe\Exception\ApiErrorException
     */


//    public function charge(Request $request)
//    {
//        $user=Auth::user();
////dd($request);
//        if(\Session::has('cart'))
//        {
//            $cart=\Session::get('cart');
//
//            // Set your secret key: remember to change this to your live secret key in production
//// See your keys here: https://dashboard.stripe.com/account/apikeys
//            \Stripe\Stripe::setApiKey('sk_test_w24eEz8VYlWjjOKhxqaqXNMf009OjdxTNO');
//
//            $charge = \Stripe\Charge::create([
//            'amount' => $cart->totalPrice,
//            'currency' => 'usd',
//            'source' => $request->stripeToken,
//            'receipt_email' => Auth::User()->email,
//
//        ]);
//            $saveorder =Order::create([
//                'user_id'=>Auth::User()->id,
//                'total_ammount'=>$charge->amount/100,
//                'order_status'=>'pending',
//                'transaction_number'=>$charge->stripeToken
//
//            ]);
//
//        }
////        dd($request);
////        dd($request->id);
//        if($saveorder){
//            return redirect()->back();
//        }}

        public function OrderPlacement(){
        $cart = \Session::get('cart');
        $addorder=$this->order->saveorder($cart);
//        dd($cart);
        $user=User::all()->where('type','admin');
        foreach ($user as $u)
        {
            $u->notify(new OrderPlacement());

        }
        \Session::forget('cart');
        return redirect('home');
    }

    public function approve_order(Request $request){
//
//        dd($request);
//        $orders = Order::all()->where('Status','pending')->toArray();
        $orders = Order::all();


        $orders = Order::where('order_status','pending')->get();


//        for ($i=0;$i<count($orders);$i++){
//            $orderDetails[]=OrderDetail::where('order_id',$orders[$i]->id)->get();
//        }

//        $orderDetails=OrderDetail::where('order_id',$orders[0]->id)->get();

//        $orderDetails=OrderDetail::all();

//        $test = $orderDetails[2]->product_id;

//        dd($test['name']);
if(isset($orders)){
//    dump('im here');
    return view('orders.approval', compact('orders'));
}
else{
//    dd('im in 404');
    return view('404');
}
    }
    /////////////////
    public function getOrderDetails(Request $request){
        $formdata=$request->id;
//        dd($formdata);
        $orderDetail=OrderDetail::where('order_id',$formdata)->get();
        foreach ($orderDetail as $data){
            $data->order=Shop::find(Product::find($data->product_id)->shop_id)->name;
            $data->product_id = Product::where('id', $data->product_id)->pluck('name');
        }
        return response($orderDetail);
    }
    /// //////////////

    public function assignRiderNoti(Request $request){
        $formdata=$request;

        $update=$this->order->changeStatus($formdata);
        if($update){
            $user=User::all()->where('type','rider');
            foreach ($user as $u)
            {
                $u->notify(new OrderPlacement());

            }
            return redirect(route('approve_order'))->with('message','Order Successfully Approved And Sent To Riders');
        }

    }

    public function discardOrder(Request $request){
        $formdata=$request;
        $discard=$this->order->discardOrder($formdata);
        if($discard){
            return redirect(route('approve_order'))->with('message','Order Successfully Discarded');

        }
    }

    public function orderList(){
            $orders=Order::all();
            return view('orders.order_list',compact('orders'));
    }
    public function trackOrder(){
            $userId=\auth()->user()->id;

            $totalOrders=Order::where([['user_id','=',$userId],['order_status','=','accepted'],])->get();



            $bool = 1;
            foreach ($totalOrders as $data){
                $delivery[]=AssignOrder::where([['order_id','=',$data->id],['order_status','=','on the way'],])->first();
                $bool = 0;
            }

//            if ($bool == 0) {
                return view('tracking.track_order', compact('delivery','bool'));
//            }else{
//                return view('tracking.track_order', compact('delivery','bool'));

//            }
            }

    public function getOrderRiderDetails(Request $request){

        $formdata=$request->id;
//                        dd($formdata);

//        $riderId=DB::table('assign_orders')->select('rider_id')->where('order_id',$formdata)->first();
        $riderId=AssignOrder::where('order_id',$formdata)->first();
        $riderDetail=Rider::find($riderId->rider_id);

//        dd($riderDetail);

        //        foreach ($riderDetail as $rider){
//            $rider= $rider->User['fullname'];
//        }

        $orderDetail=OrderDetail::where('order_id',$formdata)->get();
        foreach ($orderDetail as $data){
            $data->order=Shop::find(Product::find($data->product_id)->shop_id)->name;
            $data->product_id = Product::where('id', $data->product_id)->pluck('name');
            $data->rider=$riderDetail->User['fullname'];
            $data->vehicle=$riderDetail->vehicle_number;
            $data->contact=$riderDetail->User['user_contact'];
        }





        return response($orderDetail);


    }





}
