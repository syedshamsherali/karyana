<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Feedback;
use App\Models\Product;
use App\Models\Shop;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Transaction;
use App\Support\Collection;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware(['auth' => 'verified']);
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {
        $shops=Shop::all();
        $products=Product::all();
        return view('home',compact('shops','products'));

    }

    public function shopDetail($id)
    {

        $latestProduct=DB::table('products')->latest()->first();
        $shop=Shop::find($id);
        $products=Product::where('shop_id',$id)->paginate(5);

        return view('shops.shopDetail',compact('products','shop','latestProduct'));

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * `
     */

    public function productDetail($id){
//        if (Auth::user()) {
//            $user_id = Auth::user()->id;
//        }
//        else{
//            return redirect()->route('login');
//        }
//        $user_id = Auth::user()->id;
        $product=Product::find($id);
        $rating = Feedback::all()->where('product_id',$id);
        $relatedProduct=Product::all();
        $array = $product->image;
        $parray = $arr = explode(',', $array);
            return view('products.productDetail',compact('parray','product','relatedProduct','rating'));


//        return view('products.productDetail',compact('product','relatedProduct'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function test($id){

        $catg=Category::find($id);
        $cat=$catg->products()->paginate(6);
//        dd($cat);
        $latestProduct=DB::table('products')
            ->latest()->first();
        return view('test.test',compact('cat','catg','latestProduct'));
    }

    public function compareProduct(){
        return view('products.productCompare');
    }

}
