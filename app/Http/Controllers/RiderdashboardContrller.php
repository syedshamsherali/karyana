<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSaveRequest;
use App\Models\AssignOrder;
use App\Models\Order;
use App\Models\Rider;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RiderdashboardContrller extends Controller
{
    //
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Rider,s Profile on Rider-dash
     *
     */

    public function rider_profile(){
//        dd(Auth::user()->id);
        $user=Auth::user()->id;
        $userlist=User::find($user);
        return view('rider.rider-profile',compact('userlist'));

    }
//-----------------------------------------------------------------------------------

            public function editprofile(Request $request)
            {
                $data=$request->all();
//                dd($data);
                {
                    $user = User::where('id', Auth::user()->id)->update([
                        'address' => $request['address'],
                        'city' => $request['city'],
                        'user_contact' => $request['user_contact'],
                    ]);
                    if ($user) {
                        return redirect()->back()->withMessage('Updated successfully');

                    }
//        dd(Auth::user()->id);
                }


            }


//    ------------------------------------------------------------------------------
    public function rider_edit_profile(){
        $user=Auth::user()->id;
        $userlist=User::find($user);
        return view('rider.edit-riderprofile',compact('userlist'));
    }



   public function order_history(){

       $userId = auth()->user()->id;

       $rider = Rider::where('user_id',$userId)->get();
//       dd($rider);

       foreach ($rider as $riders) {
           $assignOrder = AssignOrder::where('rider_id', $riders->id)->get();
       }


//       dd($assignOrder);


       return view('rider.rider-orderhistory',compact('assignOrder'));
   }
}
