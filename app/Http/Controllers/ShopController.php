<?php

namespace App\Http\Controllers;
use App\Http\Requests\ShopValidationRequest;
use App\Models\Feedback;
use App\Models\Order;
use App\Models\Post;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class   ShopController extends Controller
{
    private $shop;
    /**
     * ShopController constructor.
     * @param Shop $shop
     */
public function __construct(Shop $shop)
{
        $this->shop = $shop;
}
    public function view_shop(){

            return view('shops.add_shop');
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(ShopValidationRequest $request){
        $shopdata= $request;
         $saveshops=$this->shop->saveshop($shopdata);
        if($saveshops){
            return redirect('/add-shop')->with('message','Shop added Successfully');
        }
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function shop_list(){
        $shops=Shop::all();
        return view('shops.shop_list',compact('shops'));
    }
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function shop_edit($id){
        $shops=Shop::find($id);
        return view('shops.shop_edit',compact('shops'));
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function shop_update(ShopValidationRequest $request){
        $saveshops=$this->shop->updatashop($request);
        if($saveshops){
                    return redirect('/shop-list')->with('message','post successfully updated');
            }

    }
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function shop_delete($id){
        $shop=Shop::find($id);
        $delete=$shop->delete($shop);
        if($delete){
//            toast('Shop Deleted Successfully ','warning');
            return redirect('/shop-list')->with('message','Shop successfully deleted ');

        }
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function admindashboard(){
        $status = 'accepted';
        $data = Order::all()->where('order_status','=',$status);
       $sum = $data->sum('total_ammount');
//       dd($sum);
        $count_product = Product::all()->count();
        $count_shop = Shop::all()->count();
        $count_order = Order::all()->where('order_status','pending')->count();
        $arr = [
            'total_product' => $count_product,
            'total_shop' => $count_shop,
            'count_order' => $count_order,
        ];
        return view('dashboards.admin_dashboard',compact('sum','arr'));
    }
}
