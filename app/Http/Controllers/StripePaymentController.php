<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Stripe;
use App\Models\Cart;

class StripePaymentController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        return view('stripe');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws Stripe\Exception\ApiErrorException
     */
    public function stripePost(Request $request)
    {
//        $charge=;
        $transaction_number=$request->stripeToken;
        if(\Session::has('cart')) {
            $cart = \Session::get('cart');

            Stripe\Stripe::setApiKey('sk_test_w24eEz8VYlWjjOKhxqaqXNMf009OjdxTNO');
            $charge=Stripe\Charge::create([


                "amount" => $cart->totalPrice*100+100,
                "currency" => "pkr",
                "source" => $request->stripeToken,
                'receipt_email' => Auth::User()->email,
                "description" => "Test payment from Karyana.Pk.",



            ]);
            if ($charge){
//                dd($charge);
            $savepayments=Order::create([
            'user_id'=>Auth::User()->id,
            'total_ammount'=>$cart->totalPrice + 100,
            'order_status'=>'pending',
            'payment_status'=>'paid',
            'transaction_number'=>$transaction_number,
            ]);

//dd($savepayments->id);
                $contents=$cart->contents;

                foreach ($contents as $data){
                    $orderDetail = new OrderDetail();
                    $orderDetail->order_id =$savepayments->id;
                    $orderDetail->product_id=$data['product']->id;
                    $orderDetail->quantity=$data['qty'];
                    $orderDetail->amount=$data['price'];

                    $orderDetail->save();

                }
            }
else {
    $saveorder = Order::create([
        'user_id' => Auth::User()->id,
        'total_ammount' => $charge->amount ,
        'order_status' => 'pending',
        'payment_status' => 'cash_on_delivery',
        'transaction_number' => $transaction_number,

    ]);

}

        }

        Session::flash('success', 'Payment successful!');

//        return redirect()->route('OrderPlacement');
        \Session::forget('cart');
        return redirect('home');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function StripeOrderPlacement(){
        $cart = \Session::get('cart');
//        $addorder=$this->order->saveorder($cart);
//        dd($cart);
//        $user=User::find(1);
//        $user->notify(new OrderPlacement);
        \Session::forget('cart');
        return redirect('home');
    }


}
