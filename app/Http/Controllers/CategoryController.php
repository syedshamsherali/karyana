<?php

namespace App\Http\Controllers;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
//    private $category;
//    public function __construct(Category $category)
//    {
//        $this->category = $category;
//    }

    public function manageCategory()
    {
        $allCategories = Category::all();
        return view('categories.add_category',compact('allCategories'));
    }
    public function add_category(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $input = $request->all();
        $input['parent_id'] = empty($input['parent_id']) ? null : $input['parent_id'];

        Category::create($input);
        return back()->with('success', 'New Category added successfully.');
    }
    public function category_table(){
//        dd('hamed');
        $category = Category::where('parent_id', '=', NULL)->get();
        return view('categories.category_table',compact('category'));
    }
    public function deleteCategory(Request $request){
        $categoryId = Category::find($request->id);
//        dd($categoryId);
        $categoryId->delete();
        return "data has been deleted";
    }
    public function createChild(Request $request){
        if ($request->parent_id) {
            Category::create(array('parent_id' => $request->parent_id, 'name' => $request->name));
        }else{
            Category::create(array('name' => $request->name));
        }
        return "data has been created";
    }
    public function updateCategory(Request $request){
             Category::where('id',$request->id)->update(array('name'=>$request->name));
        return "data has been updated";
    }

}
