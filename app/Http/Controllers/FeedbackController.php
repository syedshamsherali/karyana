<?php

namespace App\Http\Controllers;


use App\Models\Category;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
//    protected $product_id;


    public function productDetail(){
        if (\auth()->user()){
            return view('productDetail');
        }
        else
        {
            return redirect('login');

        }
    }

    public function post_comment(Request $request)
    {
//        dd('up');
//        if(Auth::user()) {
//            dd('if');
            $data = array('user_id' => $request->user()->id, 'product_id' => $request->product_id, 'message' => $request->message, 'rating' => $request->rating);
            $created = Feedback::create($data);
//            $rating = Feedback::all()->where('rating', '4');
//        }else{
////            dd('else');
//            return view('auth.login');
//        }
//        return redirect()->route('login');
//        return view('auth.login');
    }

    public function fetchComment(Request $request)
    {

        $product_id = $request->product_id;

        $feedback = Feedback::where('product_id',$product_id)->get();
        return json_encode($feedback);
    }
    public function deleteComment(Request $request){
        $commentId = Feedback::find($request->id);
//        dd(commentId);
        $commentId->delete();
        return "data has been deleted";
    }
}
