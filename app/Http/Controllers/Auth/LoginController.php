<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
  protected $redirectTo = '';
  protected function redirectTo(){
      if(auth::user()['type'] == 'admin'){
          return '/admin-dashboard';
      }
      if(auth::user()['type'] == 'rider'){
          return '/rider-profile';
      }
      if(auth::user()['type'] == 'customer'){
          return '/home';
      }
  }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
//    public function redirectToProvider()
//    {
////        return Socialite::driver('facebook')->redirect();
//    }
//    /**
//     * Return a callback method from facebook api.
//     *
//     * @return callback URL from facebook
//     */
//    public function callback()
//    {
//        $userSocial = Socialite::driver('facebook')->user();
//        //return $userSocial;
//        $finduser = User::where('facebook_id', $userSocial->id)->first();
//        if($finduser){
//            Auth::login($finduser);
//            return Redirect::to('/');
//        }else{
//            $new_user = User::create([
//
//                'fullname' => $userSocial['fullname'],
//                'email' => $userSocial['email'],
//                'user_contact'=> $userSocial['user_contact'],
//                'password' => Hash::make($userSocial['password']),
//                'city' => $userSocial['city'],
//                'address' => $userSocial['address'],
//
//            ]);
//            Auth::login($new_user);
////            return redirect()->back();
////        }
//    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleFacebookCallback()
    {
        $user = Socialite::driver('facebook')->user();

        $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
        $password = substr($random, 0, 5);
        $getUser = User::where('email', $user->email)->first();
        if (!$getUser) {
            $authUser = User::create([
                'fullname' => $user->name,
                'email' => $user->email,
                'address' => null,
                'city'=> null,
                'user_contact' => null,
                'type' => 'customer',
                'is_active' => '1',
                'password' => Hash::make($password),
            ]);

            Auth::login($authUser);

            return redirect()->route('home');

        }
        else{
            Auth::login($getUser);
            return redirect()->route('facebook');
        }

        // $user->token;

    }




}
