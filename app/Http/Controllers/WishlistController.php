<?php

namespace App\Http\Controllers;

use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Contracts\User;

class WishlistController extends Controller
{
    private $wish;
    public function __construct(Wishlist $wish)
    {
        $this->wish = $wish;

    }

    public function wishlistview() {
        $id = Auth::user()->id;
        $allwhishs =$this->wish->UsersWishes($id);
//           dd($allwhishs);
        return view('wishlists.wishlist', compact('allwhishs'));
    }
    public function savewish($id){
        $curresntuser = Auth::user()->id;
        $addtowish=$this->wish->savewish($id,$curresntuser);
    }


    public function wishdelete($id)
    {
        $wish = Wishlist::find($id);
        $delete = $wish->delete($wish);
        if ($delete) {
            return redirect()->back()->with('message', 'Wish successfully deleted');
        }

    }






}
