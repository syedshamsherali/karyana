<?php

namespace App\Http\Controllers;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Rider;
use App\Models\Shop;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserSaveRequest;
//use Illuminate\Support\Facades\Auth;

class RiderController extends Controller
{
    private $rider, $user;

    /**
     * RiderController constructor.
     * @param $rider
     * @param $user
     */
    public function __construct(Rider $rider, User $user)
    {
        $this->rider = $rider;
        $this->user = $user;
    }

    public function add_rider(){
        return view('rider.add_rider');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function rider_list(){
                $userlist=Rider::all();

        return view('rider.rider_list',compact('userlist'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */


    public function saveRider(UserSaveRequest $request)
    {
//        dd($request);
        $formData = $request;

        $savedRider = $this->rider->saveRider($formData);
        if ($savedRider) {
            return redirect()->route('add_rider')->withMessage('Rider Added successfully');
        } else {
            return redirect()->route('add_rider');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

              public function rider_delete($id)
              {
                  $rider = User::find($id);
                  $delete = $rider->delete($rider);
                  if ($delete) {
                      return redirect()->route('rider_list')->with('message', 'Rider successfully deleted');
                  }

              }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

              public function rider_edit($id){
                  $riders=User::find($id);
            return view('rider.rider_edit',compact('riders'));
              }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
                    public function update_rider(Request $request ){
//                      $formdata =$request;
//                      dd($request);
                      $updatedformData = $request->except('id');
                      $userId = $request->id;

                      $updateUser = $this->rider->updateUser($request ,$userId);

                      if ($updateUser) {
                     return redirect()->route('rider_list')->withMessage('Updated successfully');

                      } else {
                          return redirect()->route('rider_list');
                      }


                  }



                  public function Tracking(){

                        return view('rider.rider_tracking');

                  }

                  public function accept_order(){
                      $orders = Order::where('order_status','approved')->get();
                      return view('orders.acceptance',compact('orders'));
                  }

                    public function getOrderDetailsRider(Request $request){
                        $formdata=$request->id;
//                        dd($formdata);
                        $orderDetail=OrderDetail::where('order_id',$formdata)->get();
                        foreach ($orderDetail as $data){
                            $data->order=Shop::find(Product::find($data->product_id)->shop_id)->name;
                            $data->product_id = Product::where('id', $data->product_id)->pluck('name');
                        }
                        return response($orderDetail);
                    }

                    public function riderAcceptOrder(Request $request){
                        $formdata=$request;
                        $update=$this->rider->changeStatus($formdata);
                        if($update){
                            return redirect(route('accept_order'))->with('message','Order Accepted , Happy Riding!!');
                        }
                    }

                    public function delivered (Request $request){
                        $formdata=$request->id;
                        $update=$this->rider->delivered($formdata);

                        if($update){
                            return redirect(route('order_history'))->with('message','Order Delivered Successfully');
                        }
                    }

}
