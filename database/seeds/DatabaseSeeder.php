<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ShopsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(CategoriesProductsSeeder::class);
        $this->call(RiderTableSeeder::class);
        $this->call(OrderTableSeeder::class);
//        $this->call(Order_DetailsTableSeeder::class);

    }
}
