<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class Order_DetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'order_id'=>1,
            'product_id'=>'7Up Soft Drink 500ml',
            'quantity'=>'7Up Soft Drink 500ml carbonated Drink',
            'amount'=>'1573746129_.jpeg',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
    }
}
