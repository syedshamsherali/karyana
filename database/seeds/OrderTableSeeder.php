<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('orders')->insert([
            'user_id'=> '3',
            'total_ammount'=> '100',
            'order_status'=> 'pending',
            'payment_status'=> 'cash_on_delivery',
            'transaction_number'=> '123456789',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('orders')->insert([
            'user_id'=> '3',
            'total_ammount'=> '100',
            'order_status'=> 'pending',
            'payment_status'=> 'cash_on_delivery',
            'transaction_number'=> '123456789',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('orders')->insert([
            'user_id'=> '3',
            'total_ammount'=> '100',
            'order_status'=> 'pending',
            'payment_status'=> 'paid',
            'transaction_number'=> '123456789',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('orders')->insert([
            'user_id'=> '3',
            'total_ammount'=> '100',
            'order_status'=> 'pending',
            'payment_status'=> 'paid',
            'transaction_number'=> '123456789',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);
//        DB::table('orders')->insert([
//            'user_id'=> '3',
//            'total_ammount'=> '100',
//            'Status'=> 'pending',
//            'transaction_number'=> '123456789',
//            'created_at'=>Carbon::now(),
//            'updated_at'=>Carbon::now(),
//        ]);

    }
}
