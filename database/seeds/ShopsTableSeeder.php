<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('shops')->insert([
            'name'=> 'Alfatah',
            'city'=> 'Lahore',
            'state'=> 'punjab',
            'address'=> 'Shadman 1 Shadman, Lahore, Punjab 54000',
            'opening_time'=> '09:00:00',
            'closing_time'=> '23:00:00',
            'logo'=> '1573712078_.jpg',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('shops')->insert([
            'name'=> 'Gourmet',
            'city'=> 'Lahore',
            'state'=> 'punjab',
            'address'=> 'Shadman 1 Shadman, Lahore, Punjab 54000',
            'opening_time'=> '09:00:00',
            'closing_time'=> '23:00:00',
            'logo'=> '1573712099_.jpeg',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('shops')->insert([
            'name'=> 'Hyperstar',
            'city'=> 'Lahore',
            'state'=> 'punjab',
            'address'=> 'Shadman 1 Shadman, Lahore, Punjab 54000',
            'opening_time'=> '09:00:00',
            'closing_time'=> '23:00:00',
            'logo'=> '1573711923_.jpg',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('shops')->insert([
            'name'=> 'Imtiaz General Store',
            'city'=> 'Lahore',
            'state'=> 'punjab',
            'address'=> 'Shadman 1 Shadman, Lahore, Punjab 54000',
            'opening_time'=> '09:00:00',
            'closing_time'=> '23:00:00',
            'logo'=> '1573711956_.jpg',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('shops')->insert([
            'name'=> 'Metro Cash & Carry',
            'city'=> 'Lahore',
            'state'=> 'punjab',
            'address'=> 'Shadman 1 Shadman, Lahore, Punjab 54000',
            'opening_time'=> '09:00:00',
            'closing_time'=> '23:00:00',
            'logo'=> '1573711983_.png',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('shops')->insert([
            'name'=> 'Swera Departmental Store',
            'city'=> 'Lahore',
            'state'=> 'punjab',
            'address'=> 'Shadman 1 Shadman, Lahore, Punjab 54000',
            'opening_time'=> '09:00:00',
            'closing_time'=> '23:00:00',
            'logo'=> '1573712014_.jpg',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
    }
}
