<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RiderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('riders')->insert([
            'user_id'=> '3',
            'is_available'=> '1',
            'vehicle_number'=> 'GAM12345',
            'thumbnail'=> '1578634860-png',
            'licence_number'=> 'R521M581uA0',
            'date_of_joining'=> '2020-01-10',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
    }
}
