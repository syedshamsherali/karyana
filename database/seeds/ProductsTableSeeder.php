<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'7Up Soft Drink 500ml',
                'description'=>'7Up Soft Drink 500ml carbonated Drink',
                'image'=>'1573746129_.jpeg',
                'price'=>55,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Coca Cola Zero Cherry Flavour 330ml',
                'description'=>'Coca Cola Zero Cherry Flavour 330ml carbonated drinks',
                'image'=>'1573746203_.jpeg',
                'price'=>45,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Coffee Mate',
                'description'=>'Coffee Mate Coffee',
                'image'=>'1573746267_.png',
                'price'=>200,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'illy deca Decaffeinated',
                'description'=>'illy deca Decaffeinated coffee',
                'image'=>'1573746322_.png',
                'price'=>300,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Almarai Joosy Life Juice Apple  250ml',
                'description'=>'Almarai Joosy Life Juice Apple  250ml non carbonated drink',
                'image'=>'1573746466_.jpeg',
                'price'=>130,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Copella Apple Juice 900ml',
                'description'=>'Copella Apple Juice 900ml non carbonated Drink',
                'image'=>'1573746517_.jpeg',
                'price'=>225,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Active O2',
                'description'=>'Active O2 Water',
                'image'=>'1573746554_.jpeg',
                'price'=>95,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Active O2 Flavor Water Apple 500ml',
                'description'=>'Active O2 Flavor Water Apple 500ml water',
                'image'=>'1573746626_.jpeg',
                'price'=>120,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Aptamil Follow On Milk 800 Grams',
                'description'=>'Aptamil Follow On Milk 6-12 Months Step 2 - 800 Grams',
                'image'=>'1573746693_.png',
                'price'=>300,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Aptamil Hungry Infant Milk 800 Grams',
                'description'=>'Aptamil Hungry Infant Milk 800 Grams',
                'image'=>'1573746770_.jpeg',
                'price'=>355,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Kellogg\'s Almond Honey Corn Flakes 300g',
                'description'=>'Kellogg\'s Almond Honey Corn Flakes 300g',
                'image'=>'1573746818_.jpeg',
                'price'=>500,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Mitchell\'s Apricot Jam 340g',
                'description'=>'Mitchell\'s Apricot Jam 340g',
                'image'=>'1573746843_.jpeg',
                'price'=>166,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Alpro Original Soya Sweeted Regular Milk 1 Ltr',
                'description'=>'Alpro Original Soya Sweeted Regular Milk 1 Ltr',
                'image'=>'1573746879_.jpeg',
                'price'=>300,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Kdd Flavor Milk Chocolate 180ml',
                'description'=>'Kdd Flavor Milk Chocolate 180ml',
                'image'=>'1573746907_.jpeg',
                'price'=>85,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Heath Toffee Bits Milk Chocolate 226g',
                'description'=>'Heath Toffee Bits Milk Chocolate 226g',
                'image'=>'1573746946_.jpeg',
                'price'=>95,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>1,
                'name'=>'Hershey\'s Kisses Special Dark Mildly Sweet',
                'description'=>'Hershey\'s Kisses Special Dark Mildly Sweet Chocolate Classic Bag 340 Grams',
                'image'=>'1573746985_.png',
                'price'=>485,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);

        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Coca cola zero sugar vanilla 330ml',
                'description'=>'Coca cola zero sugar vanilla 330ml',
                'image'=>'1573747627_.jpeg',
                'price'=>155,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Ka sparkling black grape soda 330ml',
                'description'=>'Ka sparkling black grape soda 330ml',
                'image'=>'1573747668_.jpeg',
                'price'=>140,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Lavazza Espresso Cremoso',
                'description'=>'Lavazza Espresso Cremoso',
                'image'=>'1573747698_.jpeg',
                'price'=>400,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Nescafe Gold Blend',
                'description'=>'Nescafe Gold Blend',
                'image'=>'1573747843_.png',
                'price'=>250,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Jaffa Gold Tropical Juice Drink',
                'description'=>'Jaffa Gold Tropical Juice Drink',
                'image'=>'1573747889_.jpeg',
                'price'=>300,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Masafi Mango Juice 200ml',
                'description'=>'Masafi Mango Juice 200ml',
                'image'=>'1573747925_.jpeg',
                'price'=>120,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Active o2 flavor water orange',
                'description'=>'Active o2 flavor water orange',
                'image'=>'1573748047_.jpeg',
                'price'=>150,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Active O2 Flavour water Raspberry',
                'description'=>'Active O2 Flavour water Raspberry',
                'image'=>'1573748078_.jpeg',
                'price'=>120,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Enfamil Premium Infant Formula Powder 598g',
                'description'=>'Enfamil Premium Infant Formula Powder 598g',
                'image'=>'1573748116_.jpeg',
                'price'=>550,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Gerber Baby Pudding Organic Banana',
                'description'=>'Gerber Baby Pudding Organic Banana Sitter Food 113 Grams',
                'image'=>'1573748161_.jpeg',
                'price'=>500,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Mitchell\'s Lemon Ginger Marmalade 450g',
                'description'=>'Mitchell\'s Lemon Ginger Marmalade 450g',
                'image'=>'1573748205_.jpeg',
                'price'=>650,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Mitchell\'s Strawberry Jelly 450g',
                'description'=>'Mitchell\'s Strawberry Jelly 450g',
                'image'=>'1573748230_.jpeg',
                'price'=>130,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Kraft Philadelphia Original 180 Grams',
                'description'=>'Kraft Philadelphia Original 180 Grams',
                'image'=>'1573748263_.jpeg',
                'price'=>400,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Lurpak Spreadable Olive Oil Lighter 250 Grams',
                'description'=>'Lurpak Spreadable Olive Oil Lighter 250 Grams',
                'image'=>'1573748287_.jpeg',
                'price'=>400,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Kenny\'s All Natural Low Fat Black',
                'description'=>'Kenny\'s All Natural Low Fat Black Licorice Twists 141g',
                'image'=>'1573748336_.jpeg',
                'price'=>654,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>2,
                'name'=>'Limitless Protein Bar Chocolate Crunch 30g',
                'description'=>'Limitless Protein Bar Chocolate Crunch 30g',
                'image'=>'1573748366_.jpeg',
                'price'=>233,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);

        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Mirinda Soft Drink 500ml',
                'description'=>'Mirinda Soft Drink 500ml',
                'image'=>'1573748603_.jpeg',
                'price'=>85,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Pepsi Drink Regular 300ml',
                'description'=>'Pepsi Drink Regular 300ml',
                'image'=>'1573748641_.jpeg',
                'price'=>95,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Nescafe Instant Arabiana',
                'description'=>'Nescafe Instant Arabiana',
                'image'=>'1573748680_.jpeg',
                'price'=>300,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Rio Brazilian Turkish',
                'description'=>'Rio Brazilian Turkish',
                'image'=>'1573748750_.jpeg',
                'price'=>500,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Masafi Tropical Juice 200ml',
                'description'=>'Masafi Tropical Juice 200ml',
                'image'=>'1573748799_.jpeg',
                'price'=>60,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Nestle Fruita Vitals Pomegrantate',
                'description'=>'Nestle Fruita Vitals Pomegrantate',
                'image'=>'1573748831_.jpeg',
                'price'=>85,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Aqua coconut Water 1liter',
                'description'=>'Aqua coconut Water 1liter',
                'image'=>'1573748876_.png',
                'price'=>350,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Dayfresh Milk Loctose Free 200ml',
                'description'=>'Dayfresh Milk Loctose Free 200ml',
                'image'=>'1573748913_.jpeg',
                'price'=>250,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Heinz Egg Custard with Rice 6 x 120g',
                'description'=>'Heinz Egg Custard with Rice 6 x 120g',
                'image'=>'1573748951_.jpeg',
                'price'=>300,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Nestle Cerelac Beras Rice 225 Grams',
                'description'=>'Nestle Cerelac Beras Rice 225 Grams',
                'image'=>'1573748988_.jpeg',
                'price'=>300,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Nutella Hazelnut Spread with Cocoa 350 Grams',
                'description'=>'Nutella Hazelnut Spread with Cocoa 350 Grams',
                'image'=>'1573749015_.png',
                'price'=>455,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Shezan Mango Jam 440g',
                'description'=>'Shezan Mango Jam 440g',
                'image'=>'1573749037_.jpeg',
                'price'=>130,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Nestle Every Day Mix Tea Milk 900g',
                'description'=>'Nestle Every Day Mix Tea Milk 900g',
                'image'=>'1573749062_.jpeg',
                'price'=>320,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Nestle Everyday Lite 250 Grams',
                'description'=>'Nestle Everyday Lite 250 Grams',
                'image'=>'1573749088_.jpeg',
                'price'=>650,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Mabel Candy Mix Fruit Flavoured Candies 95g',
                'description'=>'Mabel Candy Mix Fruit Flavoured Candies 95g',
                'image'=>'1573749195_.jpeg',
                'price'=>350,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>3,
                'name'=>'Mentos Menthol Gum 45s 63g',
                'description'=>'Mentos Menthol Gum 45s 63g',
                'image'=>'1573749228_.jpeg',
                'price'=>230,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);

        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Pespsi-Regular',
                'description'=>'Pespsi-Regular',
                'image'=>'1573749281_.jpeg',
                'price'=>130,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Soda folk can drink cream soda 330ml',
                'description'=>'Soda folk can drink cream soda 330ml',
                'image'=>'1573749323_.jpeg',
                'price'=>220,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Starbucks Frappuccino Coffee Drink',
                'description'=>'Starbucks Frappuccino Coffee Drink',
                'image'=>'1573794279_.jpeg',
                'price'=>360,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Starbucks House Bleand Coffee 250g',
                'description'=>'Starbucks House Bleand Coffee 250g',
                'image'=>'1573794308_.jpeg',
                'price'=>600,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Nestle Fruita Vitals Grapes Juice 1Litre',
                'description'=>'Nestle Fruita Vitals Red Grapes Juice 1Litre',
                'image'=>'1573794367_.png',
                'price'=>190,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Nestle Mango Orange Juice 200ml',
                'description'=>'Nestle Mango Orange Juice 200ml',
                'image'=>'1573794396_.jpeg',
                'price'=>280,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Masafi alkalife alkaline water 1.5liter',
                'description'=>'Masafi alkalife alkaline water 1.5liter',
                'image'=>'1573794429_.png',
                'price'=>130,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Masafi alkalife alkaline water 1.5liter',
                'description'=>'Masafi alkalife alkaline water 1.5liter',
                'image'=>'1573794429_.png',
                'price'=>166,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Nestle Pure Life Water 5Litre',
                'description'=>'Nestle Pure Life Water 5Litre',
                'image'=>'1573794456_.png',
                'price'=>450,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Nestle Cerelac, Honey  1000 Grams',
                'description'=>'Nestle Cerelac, Honey and Wheat with Milk 1000 Grams',
                'image'=>'1573794568_.png',
                'price'=>340,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'S-26 Progress Gold Milk Powder  Tin 900g',
                'description'=>'S-26 Progress Gold Milk Powder Stage-3 Tin 900g',
                'image'=>'1573794630_.jpeg',
                'price'=>500,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Tesco Caramel & Chocolate Spread 400g',
                'description'=>'Tesco Caramel & Chocolate Spread 400g',
                'image'=>'1573794655_.jpeg',
                'price'=>540,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Tesco Duo Chocolate Spread 400g',
                'description'=>'Tesco Duo Chocolate Spread 400g',
                'image'=>'1573794679_.jpeg',
                'price'=>450,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Nestle Milk Pak Yogurt 450g',
                'description'=>'Nestle Milk Pak Yogurt 450g',
                'image'=>'1573794705_.jpeg',
                'price'=>350,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Nestle Podina Raita 250 Grams',
                'description'=>'Nestle Podina Raita 250 Grams',
                'image'=>'1573794726_.png',
                'price'=>340,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>4,
                'name'=>'Noble Star Chocolate Candy 180g',
                'description'=>'Noble Star Chocolate Candy 180g',
                'image'=>'1573794765_.jpeg',
                'price'=>350,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);

        DB::table('products')->insert([
                'shop_id'=>5,
                'name'=>'Soda folk can drink orange330ml',
                'description'=>'Soda folk can drink orange330ml',
                'image'=>'1573794828_.jpeg',
                'price'=>350,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>5,
                'name'=>'Tchibo caffe crema',
                'description'=>'Tchibo caffe crema',
                'image'=>'1573794867_.jpeg',
                'price'=>660,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>5,
                'name'=>'Nestle Tropical  Punch Juice 1Litr',
                'description'=>'Nestle Tropical  Punch Juice 1Litr',
                'image'=>'1573794911_.jpeg',
                'price'=>240,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>5,
                'name'=>'Nestle Tropical Punch Juice 200ml',
                'description'=>'Nestle Tropical Punch Juice 200ml',
                'image'=>'1573794940_.jpeg',
                'price'=>374,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>5,
                'name'=>'Pom  Pomegranate Juice 236ml',
                'description'=>'Pom Wonderful 100% Pomegranate Juice 236ml',
                'image'=>'1573795004_.jpeg',
                'price'=>380,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);

        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Tchibo Espresso',
                'description'=>'Tchibo Espresso',
                'image'=>'1573795092_.jpeg',
                'price'=>480,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Tchibo Gold Instant',
                'description'=>'Tchibo Gold Instant',
                'image'=>'1573795133_.jpeg',
                'price'=>750,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Ribena Blackcurrant Juice 250ml',
                'description'=>'Ribena Blackcurrant Juice 250ml',
                'image'=>'1573795171_.jpeg',
                'price'=>320,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Stute Apple & Raspberry Juice 1 Liter',
                'description'=>'Stute Apple & Raspberry Juice 1 Liter',
                'image'=>'1573795243_.jpeg',
                'price'=>355,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Stute Superior tropical  drink 1.5liter',
                'description'=>'Stute Superior tropical fruit juice drink 1.5liter',
                'image'=>'1573795318_.jpeg',
                'price'=>380,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Walkers Ginger Drink 150ml',
                'description'=>'Walkers Ginger Drink 150ml',
                'image'=>'1573795348_.jpeg',
                'price'=>450,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Simple Truth orgranic coconut water',
                'description'=>'Simple Truth orgranic coconut water',
                'image'=>'1573795387_.jpeg',
                'price'=>560,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Yamama Rose Water',
                'description'=>'Yamama Rose Water',
                'image'=>'1573795418_.jpeg',
                'price'=>650,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Oreo Ice Cream Cone 110ml',
                'description'=>'Oreo Ice Cream Cone 110ml',
                'image'=>'1573795452_.jpeg',
                'price'=>290,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Total Greek Yoghurt 500 Grams',
                'description'=>'Total Greek Yoghurt 500 Grams',
                'image'=>'1573795486_.jpeg',
                'price'=>650,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Tesco Bubbly White Bars 5 X 25g',
                'description'=>'Tesco Bubbly White Bars 5 X 25g',
                'image'=>'1573795527_.jpeg',
                'price'=>680,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Tesco Cough Candy 200g',
                'description'=>'Tesco Cough Candy 200g',
                'image'=>'1573795613_.jpeg',
                'price'=>850,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Twix White Limited Edition 46 Grams',
                'description'=>'Twix White Limited Edition 46 Grams',
                'image'=>'1573795636_.jpeg',
                'price'=>450,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00'
        ]);
        DB::table('products')->insert([
                'shop_id'=>6,
                'name'=>'Werther\'s Original Soft Chocolate Caramels 100g',
                'description'=>'Werther\'s Original Soft Chocolate Caramels 100g',
                'image'=>'1573795663_.png',
                'price'=>870,
                'mfc_date'=>'2019-11-20 00:00:00',
                'exp_date'=>'2020-01-20 00:00:00',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
        ]);



    }
}
