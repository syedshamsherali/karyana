<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            'fullname' => 'admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'user_contact' => '03021456987',
            'type' => 'admin',
            'password' => Hash::make('123123123'),
            'city' => 'lahore',
            'address' => 'shadman',
            'longitude' => null,
            'latitude' => null,
            'is_active' => 1,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('users')->insert([

            'fullname' => 'customer',
            'email' => 'customer@gmail.com',
            'email_verified_at' => now(),
            'user_contact' => '03021456987',
            'type' => 'customer',
            'password' => Hash::make('123123123'),
            'city' => 'lahore',
            'address' => 'shadman',
            'longitude' => null,
            'latitude' => null,
            'is_active' => 1,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('users')->insert([

            'fullname' => 'Haider Ali',
            'email' => 'rider@gmail.com',
            'email_verified_at' => now(),
            'user_contact' => '03021456987',
            'type' => 'rider',
            'password' => Hash::make('123123123'),
            'city' => 'lahore',
            'address' => 'shadman',
            'longitude' => null,
            'latitude' => null,
            'is_active' => 1,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
    }
}
