<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('categories')->insert([

            'name'=>'Beverages',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);

        DB::table('categories')->insert([

            'name'=>' Food  ',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);

        DB::table('categories')->insert([

            'name'=>' Cooking & Baking',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);

        DB::table('categories')->insert([

            'name'=>' Fruit & Vegetable',

            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);



        DB::table('categories')->insert([
            'parent_id'=>1,
            'name'=>'Juices&Drinks',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('categories')->insert([
            'parent_id'=>1,
            'name'=>'Carbonated Drinks',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('categories')->insert([
            'parent_id'=>1,
            'name'=>' Coffee  ',

            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]);
        DB::table('categories')->insert([
            'parent_id'=>1,
            'name'=>' Water  ',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);

        DB::table('categories')->insert([
            'parent_id'=>2,
            'name'=>' Baby Food  ',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('categories')->insert([
            'parent_id'=>2,
            'name'=>' Breakfast',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('categories')->insert([
            'parent_id'=>2,
            'name'=>' Biscuits',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('categories')->insert([
            'parent_id'=>2,
            'name'=>' Sweet',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('categories')->insert([
            'parent_id'=>2,
            'name'=>' Dairy Products',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('categories')->insert([
            'parent_id'=>2,
            'name'=>' Protein ',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);
        DB::table('categories')->insert([
            'parent_id'=>2,
            'name'=>' Nuts&Seeds',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);


        DB::table('categories')->insert([
            'parent_id'=>3,
            'name'=>'Baking',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),

        ]);

    }
}
